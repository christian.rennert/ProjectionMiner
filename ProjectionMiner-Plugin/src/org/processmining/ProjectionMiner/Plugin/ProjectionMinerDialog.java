package org.processmining.ProjectionMiner.Plugin;

import com.fluxicon.slickerbox.factory.SlickerFactory;
import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.*;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.eSTPlugIn;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.estminer_fix.plugins.ESTMinerPlugin;
import org.processmining.ProjectionMiner.utils.FiringSequenceHeuristics.*;
import org.processmining.ProjectionMiner.utils.Modifiers.PetriNetModifier;
import org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions.InactiveStrategy;
import org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions.InductivePTreeSampler;
import org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions.LoopingTransitionStrategy;
import org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions.TraceClusterer;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProjectionMinerDialog extends JPanel {
    // ProM concerning ID
    private static final long serialVersionUID = 410560729436210971L;

    // All items in the user interface, that can be handled or read.
    public static final String TITLE = "Configure ProjectionMiner parameters:";

    // Relevant Dialog fields
    private final JComboBox<DiscoveryPlugin> discoveryAlgorithmComboBox;
    private final JComboBox<String> replayMethodComboBox;
    private final JComboBox<LoopingTransitionStrategy> transitionSamplerJComboBox;
    private final JCheckBox initialMergeCheckBox;
    private final JSlider maxDepthSlider;
    private final JCheckBox skipFirstDiscoveryCheckBox;
    //    private final JCheckBox smallStepsCheckBox;
//    private final JCheckBox colorRecursionCheckBox;
    private final JCheckBox reduceSilentTransitionsCheckBox;
    private final JCheckBox resetColorCodingCheckBox;
    private final JComboBox<AbstractFiringSequenceChooser> chooserJComboBox;
    private final JCheckBox uniqueTracePerVariantCheckBox;

    /**
     * Constructs the user interface for the eST-Miner in ProM.
     *
     * @param log The log containing the traces.
     */
    @SuppressWarnings("unchecked")
    public ProjectionMinerDialog(Petrinet net, XLog log) {
        // Constructs an empty UI with the given title.
//    super(TITLE);
        SlickerFactory factory = SlickerFactory.instance();

        int leftColumnWidth = 200;
        int columnMargin = 20;
        int rowHeight = 40;

        SpringLayout layout = new SpringLayout();
        setLayout(layout);


        final JLabel discoverySettingsLabel;
        {
            discoverySettingsLabel = factory.createLabel("   -- Basic discovery settings --   ");
            add(discoverySettingsLabel);
            layout.putConstraint(SpringLayout.NORTH, discoverySettingsLabel, 5, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, discoverySettingsLabel, leftColumnWidth, SpringLayout.WEST, this);
        }

        // Chooses the Discovery algorithm
        DiscoveryPlugin[] discoveryVariants = new DiscoveryPlugin[]{
                new ESTMinerPlugin(),
                new eSTPlugIn(),
                new LocalInductiveMiner(),
                new LocalILPMiner(),
                new LocalSplitMiner(),
                new TransitionSystemRegionMiner(),
                new LocalHeuristicMiner()
        };
        final JLabel variantLabel;
        {
            variantLabel = factory.createLabel("Discovery algorithm:");
            add(variantLabel);
            layout.putConstraint(SpringLayout.NORTH, variantLabel, 23, SpringLayout.NORTH, discoverySettingsLabel);
            layout.putConstraint(SpringLayout.EAST, variantLabel, leftColumnWidth, SpringLayout.WEST, this);

            discoveryAlgorithmComboBox = factory.createComboBox(discoveryVariants);
            discoveryAlgorithmComboBox.setPreferredSize(discoveryAlgorithmComboBox.getMaximumSize());
            add(discoveryAlgorithmComboBox);
            layout.putConstraint(SpringLayout.WEST, discoveryAlgorithmComboBox, columnMargin, SpringLayout.EAST, variantLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, discoveryAlgorithmComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    variantLabel);
        }


        final JLabel maxDepthLabel;
        final JLabel maxDepthValue;
        {
            maxDepthLabel = factory.createLabel("Maximal recursion depth:");
            add(maxDepthLabel);
            layout.putConstraint(SpringLayout.NORTH, maxDepthLabel, 23, SpringLayout.NORTH, variantLabel);
            layout.putConstraint(SpringLayout.EAST, maxDepthLabel, leftColumnWidth, SpringLayout.WEST, this);

            maxDepthSlider = factory.createSlider(SwingConstants.HORIZONTAL);
            maxDepthSlider.setMinimum(0);
            maxDepthSlider.setMaximum(10);
            maxDepthSlider.setValue(5);
            add(maxDepthSlider);
            layout.putConstraint(SpringLayout.WEST, maxDepthSlider, columnMargin, SpringLayout.EAST, maxDepthLabel);
            layout.putConstraint(SpringLayout.EAST, maxDepthSlider, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, maxDepthSlider, 0, SpringLayout.VERTICAL_CENTER,
                    maxDepthLabel);
            maxDepthValue = factory.createLabel(Integer.toString(maxDepthSlider.getValue()));
            add(maxDepthValue);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, maxDepthValue, 0, SpringLayout.VERTICAL_CENTER, maxDepthLabel);
            layout.putConstraint(SpringLayout.WEST, maxDepthValue, columnMargin, SpringLayout.EAST, maxDepthSlider);

            maxDepthSlider.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    maxDepthValue.setText(Integer.toString(maxDepthSlider.getValue()));
                }
            });
        }

        final JLabel firingSequencesLabel;
        {
            firingSequencesLabel = factory.createLabel("-- Firing Sequence & Log Settings --");
            add(firingSequencesLabel);
            layout.putConstraint(SpringLayout.NORTH, firingSequencesLabel, 40, SpringLayout.NORTH, maxDepthLabel);
            layout.putConstraint(SpringLayout.EAST, firingSequencesLabel, leftColumnWidth, SpringLayout.WEST, this);
        }

        String[] replayMethods = new String[]{
                "Alignments",
                "Search on Petri net",
                "Search on Petri net (greedy)",
        };
        final JLabel ReplayMethodLabel;
        {
            ReplayMethodLabel = factory.createLabel("Firing sequence finding method:");
            add(ReplayMethodLabel);
            layout.putConstraint(SpringLayout.NORTH, ReplayMethodLabel, 23, SpringLayout.NORTH, firingSequencesLabel);
            layout.putConstraint(SpringLayout.EAST, ReplayMethodLabel, leftColumnWidth, SpringLayout.WEST, this);

            replayMethodComboBox = factory.createComboBox(replayMethods);
            replayMethodComboBox.setPreferredSize(replayMethodComboBox.getMaximumSize());
            add(replayMethodComboBox);
            replayMethodComboBox.setSelectedIndex(1);
            layout.putConstraint(SpringLayout.WEST, replayMethodComboBox, columnMargin, SpringLayout.EAST, ReplayMethodLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, replayMethodComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    ReplayMethodLabel);
        }


        // Chooses the transition sampler algorithm
        AbstractFiringSequenceChooser[] chooserVariants = new AbstractFiringSequenceChooser[]{
                new GlobalMinimizeTokenIncrementsFiringSequenceChooser(),
                new GlobalMinimizeTokensFiringSequenceChooser()
        };
        final JLabel firingSequenceChooserLabel;
        {
            firingSequenceChooserLabel = factory.createLabel("Heuristic for choosing sequences:");
            add(firingSequenceChooserLabel);
            layout.putConstraint(SpringLayout.NORTH, firingSequenceChooserLabel, 23, SpringLayout.NORTH, ReplayMethodLabel);
            layout.putConstraint(SpringLayout.EAST, firingSequenceChooserLabel, leftColumnWidth, SpringLayout.WEST, this);

            chooserJComboBox = factory.createComboBox(chooserVariants);
            chooserJComboBox.setPreferredSize(chooserJComboBox.getMaximumSize());
            add(chooserJComboBox);
            layout.putConstraint(SpringLayout.WEST, chooserJComboBox, columnMargin, SpringLayout.EAST, firingSequenceChooserLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, chooserJComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    firingSequenceChooserLabel);
        }

        final JLabel uniqueTracePerVariantLabel;
        {
            uniqueTracePerVariantLabel = factory.createLabel("Reduce to one trace per Variant?");
            add(uniqueTracePerVariantLabel);
            layout.putConstraint(SpringLayout.NORTH, uniqueTracePerVariantLabel, 23, SpringLayout.NORTH, firingSequenceChooserLabel);
            layout.putConstraint(SpringLayout.EAST, uniqueTracePerVariantLabel, leftColumnWidth, SpringLayout.WEST, this);

            uniqueTracePerVariantCheckBox = factory.createCheckBox("", true);
            uniqueTracePerVariantCheckBox.setPreferredSize(uniqueTracePerVariantCheckBox.getMaximumSize());
            add(uniqueTracePerVariantCheckBox);
            layout.putConstraint(SpringLayout.WEST, uniqueTracePerVariantCheckBox, columnMargin, SpringLayout.EAST, uniqueTracePerVariantLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, uniqueTracePerVariantCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    uniqueTracePerVariantLabel);
        }

        final JLabel discoveryAssistanceLabel;
        {
            discoveryAssistanceLabel = factory.createLabel("-- Discovery assistance Settings -- ");
            add(discoveryAssistanceLabel);
            layout.putConstraint(SpringLayout.NORTH, discoveryAssistanceLabel, 40, SpringLayout.NORTH, uniqueTracePerVariantLabel);
            layout.putConstraint(SpringLayout.EAST, discoveryAssistanceLabel, leftColumnWidth, SpringLayout.WEST, this);
        }

        // Chooses the transition sampler algorithm
        LoopingTransitionStrategy[] transitionSamplerVariants = new LoopingTransitionStrategy[]{
                new InductivePTreeSampler(),
                new TraceClusterer(),
                new InactiveStrategy()
        };
        final JLabel transitionSamplerLabel;
        {
            transitionSamplerLabel = factory.createLabel("Guiding approach, if stuck:");
            add(transitionSamplerLabel);
            layout.putConstraint(SpringLayout.NORTH, transitionSamplerLabel, 23, SpringLayout.NORTH, discoveryAssistanceLabel);
            layout.putConstraint(SpringLayout.EAST, transitionSamplerLabel, leftColumnWidth, SpringLayout.WEST, this);

            transitionSamplerJComboBox = factory.createComboBox(transitionSamplerVariants);
            transitionSamplerJComboBox.setPreferredSize(transitionSamplerJComboBox.getMaximumSize());
            add(transitionSamplerJComboBox);
            layout.putConstraint(SpringLayout.WEST, transitionSamplerJComboBox, columnMargin, SpringLayout.EAST, transitionSamplerLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, transitionSamplerJComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    transitionSamplerLabel);

            transitionSamplerJComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    maxDepthSlider.setValue(((LoopingTransitionStrategy) transitionSamplerJComboBox.getSelectedItem()).getPrefRecursionDepth());
                }
            });
        }

        final JLabel skipFirstDiscoveryLabel;
        {
            skipFirstDiscoveryLabel = factory.createLabel("Initial guidance?");
            add(skipFirstDiscoveryLabel);
            layout.putConstraint(SpringLayout.NORTH, skipFirstDiscoveryLabel, 23, SpringLayout.NORTH, transitionSamplerLabel);
            layout.putConstraint(SpringLayout.EAST, skipFirstDiscoveryLabel, leftColumnWidth, SpringLayout.WEST, this);

            skipFirstDiscoveryCheckBox = factory.createCheckBox("", false);
            skipFirstDiscoveryCheckBox.setPreferredSize(skipFirstDiscoveryCheckBox.getMaximumSize());
            add(skipFirstDiscoveryCheckBox);
            layout.putConstraint(SpringLayout.WEST, skipFirstDiscoveryCheckBox, columnMargin, SpringLayout.EAST, skipFirstDiscoveryLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, skipFirstDiscoveryCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    skipFirstDiscoveryLabel);
        }

        final JLabel petriNetManipulationLabel;
        {
            petriNetManipulationLabel = factory.createLabel("-- PN Input & Output manipulation --");
            add(petriNetManipulationLabel);
            layout.putConstraint(SpringLayout.NORTH, petriNetManipulationLabel, 40, SpringLayout.NORTH, skipFirstDiscoveryLabel);
            layout.putConstraint(SpringLayout.EAST, petriNetManipulationLabel, leftColumnWidth, SpringLayout.WEST, this);
        }

        final JLabel initialMergeLabel;
        {
            initialMergeLabel = factory.createLabel("Initial place merging?");
            add(initialMergeLabel);
            layout.putConstraint(SpringLayout.NORTH, initialMergeLabel, 23, SpringLayout.NORTH, petriNetManipulationLabel);
            layout.putConstraint(SpringLayout.EAST, initialMergeLabel, leftColumnWidth, SpringLayout.WEST, this);

            initialMergeCheckBox = factory.createCheckBox("", false);
            initialMergeCheckBox.setEnabled(net != null);
            initialMergeCheckBox.setVisible(net != null);
            if(net != null) {
                initialMergeCheckBox.setPreferredSize(initialMergeCheckBox.getMaximumSize());
                add(initialMergeCheckBox);
                layout.putConstraint(SpringLayout.WEST, initialMergeCheckBox, columnMargin, SpringLayout.EAST, initialMergeLabel);
                layout.putConstraint(SpringLayout.VERTICAL_CENTER, initialMergeCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                        initialMergeLabel);
            }else{
                JLabel noNetMergeLabel = factory.createLabel("[Unavailable: no net given]");
                add(noNetMergeLabel);
                layout.putConstraint(SpringLayout.WEST, noNetMergeLabel, columnMargin, SpringLayout.EAST, initialMergeLabel);
                layout.putConstraint(SpringLayout.VERTICAL_CENTER, noNetMergeLabel, 0, SpringLayout.VERTICAL_CENTER,
                        initialMergeLabel);
            }
        }

//        final JLabel smallStepsLabel;
//        {
//            smallStepsLabel = factory.createLabel("Small step sampling?");
//            add(smallStepsLabel);
//            layout.putConstraint(SpringLayout.NORTH, smallStepsLabel, 23, SpringLayout.NORTH, initialMergeLabel);
//            layout.putConstraint(SpringLayout.EAST, smallStepsLabel, leftColumnWidth, SpringLayout.WEST, this);
//
//            smallStepsCheckBox = factory.createCheckBox("", true);
//            smallStepsCheckBox.setPreferredSize(smallStepsCheckBox.getMaximumSize());
//            add(smallStepsCheckBox);
//            layout.putConstraint(SpringLayout.WEST, smallStepsCheckBox, columnMargin, SpringLayout.EAST, smallStepsLabel);
//            layout.putConstraint(SpringLayout.VERTICAL_CENTER, smallStepsCheckBox, 0, SpringLayout.VERTICAL_CENTER,
//                    smallStepsLabel);
//        }

//        final JLabel colorRecursionLabel;
//        {
//            colorRecursionLabel = factory.createLabel("Color Recursion?");
//            add(colorRecursionLabel);
//            layout.putConstraint(SpringLayout.NORTH, colorRecursionLabel, 23, SpringLayout.NORTH, initialMergeLabel);
//            layout.putConstraint(SpringLayout.EAST, colorRecursionLabel, leftColumnWidth, SpringLayout.WEST, this);
//
//            colorRecursionCheckBox = factory.createCheckBox("", true);
//            colorRecursionCheckBox.setPreferredSize(colorRecursionCheckBox.getMaximumSize());
//            add(colorRecursionCheckBox);
//            layout.putConstraint(SpringLayout.WEST, colorRecursionCheckBox, columnMargin, SpringLayout.EAST, colorRecursionLabel);
//            layout.putConstraint(SpringLayout.VERTICAL_CENTER, colorRecursionCheckBox, 0, SpringLayout.VERTICAL_CENTER,
//                    colorRecursionLabel);
//        }

        final JLabel reduceSilentTransitionsLabel;
        {
            reduceSilentTransitionsLabel = factory.createLabel("Result: Reduce silent transitions?");
            add(reduceSilentTransitionsLabel);
            layout.putConstraint(SpringLayout.NORTH, reduceSilentTransitionsLabel, 23, SpringLayout.NORTH, initialMergeLabel);
            layout.putConstraint(SpringLayout.EAST, reduceSilentTransitionsLabel, leftColumnWidth, SpringLayout.WEST, this);

            reduceSilentTransitionsCheckBox = factory.createCheckBox("", false);
            reduceSilentTransitionsCheckBox.setPreferredSize(reduceSilentTransitionsCheckBox.getMaximumSize());
            add(reduceSilentTransitionsCheckBox);
            layout.putConstraint(SpringLayout.WEST, reduceSilentTransitionsCheckBox, columnMargin, SpringLayout.EAST, reduceSilentTransitionsLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, reduceSilentTransitionsCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    reduceSilentTransitionsLabel);
        }

        final JLabel resetColorCodingLabel;
        {
            resetColorCodingLabel = factory.createLabel("Reset color coding?");
            add(resetColorCodingLabel);
            layout.putConstraint(SpringLayout.NORTH, resetColorCodingLabel, 23, SpringLayout.NORTH, reduceSilentTransitionsLabel);
            layout.putConstraint(SpringLayout.EAST, resetColorCodingLabel, leftColumnWidth, SpringLayout.WEST, this);

            resetColorCodingCheckBox = factory.createCheckBox("", net == null);
            resetColorCodingCheckBox.setPreferredSize(resetColorCodingCheckBox.getMaximumSize());
            add(resetColorCodingCheckBox);
            layout.putConstraint(SpringLayout.WEST, resetColorCodingCheckBox, columnMargin, SpringLayout.EAST, resetColorCodingLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, resetColorCodingCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    resetColorCodingLabel);
        }
    }

    public ProjectionMinerParameters apply() {
        ProjectionMinerParameters parameters = new ProjectionMinerParameters(
                (DiscoveryPlugin) discoveryAlgorithmComboBox.getSelectedItem(),
                replayMethodComboBox.getSelectedIndex(),
                (LoopingTransitionStrategy) transitionSamplerJComboBox.getSelectedItem(),
                initialMergeCheckBox.isSelected(),
                maxDepthSlider.getValue(),
                skipFirstDiscoveryCheckBox.isSelected(),
                reduceSilentTransitionsCheckBox.isSelected(),
                replayMethodComboBox.getSelectedIndex() == 2,
                (AbstractFiringSequenceChooser) chooserJComboBox.getSelectedItem(),
                uniqueTracePerVariantCheckBox.isSelected()
        );

        PetriNetModifier.getInstance().updateShouldColor(true);

        if (resetColorCodingCheckBox.isSelected()) {
            PetriNetModifier.getInstance().resetColor();
        }

        return parameters;
    }
}


