package org.processmining.ProjectionMiner.Plugin;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions.LoopingTransitionStrategy;
import org.processmining.ProjectionMiner.utils.FiringSequenceHeuristics.AbstractFiringSequenceChooser;

public class ProjectionMinerParameters {
    private final DiscoveryPlugin discoveryPlugin;
    private final int selectedReplayMethod;
    private final LoopingTransitionStrategy loopingTransitionSampler;
    private final boolean initialPlaceMerge;
    private final int maxRecursionDepth;
    private final boolean skipFirstDiscovery;
    private final boolean reduceSilentTransitions;
    private final AbstractFiringSequenceChooser firingSequenceChooser;
    private final boolean uniqueTracePerVariant;
    private final boolean greedyFiringSequences;

    public ProjectionMinerParameters(DiscoveryPlugin discoveryPlugin, int selectedReplayMethod, LoopingTransitionStrategy loopingTransitionSampler, boolean initialPlaceMerge,
                                     int maxRecursionDepth, boolean skipFirstDiscovery, boolean reduceSilentTransitions,
                                     boolean greedyFiringSequences, AbstractFiringSequenceChooser firingSequenceChooser, boolean uniqueTracePerVariant) {
        this.discoveryPlugin = discoveryPlugin;
        this.selectedReplayMethod = selectedReplayMethod;
        this.loopingTransitionSampler = loopingTransitionSampler;
        this.initialPlaceMerge = initialPlaceMerge;
        this.maxRecursionDepth = maxRecursionDepth;
        this.skipFirstDiscovery = skipFirstDiscovery;
        this.reduceSilentTransitions = reduceSilentTransitions;
        this.greedyFiringSequences = greedyFiringSequences;
        this.firingSequenceChooser = firingSequenceChooser;
        this.uniqueTracePerVariant = uniqueTracePerVariant;
    }

    public int getSelectedReplayMethod() {
        return selectedReplayMethod;
    }

    public DiscoveryPlugin getDiscoveryPlugin() {
        return discoveryPlugin;
    }

    public LoopingTransitionStrategy getLoopingTransitionSampler() {
        return loopingTransitionSampler;
    }

    public boolean shouldInitialPlaceMerge() {
        return initialPlaceMerge;
    }

    public int getMaxRecursionDepth() {
        return maxRecursionDepth;
    }

    public boolean shouldSkipFirstDiscovery() {
        return skipFirstDiscovery;
    }

    public boolean shouldReduceSilentTransitions() {
        return reduceSilentTransitions;
    }

    public boolean shouldUseUniqueTracePerVariant() {
        return uniqueTracePerVariant;
    }

    public int getSequenceGeneratorDepth() {
        return 15;
    }

    public boolean isGreedyFiringSequences() {
        return greedyFiringSequences;
    }

    public AbstractFiringSequenceChooser getFiringSequenceChooser() {
        return firingSequenceChooser;
    }
}
