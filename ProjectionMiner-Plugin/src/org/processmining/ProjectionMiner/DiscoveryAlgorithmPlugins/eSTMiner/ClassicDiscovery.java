package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import java.util.ArrayList;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.CandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceState;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;

/**
 *
 */
public class ClassicDiscovery extends Thread {
    // discovery using the threshold delta and the potential traces queue to ensure minimal global fitness

    private final CandidateTraverser traverser;
    private final ImplicitPlaceRemover placeRemover;
    private final eSTLog log;
    private final double threshold;
    private final boolean removeConcurrently;
    private eSTProcessModel processModel;

    /**
     * @param processModel
     * @param traverser
     * @param placeRemover
     * @param parameters
     * @param log
     */
    public ClassicDiscovery(final eSTProcessModel processModel, final CandidateTraverser traverser,
                            ImplicitPlaceRemover placeRemover, Parameters parameters, eSTLog log) {
        this.processModel = processModel;
        this.traverser = traverser;
        this.placeRemover = placeRemover;
        this.log = log;
        this.threshold = parameters.getThresholdTau();
        removeConcurrently = parameters.shouldRemoveImplicits() && parameters.shouldRemoveImplicitsConcurrently();
    }

    @Override
    public void run() {
        try {
            addPlaces();
            System.out.println("Finished adding places without interuption.");

        } catch (InterruptedException e) {
            System.out.println("Interupted Exception thrown!");
        }
    }

    private void addPlaces() throws InterruptedException {
        int currentTreeDepth = 0;
        boolean nextTreeDepthReached = false;
        eSTPlace current = traverser.getNext(null, eSTPlaceState.FIT);
        while (current != null) {
            if (!isInterrupted()) {

                //update current tree depth (for global fitness evaluation and queue retesting
                nextTreeDepthReached = false;
                int updatedTreeDepth = current.getDepth();
                if (currentTreeDepth != current.getDepth()) {//for debugging
//                    System.out.println("\n New tree level: changed from " + currentTreeDepth + " to "
//                            + current.getDepth());
                    currentTreeDepth = updatedTreeDepth;
                    nextTreeDepthReached = true;
                }


                //-------------evaluating local fitness of current place----------------------------
                eSTPlaceState fitness = eSTPlaceEvaluator.testPlace(current, processModel, log, threshold);
                if (fitness == eSTPlaceState.FIT) {
                    //dealing with locally fit places
                    /*
                     * String traceVectorString ="Current Trace Vector: "; //for
                     * debugging for (int t=0;
                     * t<current.getTraceVector().length; t++) {
                     * traceVectorString =
                     * traceVectorString+current.getTraceVector()[t]+","; }
                     * System.out.println(traceVectorString);
                     */
                    PlugInStatistics.instance().incNumFitting();


                    //add place and check for implicitness with respect to new fitting place (if concurrent
                    // implicitness check is turned on)
                    if (removeConcurrently) {
                        final long timeIPremovalStart = System.currentTimeMillis();
                        ArrayList<eSTPlace> pMPlaces = processModel.getPlaces();
                        ArrayList<eSTPlace> implicitPlaces = placeRemover.removeImplicitsRelatedToPlace(current, processModel, log);
                        pMPlaces.add(current);
                        pMPlaces.removeAll(implicitPlaces);
                        System.out.println("Removed implicit places concurrently: " + implicitPlaces.toString());
                        processModel.setPlaces(pMPlaces);
                        PlugInStatistics.instance().addCurrentRemainingPlaces(pMPlaces.size());
                        PlugInStatistics.instance().incTimeImpTest(System.currentTimeMillis() - timeIPremovalStart);
                        //System.out.println(System.currentTimeMillis()-timeIPremovalStart);
                    } else {
                        processModel.addPlace(current);
                    }
                } //end of handling locally fitting current place
                else {// dealing with locally unfit places (just ignore them)
                    PlugInStatistics.instance().incNumUnfitting();
                }

                //prepare next candidate iteration
                long startTimeCandFind = System.currentTimeMillis();
                current = traverser.getNext(current, fitness);
                PlugInStatistics.instance().incTimeCandFind(System.currentTimeMillis() - startTimeCandFind);
            } else {
                //handling interuption (timelimit, if used)
                System.out.println("!!!Timelimit for adding places has been reached!!!");
                break;
            }
        } //end of candidate traversal loop (interupted or finished)
        if (removeConcurrently) {//if enabled, remove implicit places from current (final) model
            System.out.println("__End of Discovery - Final implicit place removal...__");
            System.out.println("Places before final IP removal: " + processModel.getPlaces().size());
            processModel = placeRemover.removeAllIPs(processModel, log);
            System.out.println("Places after final IP removal: " + processModel.getPlaces().size());
            processModel.printPlaceSummary();
        }
    }// end of discovery


    //G&S
    public eSTProcessModel getProcessModel() {
        return processModel;
    }

}
