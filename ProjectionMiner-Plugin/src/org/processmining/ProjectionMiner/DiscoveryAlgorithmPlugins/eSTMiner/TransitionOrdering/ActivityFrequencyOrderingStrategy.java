package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

import java.util.*;

/**
 * The total occurrences of each transition is counted in all traces to give a heuristic. The orderings of in- and
 * outgoing transitions are set in a descending order of the frequency of the corresponding transition.
 */
public class ActivityFrequencyOrderingStrategy extends TransitionOrderingStrategy {
    // The absolute frequency of each transition occurring. Stored in a corresponding ordering to the given transitions.
    private int[] transitionFrequencies;

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public ActivityFrequencyOrderingStrategy() {
        super();
    }

    @Override
    public void setTransitions(String[] transitions) {
        super.setTransitions(transitions);
        transitionFrequencies = new int[transitions.length];
    }

    /**
     * Computes and stores the absolute occurrences for each transition.
     *
     * @param log        The event log.
     * @param parameters The parameters of the eST miner algorithm.
     */
    @Override
    public void computeHeuristics(XLog log, Parameters parameters) {
        HashMap<String, Integer> posOverview = new HashMap<>();
        XEventClassifier eventClassifier = parameters.getEventClassifier();

        // Efficient storage of the transitions positions.
        for (int i = 0; i < transitions.length; i++) {
            posOverview.put(transitions[i], i);
        }

        // Incrementing the value for the occurrence, if a transition occurs.
        for (XTrace trace : log) {
            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);

                transitionFrequencies[posOverview.get(key)]++;
            }
        }
    }

    /**
     * Applies the computed heuristics to sort the in- and outgoing transition array accordingly.
     *
     * @return The reordered inTransitions and outTransitions arrays according to the total number of their occurrences.
     */
    @Override
    public TransitionOrderingResult apply() {
        // Transitions and their negative counts are added to a HashMap as we want to reverse the ordering.
        HashMap<String, Integer> transitionToFrequency = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            transitionToFrequency.put(transitions[i], -transitionFrequencies[i]);
        }

        // Storing the entries in a list and sorting descending by value
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(transitionToFrequency.entrySet());
        Collections.sort(entryList, Map.Entry.comparingByValue());

        // Extracting the ordering and storing it in a list.
        String[] newOrdering = new String[transitions.length];
        int i = 0;
        for (Map.Entry<String, Integer> entry : entryList) {
            newOrdering[i] = entry.getKey();
            i++;
        }

        return new TransitionOrderingResult(newOrdering.clone(), newOrdering.clone());
    }

    @Override
    public String toString() {
        return "Absolute Activity Frequency";
    }

    /**
     * Getter for the frequencies of the transitions - for testing only.
     *
     * @return The Array containing the frequencies of each transition.
     */
    public int[] getTransitionFrequencies() {
        return transitionFrequencies;
    }
}
