package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

import java.util.*;

public class AverageFirstOccurrenceStrategy extends TransitionOrderingStrategy {
    private float[] averageOccurrenceIndex;

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public AverageFirstOccurrenceStrategy() {
        super();
    }

    @Override
    public void setTransitions(String[] transitions) {
        super.setTransitions(transitions);
        averageOccurrenceIndex = new float[transitions.length];
    }

    @Override
    public void computeHeuristics(XLog log, Parameters parameters) {
        int[] sumOfOccurrenceIndices = new int[transitions.length];
        int[] numOfOccurrences = new int[transitions.length];

        HashMap<String, Integer> posOverview = new HashMap<>();
        XEventClassifier eventClassifier = parameters.getEventClassifier();

        // Efficient storage of the transitions positions.
        for (int i = 0; i < transitions.length; i++) {
            posOverview.put(transitions[i], i);
        }

        // Incrementing the value for the occurrence, if a transition occurs.
        for (XTrace trace : log) {
            boolean[] hasOccurredYet = new boolean[transitions.length];
            int eventPos = 1;

            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);

                int pos = posOverview.get(key);
                if (!hasOccurredYet[pos]) {
                    sumOfOccurrenceIndices[pos] += eventPos;
                    numOfOccurrences[pos]++;
                    hasOccurredYet[pos] = true;
                }

                eventPos++;
            }
        }

        for (int i = 0; i < transitions.length; i++) {
            averageOccurrenceIndex[i] = ((float) sumOfOccurrenceIndices[i]) / numOfOccurrences[i];
        }
    }

    @Override
    public TransitionOrderingResult apply() {
        // Transitions and their negative counts are added to a HashMap as we want to reverse the ordering.
        HashMap<String, Float> transitionToIngoingFrequency = new HashMap<>();
        HashMap<String, Float> transitionToOutgoingFrequency = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            transitionToIngoingFrequency.put(transitions[i], -averageOccurrenceIndex[i]);
            transitionToOutgoingFrequency.put(transitions[i], averageOccurrenceIndex[i]);
        }

        // Storing the entries in a list and sorting descending by value
        List<Map.Entry<String, Float>> ingoingEntryList = new ArrayList<>(transitionToIngoingFrequency.entrySet());
        Collections.sort(ingoingEntryList, Map.Entry.comparingByValue());

        List<Map.Entry<String, Float>> outgoingEntryList = new ArrayList<>(transitionToOutgoingFrequency.entrySet());
        Collections.sort(outgoingEntryList, Map.Entry.comparingByValue());

        // Extracting the ordering and storing it in a list.
        String[] newIngoingOrdering = new String[transitions.length];
        String[] newOutgoingOrdering = new String[transitions.length];

        int i = 0;
        for (Map.Entry<String, Float> entry : ingoingEntryList) {
            newIngoingOrdering[i] = entry.getKey();
            i++;
        }

        i = 0;
        for (Map.Entry<String, Float> entry : outgoingEntryList) {
            newOutgoingOrdering[i] = entry.getKey();
            i++;
        }

        return new TransitionOrderingResult(newIngoingOrdering, newOutgoingOrdering);
    }

    /**
     * Returns the name of the class.
     *
     * @return The name of the class.
     */
    @Override
    public String toString() {
        return "Average First Occurrence Index";
    }
}
