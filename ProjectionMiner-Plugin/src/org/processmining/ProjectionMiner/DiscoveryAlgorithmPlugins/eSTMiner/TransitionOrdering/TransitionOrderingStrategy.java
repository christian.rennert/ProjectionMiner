package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

/**
 * Abstract class for ordering strategies on in- and outgoing transition arrays.
 */
public abstract class TransitionOrderingStrategy {
    // An array containing the transitions occuring in the log.
    protected String[] transitions;

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public TransitionOrderingStrategy() {

    }

    /**
     * Set the transitions of the event log.
     *
     * @param transitions The transitions of the event log.
     */
    public void setTransitions(String[] transitions) {
        this.transitions = transitions;
    }

    /**
     * Computes the value of the heuristics for the current ordering strategy.
     *
     * @param log        The event log.
     * @param parameters The parameters of the eST miner algorithm.
     */
    public abstract void computeHeuristics(XLog log, Parameters parameters);

    /**
     * Applies the current ordering strategy and returns the corresponding reordered inTransitions and outTransitions array.
     *
     * @return The reordered inTransitions and outTransitions arrays.
     */
    public abstract TransitionOrderingResult apply();

    /**
     * Returns the name of the class.
     *
     * @return The name of the class.
     */
    public abstract String toString();
}
