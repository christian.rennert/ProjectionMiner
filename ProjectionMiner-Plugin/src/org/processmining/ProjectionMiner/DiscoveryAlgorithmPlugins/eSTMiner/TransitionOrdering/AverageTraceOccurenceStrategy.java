package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

import java.util.*;

/**
 * Computing the average fraction of an activities occurrences. The orderings of in- and
 * outgoing transitions are set in a descending order of the frequency of the corresponding transition.
 */
public class AverageTraceOccurenceStrategy extends TransitionOrderingStrategy {
    private float[] averageTraceOccurence;

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public AverageTraceOccurenceStrategy() {
        super();
    }

    @Override
    public void setTransitions(String[] transitions) {
        super.setTransitions(transitions);
        averageTraceOccurence = new float[transitions.length];
    }

    /**
     * Computes the values of the average occurrences of an activity in a trace for the whole log.
     *
     * @param log        The event log.
     * @param parameters The parameters of the eST miner algorithm.
     */
    @Override
    public void computeHeuristics(XLog log, Parameters parameters) {
        HashMap<String, Integer> posOverview = new HashMap<>();
        XEventClassifier eventClassifier = parameters.getEventClassifier();

        // Efficient storage of the transitions positions.
        for (int i = 0; i < transitions.length; i++) {
            posOverview.put(transitions[i], i);
        }

        // Incrementing the average value of occurrences in a trace.
        for (XTrace trace : log) {
            int[] traceOccurences = new int[transitions.length];

            // Counts the occurrences of each activity in the current trace.
            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);
                traceOccurences[posOverview.get(key)]++;
            }

            // Averages the occurrences and adds them to the stored values.
            for (int i = 0; i < transitions.length; i++) {
                averageTraceOccurence[i] += ((float) traceOccurences[i]) / trace.size();
            }
        }

        // Averaging over the whole log.
        for (int i = 0; i < transitions.length; i++) {
            averageTraceOccurence[i] = averageTraceOccurence[i] / log.size();
        }
    }


    /**
     * Applies the computed heuristics to sort the in- and outgoing transition array accordingly.
     *
     * @return The reordered inTransitions and outTransitions arrays according to the total number of their occurrences.
     */
    @Override
    public TransitionOrderingResult apply() {
        // Transitions and their negative counts are added to a HashMap as we want to reverse the ordering.
        HashMap<String, Float> transitionToFrequency = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            transitionToFrequency.put(transitions[i], -averageTraceOccurence[i]);
        }

        // Storing the entries in a list and sorting descending by value
        List<Map.Entry<String, Float>> entryList = new ArrayList<>(transitionToFrequency.entrySet());
        Collections.sort(entryList, Map.Entry.comparingByValue());

        // Extracting the ordering and storing it in a list.
        String[] newOrdering = new String[transitions.length];
        int i = 0;
        for (Map.Entry<String, Float> entry : entryList) {
            newOrdering[i] = entry.getKey();
            i++;
        }

        return new TransitionOrderingResult(newOrdering.clone(), newOrdering.clone());
    }

    /**
     * Returns the name of the class.
     *
     * @return The name of the class.
     */
    @Override
    public String toString() {
        return "Average Trace Occurrence";
    }
}
