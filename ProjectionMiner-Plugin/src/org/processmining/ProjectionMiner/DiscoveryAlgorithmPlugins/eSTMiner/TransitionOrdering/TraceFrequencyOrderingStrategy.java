package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

import java.util.*;

/**
 * The number of traces containing an activity at least once are counted. The orderings of in- and
 * outgoing transitions are set in a descending order of the frequency of the corresponding transition.
 */
public class TraceFrequencyOrderingStrategy extends TransitionOrderingStrategy {
    // The frequency of each transition occurring at least once in a trace.
    // Stored in a corresponding ordering to the given transitions.
    private int[] traceFrequencies;

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public TraceFrequencyOrderingStrategy() {
        super();
    }

    @Override
    public void setTransitions(String[] transitions) {
        super.setTransitions(transitions);
        traceFrequencies = new int[transitions.length];
    }

    /**
     * Computes the values absolute trace frequency heuristics.
     *
     * @param log        The event log.
     * @param parameters The parameters of the eST miner algorithm.
     */
    @Override
    public void computeHeuristics(XLog log, Parameters parameters) {
        HashMap<String, Integer> posOverview = new HashMap<>();
        XEventClassifier eventClassifier = parameters.getEventClassifier();

        // Efficient storage of the transitions positions.
        for (int i = 0; i < transitions.length; i++) {
            posOverview.put(transitions[i], i);
        }

        // Incrementing the value by one, if a transition occurs at least once in a trace.
        for (XTrace trace : log) {
            boolean[] hasOccurredYet = new boolean[transitions.length];

            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);

                int pos = posOverview.get(key);
                if (!hasOccurredYet[pos]) {
                    traceFrequencies[pos]++;
                    hasOccurredYet[pos] = true;
                }
            }
        }
    }

    /**
     * Applies the computed heuristics to sort the in- and outgoing transition array accordingly.
     *
     * @return The reordered inTransitions and outTransitions arrays according to the total number of their occurrences.
     */
    @Override
    public TransitionOrderingResult apply() {
        // Transitions and their negative counts are added to a HashMap as we want to reverse the ordering.
        HashMap<String, Integer> transitionToFrequency = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            transitionToFrequency.put(transitions[i], -traceFrequencies[i]);
        }

        // Storing the entries in a list and sorting by value
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(transitionToFrequency.entrySet());
        Collections.sort(entryList, Map.Entry.comparingByValue());

        // Extracting the ordering and storing it in a list.
        String[] newOrdering = new String[transitions.length];
        int i = 0;
        for (Map.Entry<String, Integer> entry : entryList) {
            newOrdering[i] = entry.getKey();
            i++;
        }

        return new TransitionOrderingResult(newOrdering.clone(), newOrdering.clone());
    }

    /**
     * Returns the name of the class.
     *
     * @return The name of the class.
     */
    @Override
    public String toString() {
        return "Absolute Trace Frequency";
    }
}
