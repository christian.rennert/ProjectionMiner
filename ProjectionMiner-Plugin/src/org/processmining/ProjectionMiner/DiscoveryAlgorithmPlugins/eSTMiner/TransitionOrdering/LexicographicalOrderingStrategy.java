package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.Parameters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class for the lexicographical structuring of the in- and outgoing transition arrays.
 */
public class LexicographicalOrderingStrategy extends TransitionOrderingStrategy {

    /**
     * Constructor for the TransitionOrderingStrategy.
     */
    public LexicographicalOrderingStrategy() {
        super();
    }

    /**
     * No functionality needed here for the computation of heuristics.
     *
     * @param log        The event log.
     * @param parameters The parameters of the eST miner algorithm.
     */
    @Override
    public void computeHeuristics(XLog log, Parameters parameters) {

    }

    /**
     * Computes the ordered in- and outgoing transitions arrays according to their lexicographical ordering.
     *
     * @return The reordered inTransitions and outTransitions arrays according to their lexicographical ordering.
     */
    @Override
    public TransitionOrderingResult apply() {
        List<String> transitionList = Arrays.asList(transitions);
        Collections.sort(transitionList, String.CASE_INSENSITIVE_ORDER);

        // Extracting the ordering and storing it in a list.
        String[] newOrdering = new String[transitions.length];
        int i = 0;
        for (String transition : transitionList) {
            newOrdering[i] = transition;
            i++;
        }

        return new TransitionOrderingResult(newOrdering.clone(), newOrdering.clone());
    }

    /**
     * Returns the name of the class.
     *
     * @return The name of the class.
     */
    @Override
    public String toString() {
        return "Lexicographic";
    }
}
