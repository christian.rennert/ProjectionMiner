package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering;

/**
 * A class to contain the results of a transition ordering strategy, concerning the new ordering of the in- and outgoing transitions.
 */
public class TransitionOrderingResult {
    private final String[] inTransitions;
    private final String[] outTransitions;

    /**
     * Constructor of the object to hold the results of a transition ordering strategy.
     *
     * @param inTransitions  The ingoing transitions array.
     * @param outTransitions The outgoing transitions array.
     */
    public TransitionOrderingResult(String[] inTransitions, String[] outTransitions) {
        this.inTransitions = inTransitions;
        this.outTransitions = outTransitions;
    }

    /**
     * Getter for the ingoing transitions array.
     *
     * @return The ingoing transitions, which are hold in this object.
     */
    public String[] getInTransitions() {
        return inTransitions;
    }

    /**
     * Getter for the outgoing transitions array.
     *
     * @return The outgoing transitions, which are hold in this object.
     */
    public String[] getOutTransitions() {
        return outTransitions;
    }
}
