package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceFactory;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.eSTLog;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Abstract class for all implicit place removal classes. Implicit places are all places that can be removed without
 * changing the behavior described by a process model.
 */
public abstract class ImplicitPlaceRemover {
    // Array containing all different transitions.
    protected String[] transitions;

    // All variants of the event log.
    protected LinkedList<LinkedList<Integer>> uniqueTraces;

    public void init(String[] transitions, LinkedList<LinkedList<Integer>> uniqueTraces) {
        this.transitions = transitions;
        this.uniqueTraces = uniqueTraces;
    }

//  ___________________________________Utility Methods___________________________________

    /**
     * For a given position in the (ingoing) transition array computes the corresponding bitmask.
     *
     * @param pos         Position in the corresponding transition array.
     * @param transitions The corresponding transition array.
     * @return A bitmask for the given position in the array.
     */
    protected int getMask(final int pos, final String[] transitions) {
        return (1 << (transitions.length - (pos + 1)));
    }


//  ________________________Main methods to remove implicit places_______________________

    /**
     * Removes all implicit places for a process model and its corresponding event log.
     *
     * @param processModel A mined process model.
     * @param log          The corresponding log to the process model.
     * @return A process model containing no implicit places.
     */
    public abstract eSTProcessModel removeAllIPs(eSTProcessModel processModel, eSTLog log);

    /**
     * Method that combines similar places in such a way, that two places who share the same input and output transitions
     * but have different bidirectional arcs to the same transition are combined into one place.
     *
     * @param processModel The process model, that may have similar places
     */
    public eSTProcessModel combineSimilarPlaces(eSTProcessModel processModel) {
        ArrayList<eSTPlace> places = processModel.getPlaces();

        // Iterating through all places and finding similar places
        for (int i = 0; i < places.size(); i++) {
            // Computing the masks of the transitions without the transitions that are in- and outgoing
            int currDoubledTransitions = places.get(i).getInputTransitionKey() & places.get(i).getOutputTransitionKey();
            int currInputMask = places.get(i).getInputTransitionKey() - currDoubledTransitions;
            int currOutputMask = places.get(i).getOutputTransitionKey() - currDoubledTransitions;

            for (int j = i + 1; j < places.size(); j++) {
                // Same as before but for every place to be compared
                int nextDoubledTransitions = places.get(j).getInputTransitionKey() & places.get(j).getOutputTransitionKey();
                int nextInputMask = places.get(j).getInputTransitionKey() - nextDoubledTransitions;
                int nextOutputMask = places.get(j).getOutputTransitionKey() - nextDoubledTransitions;

                // If they have same ingoing and outgoing transitions as described before, both places are deleted and a
                // combined place is added to the list. Note, that we have to reduce the iterator manually to not skip an item.
                if (currInputMask == nextInputMask && currOutputMask == nextOutputMask) {
                    places.remove(j);
                    places.remove(i);

                    places.add(eSTPlaceFactory.getInstance().createPlace(currInputMask + currDoubledTransitions +
                            nextDoubledTransitions, currOutputMask + currDoubledTransitions + nextDoubledTransitions));
                    i--;
                    break;
                }
            }
        }

        processModel.setPlaces(places);
        return processModel;
    }

    /**
     * Removes all implicit places for a given place in a process model and its corresponding event log.
     *
     * @param specificPlace A specified place in the given process model.
     * @param processModel  A mined process model.
     * @param log           The corresponding log to the process model.
     * @return A list that does not contain implicit places whose behavior is modeled by the specified place.
     */
    public abstract ArrayList<eSTPlace> removeImplicitsRelatedToPlace(final eSTPlace specificPlace, final eSTProcessModel processModel, eSTLog log);

    public abstract String toString();
}
