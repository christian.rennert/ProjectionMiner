package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlugInStatistics;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.eSTLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Class for implicit place removal using the token based replay technique.
 */
public class ReplayBasedImplicitPlaceRemover extends ImplicitPlaceRemover {

    /**
     * Removes all implicit places for a process model and its corresponding event log.
     *
     * @param processModel A mined process model.
     * @param log          The corresponding log to the process model.
     * @return A process model containing no implicit places.
     */
    public eSTProcessModel removeAllIPs(eSTProcessModel processModel, final eSTLog log) {
        processModel.splitIntoSimilarPlaces();

        // Creating a copy of the log and reducing the copy to the replayable variants only.
        eSTLog reducedLog = new eSTLog(log);
//    reducedLog.reduceToReplayableVariants(processModel.getVariantVector());

        // Working on a shallow copy of the places in the process model and storing all found implicit places.
        ArrayList<eSTPlace> placesToCheck = new ArrayList<>(processModel.getPlaces());
        ArrayList<eSTPlace> implicitPlaces = new ArrayList<>();

        // For statistics, we store and count each iteration the number of remaining places.
        ArrayList<eSTPlace> remainingPlacesForStatistics = processModel.getPlaces();
        PlugInStatistics.instance().addCurrentRemainingPlaces(remainingPlacesForStatistics.size());

        // We check one place at a time and remove all places that are implicit to this place such that all places
        // remaining are non-implicit.
        while (placesToCheck.size() > 0) {
            // Therefore we take the first place in the list of places to be checked.
            eSTPlace place1 = placesToCheck.remove(0);
            implicitPlaces.addAll(removeImplicitsRelatedToPlace(place1, placesToCheck, reducedLog));
            placesToCheck.removeAll(implicitPlaces);

            // Updating statistics by keeping the remaining places up to date.
            remainingPlacesForStatistics.removeAll(implicitPlaces);
            PlugInStatistics.instance().addCurrentRemainingPlaces(remainingPlacesForStatistics.size());
        }

        // Removing all implicit places in the process model.
        ArrayList<eSTPlace> remainingPlaces = processModel.getPlaces();
        remainingPlaces.removeAll(implicitPlaces);
        processModel.setPlaces(remainingPlaces);

        return processModel;
    }


    /**
     * Computes the implicit places corresponding to the place given.
     *
     * @param specificPlace The specific place which has to be checked.
     * @param placesToCheck All remaining places to be checked.
     * @param reducedLog    A log for which the variants and their number of occurrences have already been calculated.
     * @return All implicit places in correspondence to the place given.
     */
    private ArrayList<eSTPlace> removeImplicitsRelatedToPlace(eSTPlace specificPlace, ArrayList<eSTPlace> placesToCheck,
                                                              eSTLog reducedLog) {
        // Finding the places related to the current place.
        ArrayList<eSTPlace> relatedPlaces = getRelatedPlaces(specificPlace, placesToCheck);
        ArrayList<eSTPlace> implicitPlaces = new ArrayList<>();

        // Incrementing the number of comparisons in the statistics..
        PlugInStatistics.instance().incComparisons(relatedPlaces.size());

        // Comparing each place to the specified, if it is not related, super- or subregion.
        for (eSTPlace currPlace : relatedPlaces) {

            // Differentiating between different cases.
            switch (testSubregionRelation(specificPlace, currPlace, reducedLog)) {
                case -1:
                    // The current place is a subregion of the given place
                    if (validateCombinedPlace(currPlace, specificPlace)) {
                        implicitPlaces.add(currPlace);
                    }
                    break;

                case 0:
                    // No relation between the places.
                    break;

                case 1:
                    // The current place is a superregion of the given place.
                    if (validateCombinedPlace(specificPlace, currPlace) && !implicitPlaces.contains(specificPlace)) {
                        implicitPlaces.add(specificPlace);
                    }
                    break;

                default:
                    if (specificPlace.isEqual(currPlace)) {
//                        System.out.println("Places " + specificPlace.toTransitionsString(transitions) + " and " + currPlace.toTransitionsString(transitions) + " are equal.");
                        implicitPlaces.add(currPlace);
                    }
//                    else {
////                        System.out.println("Places " + currPlace.toTransitionsString(transitions) + " and " + specificPlace.toTransitionsString(transitions) + "are not handled properly by IP-Remover!");
//                    }
            }
        }

        return implicitPlaces;
    }

    /**
     * Removes all implicit places for a given place in a process model and its corresponding event log.
     *
     * @param specificPlace A specified place in the given process model.
     * @param processModel  A mined process model.
     * @param log           The corresponding log to the process model.
     * @return A list that does not contain implicit places whose behavior is modeled by the specified place.
     */
    public ArrayList<eSTPlace> removeImplicitsRelatedToPlace(final eSTPlace specificPlace, final eSTProcessModel processModel, eSTLog log) {
        // Computing the replayable variants and their frequencies in the event log and storing them in the log object.
//    log.reduceToReplayableVariants(processModel.getVariantVector());

        // Computing the remaining implicit places.
        ArrayList<eSTPlace> implicitPlaces = removeImplicitsRelatedToPlace(specificPlace, processModel.getPlaces(), log);

        return implicitPlaces;
    }

    @Override
    public String toString() {
        return "Replay based implicit Place Removal";
    }

    /**
     * Checks whether there is no transition connecting the two places without loops.
     * This is used to check whether a combined place p1-p2 is valid.
     *
     * @param place1 First place to be checked.
     * @param place2 Second place to be checked.
     * @return True, if there is no transition connecting one place to another, without containing loops.
     */
    private boolean validateCombinedPlace(eSTPlace place1, eSTPlace place2) {
        // Getting all byte coded ingoing and outgoing transitions.
        int ingoingPlace1 = place1.getInputTransitionKey();
        int outgoingPlace1 = place1.getOutputTransitionKey();
        int ingoingPlace2 = place2.getInputTransitionKey();
        int outgoingPlace2 = place2.getOutputTransitionKey();

        // Checking for all transitions if they connect the places with no self-loops.
        for (int i = 0; i < transitions.length; i++) {
            int mask = getMask(i, transitions);

            if ((ingoingPlace2 & mask) > 0 && (outgoingPlace1 & mask) > 0 // A transition connects place2 to place1 with
                    && (outgoingPlace2 & mask) == 0 && (ingoingPlace1 & mask) == 0) { // no self-loops.
                return false;
            }

            if ((ingoingPlace1 & mask) > 0 && (outgoingPlace2 & mask) > 0 // A transition connects place2 to place1 with
                    && (outgoingPlace1 & mask) == 0 && (ingoingPlace2 & mask) == 0) { // no self-loops.
                return false;
            }
        }

        return true;
    }

    /**
     * Finds all places that have at least one ingoing transition in common with the given places.
     *
     * @param place  The place to be checked.
     * @param places A list of places.
     * @return The places that have at least one place in common with the given place.
     */
    private ArrayList<eSTPlace> getRelatedPlaces(eSTPlace place, ArrayList<eSTPlace> places) {
        ArrayList<eSTPlace> result = new ArrayList<>();

        for (eSTPlace currPlace : places) {
            if (hasCommonTransition(place.getInputTransitionKey(), currPlace.getInputTransitionKey())) {
                result.add(currPlace);
            }
        }

        return result;
    }

    /**
     * Check whether the keys contain at least one common transitions.
     *
     * @param transitionKey1 Bytewise coded transitions key of a place.
     * @param transitionKey2 Bytewise coded transitions key of a place.
     * @return True, if both keys have a transition in common.
     */
    private boolean hasCommonTransition(int transitionKey1, int transitionKey2) {
        return ((transitionKey1 & transitionKey2) > 0);
    }

    /**
     * Checks if a sub- or superregion relation exists between both places.
     *
     * @param place1     The first place to be compared.
     * @param place2     The second place to be compared.
     * @param reducedLog A log for which the variants and their number of occurrences have already been calculated.
     * @return -1, if first place is a subregion of the second place.
     * 0, if both places have no sub- or superregion relation.
     * 1, if first place is a superregion of the second place.
     */
    private Integer testSubregionRelation(final eSTPlace place1, final eSTPlace place2, final eSTLog reducedLog) {
        // Stores the variants and their number of occurrences.
        LinkedList<LinkedList<Integer>> traceVariants = reducedLog.getTraceVariants();
        HashMap<LinkedList<Integer>, Integer> traceVariantCounts = reducedLog.getTraceVariantCounts();

        // Result is first set to 2 as
        int result = 2;

        for (LinkedList<Integer> trace : traceVariants) {
            // Skipping all variants that are non replayable.
            if (traceVariantCounts.get(trace) > 0) {
                // Using copys of the places to not modify them.
                eSTPlace tempPlace1 = place1.copy();
                eSTPlace tempPlace2 = place2.copy();

                // Replaying all events in the trace.
                for (Integer event : trace) {

                    // Comparing markings after firing outgoing transitions.
                    tempPlace1.consumefire(getMask(event, transitions));
                    tempPlace2.consumefire(getMask(event, transitions));

                    // Stores intermediate result, if there if for a transition, the sub- or superregion property holds and no
                    // property is stored yet or the same evidence for the same property is found.
                    // If there is no relation for a transition, nothing is happening.
                    // If the superregion property holds for a transition and for another transition the subregion property, 0 is returned immediately.
                    switch (compareMarkings(tempPlace1, tempPlace2)) {
                        case -1:
                            if (result == 2 || result == -1) { // First or consistent result
                                result = -1;
                            } else {
                                return 0;
                            }
                            break;
                        case 0:
                            break; //no information gain
                        case 1:
                            if (result == 2 || result == 1) { // First or consistent result
                                result = 1;
                            } else {
                                return 0;
                            }
                    }

                    // Comparing markings after firing ingoing transitions.
                    tempPlace1.producefire(getMask(event, transitions));
                    tempPlace2.producefire(getMask(event, transitions));

                    // Stores intermediate result, if there if for a transition, the sub- or superregion property holds and no
                    // property is stored yet or the same evidence for the same property is found.
                    // If there is no relation for a transition, nothing is happening.
                    // If the superregion property holds for a transition and for another transition the subregion property, 0 is returned immediately.
                    switch (compareMarkings(tempPlace1, tempPlace2)) {
                        case -1:
                            if (result == 2 || result == -1) { // First or consistent result
                                result = -1;
                            } else {
                                return 0;
                            }
                            break;
                        case 0:
                            break; //no information gain
                        case 1:
                            if (result == 2 || result == 1) { // First or consistent result
                                result = 1;
                            } else {
                                return 0;
                            }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Compares the number of current tokens for two places.
     *
     * @param place1 The first place to be compared.
     * @param place2 The second place to be compared.
     * @return -1, if first place has currently less tokens than the second place.
     * 0, if first place has currently the same number of tokens as the second place.
     * 1, if first place has currently more tokens than the second place.
     */
    private Integer compareMarkings(eSTPlace place1, eSTPlace place2) {
        return Integer.compare(place1.getCurrentTokens(), place2.getCurrentTokens());
    }
}