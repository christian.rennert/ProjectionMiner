package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.eSTLog;

import java.util.ArrayList;

public class ILPBasedImplicitPlaceRemover extends ImplicitPlaceRemover {

    @Override
    public eSTProcessModel removeAllIPs(eSTProcessModel processModel, eSTLog log) {
        return null;
    }

    @Override
    public ArrayList<eSTPlace> removeImplicitsRelatedToPlace(eSTPlace specificPlace, eSTProcessModel processModel, eSTLog log) {
        return null;
    }

    @Override
    public String toString() {
        return "ILP - not yet implemented";
    }
}
