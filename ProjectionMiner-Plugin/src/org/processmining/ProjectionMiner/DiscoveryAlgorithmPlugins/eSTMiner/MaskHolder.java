package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import java.util.ArrayList;

public class MaskHolder {
    private static MaskHolder instance;
    private ArrayList<Integer> masks;

    private MaskHolder() {
        masks = new ArrayList<>();

        for (int i = 0; i < Integer.SIZE - 1; i++) {
            masks.add(1 << i);
        }
    }

    public static MaskHolder getInstance() {
        if (instance == null) {
            return instance = new MaskHolder();
        }
        return instance;
    }

    public ArrayList<Integer> getMasks() {
        return masks;
    }
}
