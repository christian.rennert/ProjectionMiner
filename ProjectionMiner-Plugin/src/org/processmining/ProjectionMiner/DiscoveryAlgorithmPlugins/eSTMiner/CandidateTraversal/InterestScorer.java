package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Class for the computation and storage of interestScores.
 */
public class InterestScorer {
    private static InterestScorer instance;

    private int numbOfTransitions;
    private boolean[][] isMeetingThreshold;

    /**
     * Method to get the instance of the Singleton pattern.
     *
     * @return The instance of the static class.
     */
    public static InterestScorer getInstance() {
        if (instance == null) {
            instance = new InterestScorer();
        }
        return instance;
    }

    /**
     * Sets the number of transitions in the log and creates the matrix for the interest scores.
     *
     * @param numbOfTransitions The number of transitions.
     */
    public void setNumbOfTransitions(int numbOfTransitions) {
        this.numbOfTransitions = numbOfTransitions;
        isMeetingThreshold = new boolean[numbOfTransitions][numbOfTransitions];
    }

//  /**
//   * Computes and stores the interestScores for all dependencies in the log.
//   *
//   * @param traceVariants A log containing the transitions coded by their indices.
//   */
//  public void computeValues(ArrayList<ArrayList<Integer>> traceVariants, HashMap<ArrayList<Integer>, Integer> traceVariantCounts, double threshold) {
//    ArrayList<ArrayList<Integer>> logCopy = new ArrayList<>(traceVariants);
//    double[][] eventuallyFollowRelationCounter = new double[numbOfTransitions][numbOfTransitions];
//    int[][] commonOccurrences = new int[numbOfTransitions][numbOfTransitions];
//
//    // Counting the eventually follow relations.
//    for (ArrayList<Integer> trace : logCopy) {
//      HashSet<Integer> visitedActivities = new HashSet<>();
//      HashSet<Integer> duplicateActivities = new HashSet<>();
//
//      // For each visited activity, we store the index.
//      // Otherwise, for each newly visited activity, we increment the eventually follows relation of each activity visited before
//      // to the current activity.
//      for (Integer activity : trace) {
//        if (!visitedActivities.contains(activity)) {
//
//          // Incrementing the eventually follows relations for each newly visited activity.
//          HashSet<Integer> updatedEventuallyFollows = new HashSet<>();
//
//          for (Integer vActivity : visitedActivities) {
//            if (!updatedEventuallyFollows.contains(vActivity)) {
//              eventuallyFollowRelationCounter[vActivity][activity] = eventuallyFollowRelationCounter[vActivity][activity] + traceVariantCounts.get(trace);
//              updatedEventuallyFollows.add(vActivity);
//            }
//          }
//
//          visitedActivities.add(activity);
//        } else {
//          duplicateActivities.add(activity);
//        }
//      }
//
//      // Incrementing the duplicate transitions individually.
//      for (int duplicate : duplicateActivities) {
//        eventuallyFollowRelationCounter[duplicate][duplicate] = eventuallyFollowRelationCounter[duplicate][duplicate] + traceVariantCounts.get(trace);
//      }
//    }
//
//    // Checking if the number of eventually follow relations meet the threshold.
//    for (int i = 0; i < numbOfTransitions; i++) {
//      for (int j = 0; j < numbOfTransitions; j++) {
//        if (threshold > 0) {
//          isMeetingThreshold[i][j] = (eventuallyFollowRelationCounter[i][j] >= threshold);
//        } else if (threshold == 0) {
//          isMeetingThreshold[i][j] = (eventuallyFollowRelationCounter[i][j] > threshold);
//        } else {
//          System.out.println("No valid threshold for the evaluation of the interest scores set.");
//          System.exit(-1);
//        }
//      }
//    }
//  }

    /**
     * Computes and stores the interestScores for all dependencies in the log.
     *
     * @param traceVariants A log containing the transitions coded by their indices.
     */
    public void computeValues(LinkedList<LinkedList<Integer>> traceVariants, HashMap<LinkedList<Integer>, Integer> traceVariantCounts, double threshold) {
        LinkedList<LinkedList<Integer>> logCopy = new LinkedList<>(traceVariants);

        double[][] eventuallyFollowRelationCounter = new double[numbOfTransitions][numbOfTransitions];
        int[][] commonOccurrences = new int[numbOfTransitions][numbOfTransitions];

        // Counting the eventually follow relations.
        for (LinkedList<Integer> trace : logCopy) {
            LinkedList<Integer> traceCopy = new LinkedList<>(trace);
            HashSet<Integer> previousIndices = new HashSet<>();

            while (traceCopy.size() > 0) {
                int currIndex = traceCopy.removeFirst();

                if (!previousIndices.contains(currIndex)) {
                    previousIndices.add(currIndex);

                    HashSet<Integer> followingIndices = new HashSet<>();
                    for (Integer followingIndex : traceCopy) {
                        followingIndices.add(followingIndex);
                    }

                    for (Integer followingIndex : followingIndices) {
                        eventuallyFollowRelationCounter[currIndex][followingIndex] = eventuallyFollowRelationCounter[currIndex][followingIndex] + traceVariantCounts.get(trace);
                    }
                }
            }

            for (Integer i : previousIndices) {
                for (Integer j : previousIndices) {
                    commonOccurrences[i][j] = commonOccurrences[i][j] + traceVariantCounts.get(trace);
                }
            }
        }

        // Checking if the number of eventually follow relations meet the threshold.
        for (int i = 0; i < numbOfTransitions; i++) {
            for (int j = 0; j < numbOfTransitions; j++) {
                if (threshold > 0) {
                    isMeetingThreshold[i][j] = ((eventuallyFollowRelationCounter[i][j] / commonOccurrences[i][j]) >= threshold);
                } else if (threshold == 0) {
                    isMeetingThreshold[i][j] = ((eventuallyFollowRelationCounter[i][j] / commonOccurrences[i][j]) > threshold);
                } else {
                    System.out.println("No valid threshold for the evaluation of the interest scores set.");
                    System.exit(-1);
                }
            }
        }
    }

    /**
     * Checks for the lambda-interesting property.
     *
     * @param place The place to be checked for lambda-interesting property.
     * @return True, if a place is lambda-interesting.
     */
    public boolean isLambdaInteresting(eSTPlace place) {
        ArrayList<Integer> ingoingTransitionIndices = new ArrayList<>();
        ArrayList<Integer> outgoingTransitionIndices = new ArrayList<>();

        // Computing the indices of the transitions by iterating through the place mask.
        int inputTransitionKey = place.getInputTransitionKey();
        int outputTransitionKey = place.getOutputTransitionKey();
        for (int transitionIndex = 0; transitionIndex < numbOfTransitions; transitionIndex++) {
            if ((inputTransitionKey & place.createMask(transitionIndex, numbOfTransitions)) > 0) {
                ingoingTransitionIndices.add(transitionIndex);
            }
            if ((outputTransitionKey & place.createMask(transitionIndex, numbOfTransitions)) > 0) {
                outgoingTransitionIndices.add(transitionIndex);
            }
        }

        // Checking if there is enough evidence for all outgoing transitions.
        for (int ingoingTransitionIndex : ingoingTransitionIndices) {
            for (int outgoingTransitionIndex : outgoingTransitionIndices) {
                if (!isMeetingThreshold[ingoingTransitionIndex][outgoingTransitionIndex]) {
                    return false;
                }
            }
        }

        return true;
    }
}