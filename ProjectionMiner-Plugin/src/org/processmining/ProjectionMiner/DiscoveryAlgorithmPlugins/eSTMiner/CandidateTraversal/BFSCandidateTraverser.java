package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceFactory;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceState;

import java.util.ArrayList;
import java.util.Collection;


/*
 * This class public method getNext() returns the next candidate place based on
 * the given candidate place.
 * It aims for a BFS search of the candidate tree by maintaining a queue of subtree roots: the first root is evaluated, and its children (next level)
 *  are either cut-off (ignored) or appended at the end of the queue. This results in the current level being evaluated first, before the next level is reached.
 */

// TODO untested

//ASSUME THAT POS 0 OF TRANSITIONS IS END, POS 0 OF OUTMAPPING MAPS TO START
public class BFSCandidateTraverser extends CandidateTraverser {

    public BFSCandidateTraverser() {

    }


    //returns the next place based on the given last place and additional information
    //uses a tree structure ordered according to transitions array (ingoing) and mapping (outgoing)
    //Makes use of the class variables roots and current root to keep track of tree traversal
    //Cuts off uninteresting branches based on fitness, other criteria planned
    //For a given place, add valid (not cut-off) children to roots queue, then return the next place (roots queue ordering ensures level by level traversal)
    //Return null when queue is empty
    public eSTPlace getNext(eSTPlace lastPlace, eSTPlaceState fitness) {
        //case first place
        if (lastPlace == null) {
//				System.out.println("Roots:");
//				System.out.println(placeSetToString(roots));
            return currentRoot;
        }
//			fitness = MyPlaceStatus.UNKNOWN; //for debugging
        ArrayList<eSTPlace> newChildren = addValidChildrenToQueue(lastPlace, fitness);
        eSTPlace nextP = null;
/*			System.out.println("---------------");
			System.out.println("Last Place: "+lastP.toTransitions(transitions));
			rootsPlacesNames = rootsPlacesNames + "\n" + placeSetToString(newChildren);
			System.out.println("Roots: "+ (roots.size()-newChildren.size()) +"+"+ newChildren.size());
			System.out.println(placeSetToString(roots)+"\n"+placeSetToString(newChildren));
*/
        nextP = getNextRoot();
//			System.out.println("Next Place: "+nextP.toTransitions(transitions));			
        return nextP;
    }

    @Override
    public String toString() {
        return "Breadth-first search";
    }

    @Override
    public boolean hasLambda() {
        return false;
    }


    private ArrayList<eSTPlace> addValidChildrenToQueue(eSTPlace place, eSTPlaceState fitness) {
        ArrayList<eSTPlace> newChildren = new ArrayList<eSTPlace>();
        //add valid children based on fitness (first outgoing, then ingoing)
        if (fitness != eSTPlaceState.UNDERFED && fitness != eSTPlaceState.MALFED) {
            newChildren.addAll(getValidOutChildren(place));
        }
        if ((fitness != eSTPlaceState.MALFED && fitness != eSTPlaceState.OVERFED) || !hasSingleMaximalOutTransition(place)) //asymetric pruning of overfed places
            newChildren.addAll(getValidInChildren(place));
        this.roots.addAll(newChildren);
        return newChildren;
    }


    //assume fitness and tree level have been tested, other criteria still need to be checked
    private Collection<? extends eSTPlace> getValidInChildren(eSTPlace place) {
        ArrayList<eSTPlace> inChildren = new ArrayList<eSTPlace>();
        if (Integer.bitCount(place.getOutputTransitionKey()) > 1) {
            return inChildren; //this place has no in transition children (more than one out transition)
        }
        int nextOutKey = place.getOutputTransitionKey();
        int lastInKey = place.getInputTransitionKey();
        int largestInIndex = getLargestInTrIndex(lastInKey);
        for (int i = largestInIndex + 1; i < transitions.length; i++) {
            int nextInKey = (lastInKey | getMask(i));
            inChildren.add(eSTPlaceFactory.getInstance().createPlace(nextInKey, nextOutKey));
        }
        return inChildren;
    }

    //assume fitness and tree level have been tested, other criteria still need to be checked
    private Collection<? extends eSTPlace> getValidOutChildren(eSTPlace place) {
        ArrayList<eSTPlace> outChildren = new ArrayList<eSTPlace>();
        int nextInKey = place.getInputTransitionKey();
        int lastOutKey = place.getOutputTransitionKey();
        int largestOutMappingIndex = getLargestOutgoingTransitionIndex(lastOutKey);
        for (int i = largestOutMappingIndex + 1; i < outgoingTransitionMapping.length; i++) {
            int nextOutKey = (lastOutKey | getMask(getMappedTransitionIndex(i)));
            outChildren.add(eSTPlaceFactory.getInstance().createPlace(nextInKey, nextOutKey));
        }
        return outChildren;
    }

}