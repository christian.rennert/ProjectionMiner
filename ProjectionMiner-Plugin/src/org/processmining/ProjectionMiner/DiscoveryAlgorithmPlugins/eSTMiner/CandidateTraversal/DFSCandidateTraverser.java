package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceFactory;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceState;


/**
 * Class for state management and traversal in the eST-Miner algorithm. Uses Depth-First-Search (DFS) to traverse places.
 */
public class DFSCandidateTraverser extends CandidateTraverser {

    public DFSCandidateTraverser() {

    }


    //uses a tree structure ordered according to transitions array (ingoing) and mapping (outgoing).
    //Makes use of the class variables roots and current root to keep track of tree traversal
    // Cuts off uninteresting branches based on fitness, other criteria planned
    //uniwired: cut of branches that would add a connection that already exists (TODO not implemented here, add special traverser for that)
    //Manage Traversal depth - for a given place, either
    // 1) return the next place (within traversal depth)
    // 2) return the next root (no valid children)
    // 3) add all valid children to root queue and return next root (end of travsersal depth)

    /**
     * For a given place and its fitness status, the next place to be visited is computed considering the traversal
     * strategy and the pruning.
     *
     * @param lastPlace The place, which we want to find the successor for.
     * @param fitness   The fitness status corresponding to the given place.
     * @return Null, if all candidate places where visited. Otherwise, the next candidate place according to the depth-first-search traversal strategy.
     */
    public eSTPlace getNext(eSTPlace lastPlace, eSTPlaceState fitness) {
        // If no place has been visited yet, we pick the root of the current sub search tree.
        if (lastPlace == null) {
            return currentRoot;
        }

        eSTPlace nextPlace;

        // Case 1: Next place is the descendant child of the last place
        nextPlace = findNextChild(lastPlace, fitness);

        // Case 2: Next place is contained in the same subtree of the current root but in a different branch
        if ((nextPlace.getInputTransitionKey() == 0) || (nextPlace.getOutputTransitionKey() == 0)) {
            nextPlace = findNextBranch(lastPlace);
        }

        // Case 3: Next place contained in new subtree and therefore we switch the to the next root.
        if ((nextPlace.getInputTransitionKey() == 0) || (nextPlace.getOutputTransitionKey() == 0)) {
            nextPlace = getNextRoot();

            // If there is no next root, there will also no other place to be visited.
            if (nextPlace == null) {
                nextPlace = eSTPlaceFactory.getInstance().createPlace(0, 0);
            }
        }

        // Case 4: No more places have to be visited.
        if ((nextPlace.getInputTransitionKey() == 0) || (nextPlace.getOutputTransitionKey() == 0)) {
            return null;
        }

        // Returns next place.
        return nextPlace;
    }

    @Override
    public String toString() {
        return "Depth-first search";
    }

    @Override
    public boolean hasLambda() {
        return false;
    }

    //try: find child of the last candidate node (deepen current branch), outchild first (NO backtracking)
    // possibly cut off uninteresting branches
    //possibly stop due to depth limit

    /**
     * @param lastPlace
     * @param fitness
     * @return
     */
    private eSTPlace findNextChild(final eSTPlace lastPlace, final eSTPlaceState fitness) {
        int nextInKey;
        int nextOutKey;

        nextOutKey = findNextOutKey(lastPlace.getInputTransitionKey(), lastPlace.getOutputTransitionKey(), fitness); //adds a new out-transitions to current candidate, returns 0 if impossible

        //
        if (nextOutKey != 0) {
            nextInKey = lastPlace.getInputTransitionKey();
        } else {
            nextInKey = findNextInKey(lastPlace.getInputTransitionKey(), lastPlace.getOutputTransitionKey(), fitness); //add new in-transition to current candidate (only possible if outkey has exactly one transition), returns 0 if impossible
            if (nextInKey != 0) {
                nextOutKey = lastPlace.getOutputTransitionKey(); //should contain exactly one transition, otherwise inkey should be 0
            }
        }

        return eSTPlaceFactory.getInstance().createPlace(nextInKey, nextOutKey);
    }


    //adds a new out transition to current candidate (NO BACKTRACKING), possibly cuts of current branch, returns 0 on fail
    //current implementation: increase inkey from left to right, outkey from right to left

    /**
     * @param lastInKey
     * @param lastOutKey
     * @param fitness
     * @return
     */
    private int findNextOutKey(final int lastInKey, final int lastOutKey, final eSTPlaceState fitness) {
        int nextOutKey = 0;
        //if path is not at end AND last place was underfed --> cut this path
        if (((lastOutKey & getMask(largestOutgoingIndex)) == 0) //last out transition is not added
                && ((fitness == eSTPlaceState.UNDERFED) || (fitness == eSTPlaceState.MALFED))) {//previous place was underfed
            return 0;
            //System.out.println("Cutting off underfed path!");
        }
        //if path is not cut, add output transition if possible
        // add only transitions larger than the largest current out transition
        // add transitions only if not already wired
        else if ((lastOutKey & getMask(largestOutgoingIndex)) == 0) {//ensure largest transition is not set
            int currentOutIndex = getLargestOutgoingTransitionIndex(lastOutKey);//find largest outIndex
            if (largestOutgoingIndex != getMappedTransitionIndex(currentOutIndex)) {//check whether there are still transitions to find
                int newOutTransition = getMappedTransitionIndex(currentOutIndex + 1);//the index of the new, unwired transition
                nextOutKey = (lastOutKey | getMask(newOutTransition));
            }
        }
        // TODO Check old
        return nextOutKey;
    }


    //adds a new in transition to current candidate (NO BACKTRACKING), possibly cuts of current branch, returns 0 on fail

    /**
     * @param lastInKey
     * @param lastOutKey
     * @param fitness
     * @return
     */
    private int findNextInKey(final int lastInKey, final int lastOutKey, final eSTPlaceState fitness) {
        int nextInKey = 0;

        // Ingoing transitions can only be added to places with exactly one output transition
        if (Integer.bitCount(lastOutKey) == 1) {
            //path is not at end AND no outkeys can be added, so overfedness of previous place can be exploited to cut branch
            if (((lastInKey & getMask(largestIngoingIndex)) == 0)
                    && (lastOutKey == getMask(largestOutgoingIndex))
                    && ((fitness == eSTPlaceState.OVERFED) || (fitness == eSTPlaceState.MALFED))) {
                //	System.out.println("Cutting off overfed path!");
            }
            // case input can be added (add only transitions larger than the largest in transition)
            // only add transitions that are not yet wired!
            else if ((lastInKey & getMask(largestIngoingIndex)) == 0) {//ensure largest transition isn't set
                int newInTransition = Integer.lowestOneBit(lastInKey);//finds the highest (rightmost) intransition
                //while the current transition can be increased search for feasable new transition
                if (newInTransition != getMask(largestIngoingIndex)) {
                    newInTransition = newInTransition >> 1;
                    nextInKey = lastInKey | newInTransition;
                }
            }
        }

        return nextInKey;
    }


    //assume the current candidate cannot have children
    //try: backtrack at most up to current root, to find a new branch of this subtree
    private eSTPlace findNextBranch(final eSTPlace lastP) {
        int lastInputKey = lastP.getInputTransitionKey();
        int lastOutputKey = lastP.getOutputTransitionKey();
        int nextInputKey = 0;
        int nextOutputKey = 0;

        nextOutputKey = backtrackOutKey(lastInputKey, lastOutputKey);
        if (nextOutputKey != 0) {//new outbranch could be found
            nextInputKey = lastInputKey;
        } else {//no outbranch could be found
            int rootOutTree = getMask(getMappedTransitionIndex(getLowestOutTrIndex(lastOutputKey))); //ensure only the smallest out-transition is left ("root" of this outkey)
            if (lastOutputKey != rootOutTree) {//check whether the root of this outtree admits a new inbranch before backtracking (if lastOutputKey was root, this has already been checked)
                nextInputKey = findNextInKey(lastInputKey, rootOutTree, eSTPlaceState.UNKNOWN);
            }
            if (nextInputKey == 0) {//this outkey "root" doesn't admit any children
                nextInputKey = backtrackInKey(lastInputKey, getOutTreeRoot(lastOutputKey));
            }
            if (nextInputKey != 0) {//new inbranch could be found
                nextOutputKey = rootOutTree;
            }
        }
        eSTPlace nextP = eSTPlaceFactory.getInstance().createPlace(nextInputKey, nextOutputKey);
        if (isInCurrentSubtree(nextP)) {
            return nextP;
        } else {//no more places in current subtree
            return eSTPlaceFactory.getInstance().createPlace(0, 0);
        }
    }

    /**
     * Checks for a given place, if it is contained in the current subtree.
     *
     * @param nextPlace An eSTPlace.
     * @return True, if the given place is a place in the current subtree.
     */
    private boolean isInCurrentSubtree(eSTPlace nextPlace) {
        return isSubset(currentRoot.getInputTransitionKey(), nextPlace.getInputTransitionKey())
                && isSubset(currentRoot.getOutputTransitionKey(), nextPlace.getOutputTransitionKey());
    }

    /**
     * Checks if each transition in the first set of transitions is also contained in the second set.
     *
     * @param subsetCandidate   Candidate for being checked, if it is a subset of the superset of transitions.
     * @param supersetCandidate Candidate for being checked, if it is a superset of the subset of transitions.
     * @return True, if and only if each transition in the subset candidate is also contained in the superset candidate of transitions.
     */
    private boolean isSubset(int subsetCandidate, int supersetCandidate) {
        return (subsetCandidate & supersetCandidate) == subsetCandidate;
    }

    //based on the last outkey, backtracks the outbranch until a new outbranch is found, returns 0 on fail
    //assume no children can be added to current candidate
    private int backtrackOutKey(int lastInKey, int lastOutKey) {
        int nextOutKey = 0;
        if (Integer.bitCount(lastOutKey) > 1) {//if there is only one out transition, backtracking is impossible
            int lastOutIndex = getLargestOutgoingTransitionIndex(lastOutKey);//find the largest remaining out transition of the last candidate
            lastOutKey = getMask(getMappedTransitionIndex(lastOutIndex)) ^ lastOutKey;//remove this largest transition to get back to candidates' parent
            // Case 1 (getTransitionIndex(lastOutIndex) != largestOutIndex): the last branch was cut, this parent has another outbranch (last outbranch is always exactly one candidate and cannot be cut)
            if (getMappedTransitionIndex(lastOutIndex) != largestOutgoingIndex) {
                return (lastOutKey | getMask(getMappedTransitionIndex(lastOutIndex + 1))); //return next suitable outbranch of the parent
            }
            //case 2: the last branch was at it's end, so this parent cannot have other out branches
            //--> try to find the grandparent (parent of this parent) and find a new out branch of this grandparent
            if ((nextOutKey == 0) && (Integer.bitCount(lastOutKey) > 1)) { //if this parent has only one out transition, further backtracking is impossible
                lastOutIndex = getLargestOutgoingTransitionIndex(lastOutKey);//find the largest remaining out transition of the last candidate
                lastOutKey = getMask(getMappedTransitionIndex(lastOutIndex)) ^ lastOutKey;//remove this transition to get back to candidates' grandparent

                if (getMappedTransitionIndex(lastOutIndex) != largestOutgoingIndex) {
                    return (lastOutKey | getMask(getMappedTransitionIndex(lastOutIndex + 1))); //return next suitable outbranch of the parent
                }
            }
        }
        return nextOutKey;
    }


    //based on the last inkey, backtracks the inbranch until new inbranch is found, returns 0 on fail
    //assume no children can be added to current candidate
    private int backtrackInKey(int lastInKey, int lastOutKey) {
        if (Integer.bitCount(lastOutKey) > 1) {
            System.out.println("Error: backtrackInKey expects outkey of size 1!");
        }
        int nextInKey = 0;
        if (Integer.bitCount(lastInKey) > 1) {//if there is only one intransition backtracking is impossible
            int lastTransition = Integer.lowestOneBit(lastInKey); //find the largest remaining in transition of the last candidate
            lastInKey = lastTransition ^ lastInKey;//remove this largest transition to get back to candidates' parent
            //Case 1: last inbranch was cut, find next inbranch of this parent (there is at least one)
            if (lastTransition != 1) {
                lastTransition = lastTransition >> 1;
                return (lastInKey | lastTransition); //add transition
            }
            //Case 2: this inbranch is at it's end, parent cannot have other inbranch
            //--> find parent of this parent (grandparent of this candidate) and its next inbranch
            if ((nextInKey == 0) && (Integer.bitCount(lastInKey) > 1)) { //if parent has only one tranistion further backtracking is impossible
                lastTransition = Integer.lowestOneBit(lastInKey); //find the largest remaining in transition of this parent
                lastInKey = lastTransition ^ lastInKey;//remove this largest transition to get back to candidates' grandparent
                if (lastTransition != 1) {
                    lastTransition = lastTransition >> 1;
                    return (lastInKey | lastTransition); //add transition
                }
            }
        }
        return nextInKey;
    }

    private int getOutTreeRoot(int lastOutKey) {
        return getMask(getMappedTransitionIndex(getLowestOutTrIndex(lastOutKey))); //ensure only the smallest out-transition is left ("root" of this outkey)
    }
}