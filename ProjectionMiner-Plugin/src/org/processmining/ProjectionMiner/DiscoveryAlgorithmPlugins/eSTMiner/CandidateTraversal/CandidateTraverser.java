package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceFactory;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceState;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Abstract class for candidate traversal classes.
 */
public abstract class CandidateTraverser {
    // Array of transition names corresponding to ingoing ordering.
    // At the first position is the end transition.
    protected String[] transitions;

    // Array of indices (of position in transitions array) linking to transition names corresponding to outorder.
    // The start transition is mapped to the first position.
    protected int[] outgoingTransitionMapping;

    // Holds the highest index of the ingoing and outgoing transition array.
    protected int largestIngoingIndex;
    protected int largestOutgoingIndex;

    // Root management
    protected ArrayList<eSTPlace> roots;
    protected eSTPlace currentRoot;

    public CandidateTraverser() {
    }

    /**
     * Stores the in- and outgoing transitions in the class object and computes the roots of the search tree.
     *
     * @param transitions               The array containing the ingoing transitions.
     * @param outgoingTransitionMapping The mapping of the positions in the ingoing transitions array to the positions in the outgoing array.
     */
    public void init(final String[] transitions, final int[] outgoingTransitionMapping) {
        this.transitions = transitions;
        this.outgoingTransitionMapping = outgoingTransitionMapping;

        // Computing the highest index in the in- and outgoing transition array.
        this.largestIngoingIndex = transitions.length - 1;
        this.largestOutgoingIndex = getMappedTransitionIndex(outgoingTransitionMapping.length - 1); //largest out index can be mapped anywhere in the transitions array

        // Computes the roots of the search tree and picks the first root.
        roots = computeBaseRoots();
        currentRoot = roots.remove(0);
    }

    /**
     * For a given place and its fitness status, the next place to be visited is computed considering the traversal
     * strategy and the pruning.
     *
     * @param lastPlace The place, which we want to find the successor for.
     * @param fitness   The fitness status corresponding to the given place.
     * @return Null, if all candidate places where visited. Otherwise, the next candidate place according to the traversal strategy.
     */
    public abstract eSTPlace getNext(eSTPlace lastPlace, eSTPlaceState fitness);

    public abstract String toString();

    /**
     * Computes all base roots, which are places containing exactly one ingoing and outgoing transition.
     * Note, that we are skipping the first positions in the in- and outgoing transitions array, as they both hold the
     * start and end activity, which do not need to be considered. (i.e. the first root cannot be (■|*anything*))
     *
     * @return The base roots of the search tree.
     */
    protected ArrayList<eSTPlace> computeBaseRoots() {
        ArrayList<eSTPlace> baseRoots = new ArrayList<>();

        for (int in = 1; in < transitions.length; in++) {
            for (int out = 1; out < transitions.length; out++) {
                baseRoots.add(eSTPlaceFactory.getInstance().createPlace(getMask(in), getMask(getMappedTransitionIndex(out))));
            }
        }

        return baseRoots;
    }

    /**
     * Constructs a String for a list of places containing the ingoing and outgoing transitions for each place.
     *
     * @param placeList A list containing eSTPlaces.
     * @return A string describing all given places.
     */
    protected String placeSetToString(ArrayList<eSTPlace> placeList) {
        String result = "";

        for (eSTPlace place : placeList) {
            result = result + place.toTransitionsString(transitions);
        }

        return result;
    }


    /**
     * For an index of a transition in the ingoing transitions array it returns the position in the outgoing transition array.
     *
     * @param index The index of a transition in the ingoing transitions array.
     * @return The index of the transition in the outgoing transitions array.
     */
    protected int getMappedTransitionIndex(int index) {
        return outgoingTransitionMapping[index];
    }


    /**
     * Computes the rightmost transition (with the highest ordering) in the outgoing transition mapping array for a given key of transitions.
     *
     * @param outKey A bytewise coding of outgoing transitions.
     * @return The index in the outgoing transition mapping array if a suitable result is found. Otherwise the result is zero.
     */
    protected int getLargestOutgoingTransitionIndex(int outKey) {
        for (int i = outgoingTransitionMapping.length - 1; i >= 0; i--) {
            if ((getMask(outgoingTransitionMapping[i]) & outKey) > 0) {
                return i;
            }
        }

        return 0;
    }

    /**
     * Computes the leftmost transition (with the lowest ordering) in the outgoing transition mapping array for a given key of transitions.
     *
     * @param outKey A bytewise coding of outgoing transitions.
     * @return The index in the outgoing transition mapping array if a suitable result is found. Otherwise the result is zero.
     */
    protected int getLowestOutTrIndex(int outKey) {
        for (int i = 0; i < outgoingTransitionMapping.length; i++) {
            if ((getMask(getMappedTransitionIndex(i)) & outKey) > 0) {
                return i;
            }
        }

        return 0;
    }

    /**
     * Computes the rightmost transition (with the highest ordering) in the ingoing mapping array for a given key of transitions.
     *
     * @param inKey A bytewise coding of ingoing transitions.
     * @return The index in the ingoing transitions array if a suitable result is found. Otherwise the result is zero.
     */
    protected int getLargestInTrIndex(int inKey) {
        for (int i = transitions.length - 1; i >= 0; i--) {
            if ((getMask(i) & inKey) > 0) {
                return i;
            }
        }

        return 0;
    }

    /**
     * Checks whether the key of the output transitions of a place correspond to the index of the rightmost transition in
     * the outgoing transitions array. Can be used to test whether a place is eligible for overfed pruning.
     *
     * @param place An eST Place.
     * @return True, if and only if the places only outgoing transition is the transition with largest outgoing transition index.
     */
    protected boolean hasSingleMaximalOutTransition(eSTPlace place) {
        return place.getOutputTransitionKey() == getMask(largestOutgoingIndex);
    }

    /**
     * Pops first element in the roots list and sets the currentRoot to the the removed root.
     *
     * @return The new current root. If none existing returning null.
     */
    protected eSTPlace getNextRoot() {
        if (!roots.isEmpty()) {
            currentRoot = roots.remove(0);
            return currentRoot;
        }

        return null;
    }

    //for a given position in the (ingoing) transition array return the corresponding bitmask

    /**
     * For a given position in the (ingoing) transition array computes the corresponding bitmask.
     *
     * @param pos Position in the (ingoing) transition array.
     * @return A bitmask for the given position in the array.
     */
    public int getMask(final int pos) {
        return 1 << (transitions.length - 1 - pos);
    }


    /**
     * Computes a collection containing all transitions names for the given key from the given transitions array.
     *
     * @param key         Bytewise representation of transitions.
     * @param transitions A transitions array containing the names of the transitions.
     * @return All transitions, that correspond to the key.
     */
    public Collection<String> getTransitionNames(final int key, final String[] transitions) {
        Collection<String> result = new ArrayList<>();

        // Check for invalid key
        if (key > (Math.pow(2, transitions.length))) {
            return null;
        }

        // Check each suitable transition.
        for (int i = 0; i < transitions.length; i++) {
            if ((key & getMask(i)) > 0) {
                result.add(transitions[i]);
            }
        }

        return result;
    }

    public abstract boolean hasLambda();
}