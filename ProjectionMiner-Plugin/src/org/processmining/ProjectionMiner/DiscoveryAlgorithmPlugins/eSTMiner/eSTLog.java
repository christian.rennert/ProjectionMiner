package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import org.apache.commons.lang3.ArrayUtils;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.TransitionOrderingResult;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.TransitionOrderingStrategy;

import java.util.*;

/**
 * A class to manage the log.
 */
public class eSTLog {
    // The imported XES log containing all traces.
    private final XLog xLog;

    // Trace statistics.
    private int numberOfTraces;
    private LinkedList<LinkedList<Integer>> traceVariants; //transitions are encoded as integers according to the positioning in the (ingoing) transitions array
    private HashMap<LinkedList<Integer>, Integer> traceVariantCounts;

    // Transition information.
    private String[] inTransitions;
    private final String[] outTransitions;
    private final int[] outTransitionMapping;

    // The start and end activity according to the eST-Miner algorithm.
    private XEvent startActivity;
    private XEvent endActivity;

    // The position of the endActivity in the inTransitions and the startActivity in the outTransitions
    private int inEndIndex;
    private int outStartIndex;

    // Flag to get Unique Start and End labels by adding the current time or without time tag
    private final boolean useUniqueNames;

    // TODO
    private HashMap<String, Integer> transitionOccurences;
    private HashMap<String, Integer> transitionAverageIndices;

    public eSTLog(eSTLog log) {
        this.xLog = log.xLog;
        this.numberOfTraces = log.numberOfTraces;
        this.traceVariants = log.traceVariants;
        this.traceVariantCounts = log.traceVariantCounts;
        this.inTransitions = log.inTransitions;
        this.outTransitions = log.outTransitions;
        this.outTransitionMapping = log.outTransitionMapping;
        this.startActivity = log.startActivity;
        this.endActivity = log.endActivity;
        this.inEndIndex = log.inEndIndex;
        this.outStartIndex = log.outStartIndex;
        this.useUniqueNames = log.useUniqueNames;
        this.transitionOccurences = log.transitionOccurences;
        this.transitionAverageIndices = log.transitionAverageIndices;
    }

    public eSTLog(int numberOfTraces, LinkedList<LinkedList<Integer>> traceVariants, HashMap<LinkedList<Integer>, Integer> traceVariantCounts,
                  String[] inTransitions, String[] outTransitions, int[] outTransitionMapping) {
        this.xLog = null;
        this.numberOfTraces = numberOfTraces;
        this.traceVariants = traceVariants;
        this.traceVariantCounts = traceVariantCounts;
        this.inTransitions = inTransitions;
        this.outTransitions = outTransitions;
        this.outTransitionMapping = outTransitionMapping;
        this.startActivity = null;
        this.endActivity = null;
        this.inEndIndex = 0;
        this.outStartIndex = 0;
        this.useUniqueNames = false;
        this.transitionOccurences = null;
        this.transitionAverageIndices = null;
    }

    /**
     * Used for setting up a log with own traceVariants and traceVariantCounts in recomputed.
     *
     * @param log
     * @param traceVariants
     * @param traceVariantCounts
     */
    public eSTLog(eSTLog log, LinkedList<LinkedList<Integer>> traceVariants, HashMap<LinkedList<Integer>, Integer> traceVariantCounts) {
        this.xLog = null;
        inTransitions = log.inTransitions;

        int numberOfTraces = 0;
        for (int val : traceVariantCounts.values()) {
            numberOfTraces += val;
        }
        this.numberOfTraces = numberOfTraces;

        HashSet<Integer> remainingTransitions = new HashSet<>();

        for (LinkedList<Integer> variant : traceVariants) {
            for (int event : variant) {
                remainingTransitions.add(event);
            }
        }

        System.out.println(traceVariants);
        System.out.println(remainingTransitions);
        System.out.println(Arrays.asList(log.inTransitions));
        System.out.println(Arrays.toString(log.outTransitionMapping));
        System.out.println(Arrays.asList(log.outTransitions));

        String[] newInTransitions = new String[remainingTransitions.size()];
        int[] newOutTransitionMapping = new int[remainingTransitions.size()];
        String[] newOutTransitions = new String[remainingTransitions.size()];

        // Finding the inTransitions that are still contained
        int k = 0;
        for (int i = 0; i < log.inTransitions.length; i++) {
            if (remainingTransitions.contains(i)) {
                newInTransitions[k] = log.inTransitions[i];
                newOutTransitionMapping[k] = log.outTransitionMapping[i];
                k++;
            }
        }

        // Recompute the values for the mapping, s.t. the order is the same and each number exists.
        for (int i = 0; i < newOutTransitionMapping.length; i++) {
            boolean notContained = true;
            for (int l : newOutTransitionMapping) {
                if (l == i) {
                    notContained = false;
                    break;
                }
            }

            if (notContained) {
                for (int l = 0; l < newOutTransitionMapping.length; l++) {
                    if (newOutTransitionMapping[l] > i) {
                        newOutTransitionMapping[l]--;
                    }
                }
            }
        }

        for (int i = 0; i < newOutTransitionMapping.length; i++) {
            boolean valueContained = false;
            int lowestValuePos = -1;
            for (int j = 0; j < newOutTransitionMapping.length; j++) {
                if (newOutTransitionMapping[j] == i) {
                    valueContained = true;
                    continue;
                }

                if (newOutTransitionMapping[j] > i) {
                    if (lowestValuePos == -1 || newOutTransitionMapping[j] < newOutTransitionMapping[lowestValuePos]) {
                        lowestValuePos = j;
                    }
                }
            }

            if (!valueContained) {
                newOutTransitionMapping[lowestValuePos] = i;
            }
        }


        // Compute the new outTransitions
        for (int i = 0; i < remainingTransitions.size(); i++) {
            newOutTransitions[newOutTransitionMapping[i]] = newInTransitions[i];
        }

        System.out.println(Arrays.asList(newInTransitions));
        System.out.println(Arrays.toString(newOutTransitionMapping));
        System.out.println(Arrays.asList(newOutTransitions));

        this.inTransitions = newInTransitions;
        this.outTransitions = newOutTransitions;
        this.outTransitionMapping = newOutTransitionMapping;

        HashMap<Integer, Integer> mappingBetweenTransitionPositions = new HashMap<>();
        for (int i = 0; i < inTransitions.length; i++) {
            for (int j = 0; j < log.inTransitions.length; j++) {
                if (Objects.equals(inTransitions[i], log.inTransitions[j])) {
                    mappingBetweenTransitionPositions.put(j, i);
                    break;
                }
            }
        }

        LinkedList<LinkedList<Integer>> newTraceVariants = new LinkedList<>();
        HashMap<LinkedList<Integer>, Integer> newTraceVariantCounts = new HashMap<>();

        for (LinkedList<Integer> variant : traceVariants) {
            LinkedList<Integer> newVariant = new LinkedList<>();

            for (int oldEvent : variant) {
                newVariant.add(mappingBetweenTransitionPositions.get(oldEvent));
            }

            newTraceVariants.add(newVariant);
            newTraceVariantCounts.put(newVariant, traceVariantCounts.get(variant));
        }

        this.traceVariants = newTraceVariants;
        this.traceVariantCounts = newTraceVariantCounts;

        this.startActivity = null;
        this.endActivity = null;
        this.inEndIndex = 0;
        this.outStartIndex = 0;
        this.useUniqueNames = false;
        this.transitionOccurences = null;
        this.transitionAverageIndices = null;
    }

    /**
     * Creates a new eSTLog object and computes additional information out of the log.
     * Transitions are converted to integers according to their position in the (ingoing) transitions array
     *
     * @param inputLog   The read XES-Log.
     * @param parameters The settings for the eST-Miner algorithm.
     */
    public eSTLog(XLog inputLog, Parameters parameters) {
        xLog = inputLog;

        useUniqueNames = parameters.shouldUseUniqueNames();

        // Adding the start and end transition
        addUniqueStartEnd();

        // Computing additional information.
        TransitionOrderingStrategy transitionOrderingStrategy;

        transitionOrderingStrategy = parameters.getTransitionOrderingStrategy();
        String[] transitionsFromLog = getTransitionsFromLog(parameters);
        transitionOrderingStrategy.setTransitions(transitionsFromLog);

        transitionOrderingStrategy.computeHeuristics(xLog, parameters);
        TransitionOrderingResult transitionResult = transitionOrderingStrategy.apply();

        inTransitions = moveEndActivityToFirstPos(parameters, transitionResult.getInTransitions());
        outTransitions = moveStartActivityToFirstPos(parameters, transitionResult.getOutTransitions());
        outTransitionMapping = computeOutTransitionMapping();

//    transitionOccurences = computeTransitionsOccurences(parameters.getEventClassifier()); // TODO compute only when needed
//    transitionAverageIndices = computeAverageTransitionIndices(parameters.getEventClassifier()); // TODO compute only when needed


        // Computes the list of trace variants and the HashMap of their frequencies.
        computeFinalLogObjects(parameters);
    }


    /**
     * Reduces the frequency of non-replayable vaiants to zero, but still keeps all variants to ensure
     * compatible mapping to variant vectors etc.
     *
     * @param variantVector The array of variants, which can be replayed.
     */
    public void reduceToReplayableVariants(boolean[] variantVector) {
//    System.out.println("Reducing log to replayable variants.");
//
//    // Checking if the given variant vector is applicable to the list of variants.
//    if (variantVector.length != traceVariants.size()) {
//      System.out.println("Error reducing log to replayable variants! Variant vector does not match log size.");//for debugging
//    }
//
//    // Reducing frequency in the log to zero for each non-replayable variant.
//    for (int i = 0; i < variantVector.length; i++) {
//      if (!variantVector[i]) {
//        traceVariantCounts.put(traceVariants.get(i), 0);
//      }
//    }
//
//    printExtensiveLogSummary();
    }

    /**
     * Extracts the transitions, that are contained in the log.
     *
     * @param parameters The settings for the eST-Miner algorithm.
     * @return The transitions contained in the log.
     */
    private String[] getTransitionsFromLog(Parameters parameters) {
        // XES-related classifier for events
        XEventClassifier classifier = parameters.getEventClassifier();

        Set<String> transitionsList = new HashSet<>();

        // Checking the events in each trace of the log to find the transitions.
        for (XTrace trace : xLog) {
            for (XEvent event : trace) {
                String activityLabel = classifier.getClassIdentity(event);
                transitionsList.add(activityLabel);
            }
        }

        // Returning the HashSet as Array of Strings
        return transitionsList.toArray(new String[0]);
    }

    /**
     * Adds an start and end activity to each trace.
     */
    private void addUniqueStartEnd() {
        XFactory factory = XFactoryRegistry.instance().currentDefault();

        // Creating start and end attributes with unique labels by adding the actual time.
        XAttributeMap startAttributes = factory.createAttributeMap();
        XAttributeMap endAttributes = factory.createAttributeMap();

        String startTransitionName;
        String endTransitionName;

        if (useUniqueNames) {
            startTransitionName = "Start-" + System.currentTimeMillis(); //add ms to ensure unique naming
            endTransitionName = "End-" + System.currentTimeMillis();
        } else {
            startTransitionName = "Start";
            endTransitionName = "End";
        }

        startAttributes.put(XConceptExtension.KEY_NAME, factory.createAttributeLiteral(XConceptExtension.KEY_NAME,
                startTransitionName, XConceptExtension.instance()));
        endAttributes.put(XConceptExtension.KEY_NAME, factory.createAttributeLiteral(XConceptExtension.KEY_NAME,
                endTransitionName, XConceptExtension.instance()));

        // Creating a start and end activity and adding them to each trace
        final XEvent startActivity = factory.createEvent(startAttributes);
        final XEvent endActivity = factory.createEvent(endAttributes);
        for (XTrace trace : xLog) {
            trace.add(0, startActivity);
            trace.add(endActivity);
        }

        // Storing the start and end activity
        this.startActivity = startActivity;
        this.endActivity = endActivity;
    }

    //----------compute basic log properties------------------------

    /**
     * Computes the number of occurrences for each transition in the log.
     *
     * @param eventClassifier The eventClassifier corresponding to the events in the log.
     * @return A HashMap containing a mapping from all transitions to the number of their occurrences.
     */
    private HashMap<String, Integer> computeTransitionsOccurences(XEventClassifier eventClassifier) {
        HashMap<String, Integer> transitionWeights = new HashMap<>();

        // Initializing HashMap with 0 for each transition
        for (String transition : inTransitions) {
            transitionWeights.put(transition, 0);
        }

        // Iterating through all events in the log
        for (XTrace trace : xLog) {
            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);

                // Counting occurrences for each transition
                int weight = transitionWeights.get(key);
                transitionWeights.put(key, weight + 1);
            }
        }

        return transitionWeights;
    }

    //MUST compute number of occurences in advance
    //returns the naive average indices of the activities. prone to errors when repeated activities exist

    /**
     * Computes the average position of each transition.
     *
     * @param eventClassifier The eventClassifier corresponding to the events in the log.
     * @return A HashMap mapping each transition on the average rounded position in a trace.
     */
    private HashMap<String, Integer> computeAverageTransitionIndices(XEventClassifier eventClassifier) {
        HashMap<String, Integer> average_indices = new HashMap<>();

        // Initializing HashMap with 0 for each transition
        for (String transition : inTransitions) {
            average_indices.put(transition, 0);
        }

        // Compute the sum of all indices of the events in each trace
        for (XTrace trace : xLog) {
            int actIndex = 0;
            for (XEvent event : trace) {
                String key = eventClassifier.getClassIdentity(event);

                // Adding the current position to the mapping
                int index = average_indices.get(key);
                average_indices.put(key, index + actIndex);

                // Updating the position of the current position of the event
                actIndex++;
            }
        }

        // Dividing by the activity weights (the number of occurrences) to compute the average indices
        for (String key : average_indices.keySet()) {
            int indexSum = average_indices.get(key);
            indexSum = indexSum / transitionOccurences.get(key);

            average_indices.put(key, indexSum);
        }

        return average_indices;
    }

    /**
     * TODO: compute directly-follow-scores: score(x,y)=#(x<y)/|x|*|y|
     *
     * @return The directly follow scores.
     */
    public double[][] computeDFScores() {
        double[][] scores = new double[inTransitions.length][inTransitions.length];
        //TODO
        System.out.println("DFScores computation not implemented yet!!!");
        return scores;
    }

    //----------compute transition orderings------------------------

    //randomly shuffles the transitions array

    /**
     * Randomizes the ordering of a transition array.
     *
     * @param transitions An array containing transitions.
     * @return The transitions in a randomized ordering.
     */
    private String[] shuffleTransitions(String[] transitions) {
        Random random = new Random();

        // Picking each time a random transition and putting it in the last position of the array, that has been set yet.
        for (int i = transitions.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);

            // Switching positions
            String temp = transitions[index];
            transitions[index] = transitions[i];
            transitions[i] = temp;
        }

        return transitions;
    }

    /**
     * Reverses the order of the transitions in the given array.
     *
     * @param transitions An array containing transitions.
     * @return The given array in a reversed order.
     */
    private String[] reverseTransitions(String[] transitions) {
        ArrayUtils.reverse(transitions);
        return transitions;
    }

    /**
     * TODO: adapt if needed (currently randomized), endActivity should be at position 0 (for easy skipping)
     *
     * @param parameters The settings for the eST-Miner algorithm.
     * @return An ordering of the ingoing transitions, which is improved through heuristics.
     */
    private String[] computeInTransitionOrder(Parameters parameters) {
        //place ordering strategy here:
        String[] intransitions = shuffleTransitions(this.inTransitions.clone()); //ordering: random shuffle
        intransitions = moveEndActivityToFirstPos(parameters, intransitions);
        return intransitions;
    }

    /**
     * TODO: adapt if needed (currently randomized), startActivity should be at position 0 (for easy skipping)
     *
     * @param parameters The settings for the eST-Miner algorithm.
     * @return An ordering of the outgoing transitions, which is improved through heuristics.
     */
    private String[] computeOutTransitionOrder(Parameters parameters) {
        //place ordering strategy here:
        String[] outtransitions = shuffleTransitions(inTransitions.clone());//ordering: random shuffle
        outtransitions = moveStartActivityToFirstPos(parameters, outtransitions);
        return outtransitions;
    }


    /**
     * Compute the mapping from outTransitions to inTransitions.
     *
     * @return An array which maps the ordering in the inTransitions to the position in the outTransitions.
     */
    private int[] computeOutTransitionMapping() {
        System.out.println(Arrays.toString(inTransitions));
        System.out.println(Arrays.toString(outTransitions));

        HashMap<String, Integer> inTransitionPositions = new HashMap<>();

        // Mein Code
        // Storing the positions of each string in the the outTransition array.
        int i = 0;
        for (String s : inTransitions) {
            inTransitionPositions.put(s, i);
            i++;
        }

        int[] outTransitionMapping = new int[outTransitions.length];

        // Finding the positions for each transition in the array.
        i = 0;
        for (String s : outTransitions) {
            outTransitionMapping[i] = inTransitionPositions.get(s);
            i++;
        }

        int[] outTransitionMapping2 = new int[outTransitions.length];
        for (int k = 0; k < inTransitions.length; k++) {
            for (int o = 0; o < outTransitions.length; o++) {
                if (inTransitions[k] == outTransitions[o]) {
                    outTransitionMapping2[o] = k;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(outTransitionMapping2));

        return outTransitionMapping;
    }

    //moves the end activity to position 0 for the intransitions

    /**
     * Moving the start activity to the first position of the ingoing transitions array, while Keeping the order of the array otherwise.
     *
     * @param parameters    The settings for the eST-Miner algorithm.
     * @param inTransitions The array containing the ordering of the ingoing transitions.
     * @return The inTransition array with the end activity at the first position.
     */
    private String[] moveEndActivityToFirstPos(Parameters parameters, String[] inTransitions) {
        inEndIndex = findEndIndex(parameters, inTransitions);

        System.out.print("[");
        for(String t : inTransitions){
            System.out.print(t);
        }
        System.out.println("]");

        // Storing the endActivity
        String endActivity = inTransitions[inEndIndex];

        // Moving all entries from [0, (i-1)] to [1, i].
        while (inEndIndex != 0) {
            inTransitions[inEndIndex] = inTransitions[inEndIndex - 1];
            inEndIndex--;
        }

        // Putting the endActivity at the beginning of the array.
        inTransitions[0] = endActivity;

        return inTransitions;
    }


    /**
     * Moving the start activity to the first position of the outgoing transitions array, while Keeping the order of the array otherwise.
     *
     * @param parameters     The settings for the eST-Miner algorithm.
     * @param outTransitions The array containing the ordering of the outgoing transitions.
     * @return The outTransition array with the end activity at the first position.
     */
    private String[] moveStartActivityToFirstPos(Parameters parameters, String[] outTransitions) {
        outStartIndex = findStartIndex(parameters, outTransitions);

        // Storing the startActivity
        String startActivity = outTransitions[outStartIndex];

        // Moving all entries from [0, (i-1)] to [1, i].
        while (outStartIndex != 0) {
            outTransitions[outStartIndex] = outTransitions[outStartIndex - 1];
            outStartIndex--;
        }

        // Putting the startActivity at the beginning of the array.
        outTransitions[0] = startActivity;

        return outTransitions;
    }

    // find end index in the given transition array

    /**
     * Computes the position of the endActivity for a given array.
     *
     * @param parameters  The settings for the eST-Miner algorithm.
     * @param transitions The array containing transitions.
     * @return The index of endActivity in the given array of transitions.
     */
    public int findEndIndex(Parameters parameters, String[] transitions) {
        int endIndex = 0;

        // Getting the name of the endActivity
        String endActivity = parameters.getEventClassifier().getClassIdentity(this.endActivity);

        // Iterating through the transitions until the end activity is found.
        int currIndex = 0;
        for (String transition : transitions) {
            if (transition.equals(endActivity)) {
                endIndex = currIndex;
                break;
            }

            currIndex++;
        }

        return endIndex;
    }

    /**
     * Computes the position of the startActivity for a given array.
     *
     * @param parameters  The settings for the eST-Miner algorithm.
     * @param transitions The array containing transitions.
     * @return The index of startActivity in the given array of transitions.
     */
    public int findStartIndex(Parameters parameters, String[] transitions) {
        int startIndex = 0;

        // Getting the name of the startActivity
        String startActivity = parameters.getEventClassifier().getClassIdentity(this.startActivity);

        // Iterating through the transitions until the end activity is found.
        int currIndex = 0;
        for (String transition : transitions) {
            if (transition.equals(startActivity)) {
                startIndex = currIndex;
                break;
            }

            currIndex++;
        }

        return startIndex;
    }

    //----------compute final log objects------------------------

    /**
     * Computing and storing the existing variants and the count of each variant.
     *
     * @param parameters The settings for the eST-Miner algorithm.
     */
    private void computeFinalLogObjects(Parameters parameters) {
        // Convert log to integers and group variants.
        LinkedList<LinkedList<Integer>> convertedLog = convertLog(xLog, inTransitions, parameters);
        final Object[] groupedLog = groupLog(convertedLog);

        traceVariants = (LinkedList<LinkedList<Integer>>) groupedLog[0];
        traceVariantCounts = (HashMap<LinkedList<Integer>, Integer>) groupedLog[1];
    }

    //replace activities by integers corresponding to their position in the transitions array

    /**
     * Recomputes the log and stores each transitions name with the index in the transitions array.
     *
     * @param inputLog    The XES input log.
     * @param transitions An array containing transitions.
     * @param parameters  The settings for the eST-Miner algorithm.
     * @return A log in a chained lists representation containing transition IDs instead of the transition names.
     */
    private LinkedList<LinkedList<Integer>> convertLog(XLog inputLog, String[] transitions, Parameters parameters) {
        // Getting the classifier for the events.
        XEventClassifier classifier = parameters.getEventClassifier();

        // The log containing indices instead of names.
        LinkedList<LinkedList<Integer>> convertedLog = new LinkedList<LinkedList<Integer>>();

        // An overview containing the position in the transition array for each transition.
        HashMap<String, Integer> transitionPositions = new HashMap<>();

        // Storing the positions for each transition.
        int transitionIndex = 0;
        for (String transition : transitions) {
            transitionPositions.put(transition, transitionIndex);
            transitionIndex++;
        }

        // Creating a trace consisting of indices instead of names for each trace in the log and storing them.
        for (XTrace trace : inputLog) {
            LinkedList<Integer> convertedTrace = new LinkedList<Integer>();

            for (XEvent event : trace) {
                String transitionName = classifier.getClassIdentity(event);
                transitionIndex = transitionPositions.get(transitionName);
                convertedTrace.add(transitionIndex);
            }

            convertedLog.add(convertedTrace);
        }

        numberOfTraces = convertedLog.size();

        return convertedLog;
    }

    //for a converted log returns a log containing the trace variants only, togther with a map containing their frequencies

    /**
     * Computes the variants of the log and their frequency.
     *
     * @param convertedLog The log in the numerical representation of the transitions.
     * @return An array containing the trace variants and their frequencies
     */
    private Object[] groupLog(final LinkedList<LinkedList<Integer>> convertedLog) {
        HashMap<LinkedList<Integer>, Integer> frequencies = new HashMap<LinkedList<Integer>, Integer>();
        LinkedList<LinkedList<Integer>> traceVariants = new LinkedList<LinkedList<Integer>>();

        // Counting the occurrences of each trace in the log.
        for (LinkedList<Integer> trace : convertedLog) {
            if (!frequencies.containsKey(trace)) {
                frequencies.put(trace, 0);
            }
            frequencies.put(trace, frequencies.get(trace) + 1);
        }

        // Checking for each variant and adding to the variants.
        for (LinkedList<Integer> variant : frequencies.keySet()) {
            traceVariants.add(variant);
        }

        return new Object[]{traceVariants, frequencies};
    }

    //_________________printing information_____________________________________

    /**
     * Prints the inTransitions and outTransitions ordering.
     */
    public void printTransitionOrderings() {
        String inorder = "Inorder: ";
        String outorder = "Outorder: ";

        // Building the strings containing the ordering.
        for (int i = 0; i < inTransitions.length; i++) {
            inorder = inorder + inTransitions[i] + ";  ";
            outorder = outorder + outTransitions[i] + ";  ";
        }

        // Printing the ordering and removing last semicolon.
        System.out.println(inorder.substring(0, inorder.length() - 2));
        System.out.println(outorder.substring(0, outorder.length() - 2));
    }

    /**
     * Printing number of traces, the number of trace variants and the number of activities in the log.
     */
    public void printBasicLogSummary() {
        System.out.println("Number of Traces: " + numberOfTraces + ", " +
                "Unique Variants: " + traceVariants.size() + ", " +
                "Number of Activities: " + inTransitions.length);
    }

    /**
     * Printing each trace and the corresponding frequency.
     */
    private void printExtensiveLogSummary() {
        String result = "Log overview:";

        // Getting each trace and storing the trace and its frequency in a string.
        for (LinkedList<Integer> variant : traceVariants) {
            result = result + "\n" +
                    variant.toString() + "\t" +
                    traceVariantCounts.get(variant);
        }

        System.out.println(result);
    }

    // ___________________Getter & Setter___________________

    public String[] getInTransitions() {
        return inTransitions;
    }

    public int[] getOutTransitionMapping() {
        return outTransitionMapping;
    }

    public XLog getxLog() {
        return xLog;
    }

    public LinkedList<LinkedList<Integer>> getTraceVariants() {
        return traceVariants;
    }

    public HashMap<LinkedList<Integer>, Integer> getTraceVariantCounts() {
        return traceVariantCounts;
    }

    public int getNumOfTraces() {
        return numberOfTraces;
    }

    public int getInEndIndex() {
        return inEndIndex;
    }

    //returns a XLog containig all remaining traces (simlpyfied)
//  public XLog createXLog(boolean[] variantVector) {
//    ArrayList<ArrayList<Integer>> variants = getReducedTraceVariants(variantVector);
//    XLogBuilder builder = new XLogBuilder();
//    builder.startLog("ReducedLog");
//    int traceCounter=0;
//    for(ArrayList<Integer> traceVariant : variants) {
//      int variantFrequency = traceVariantCounts.get(traceVariant);
//      for(int i =0; i<variantFrequency; i++) {
//        traceCounter++;
//        builder.addTrace("Trace "+traceCounter);
//        for(Integer event : traceVariant) {
//          builder.addEvent(inTransitions[event]);
//        }
//      }
//    }
//    return builder.build();
//  }


}
