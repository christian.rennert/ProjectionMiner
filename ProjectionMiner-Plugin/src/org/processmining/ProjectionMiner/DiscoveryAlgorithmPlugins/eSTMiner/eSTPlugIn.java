package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import org.deckfour.uitopia.api.event.TaskListener.InteractionResult;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.AcceptingPetriNetWrapper;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.CandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.InterestScorer;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceFactory;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.models.connections.petrinets.behavioral.FinalMarkingConnection;
import org.processmining.models.connections.petrinets.behavioral.InitialMarkingConnection;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.semantics.petrinet.Marking;

import java.util.*;

/**
 * The class containing the Main functionality of the eST-Miner
 */
public class eSTPlugIn extends DiscoveryPlugin {

    @Override
    public AcceptingPetriNetWrapper discover(UIPluginContext context, XLog inputLog, Object parameter) throws InterruptedException {
        System.out.println(
                "_____________ eST - Miner ___________________________________________________________________________________");

        //taking a copy of the input log to work with
        XLog inputLogCopy = (XLog) inputLog.clone();
        Parameters parameters;
        if (parameter == null) {
            parameters = getGeneralParameters(inputLogCopy, context); //set general parameters using user input
        } else {
            parameters = (Parameters) parameter;
        }
        //Pre-process the Log (create usable log object)
        System.out.println("Preprocessing the Log...");
        eSTLog log = new eSTLog(inputLogCopy, parameters);
        String[] transitions = log.getInTransitions();
        int[] outTransitionsMapping = log.getOutTransitionMapping();
        double[][] DFScores = log.computeDFScores();

        log.printBasicLogSummary();
        log.printTransitionOrderings();
        final int P_all = (int) ((Math.pow(Math.pow(2, (transitions.length - 1)), 2)
                - (2 * Math.pow(2, (transitions.length - 1)))) + 1);


        final LinkedList<LinkedList<Integer>> traceVariants = log.getTraceVariants();
        final HashMap<LinkedList<Integer>, Integer> traceVariantCounts = log.getTraceVariantCounts();

        System.out.println("Traces are:");
        System.out.println(traceVariants);

        //setting the number of traceVariants within the class MyPlace
        eSTPlaceFactory.getInstance().setNumVariants(traceVariants.size());

        //Initialize Process Model (unique transitions and empty list of places)
        ArrayList<eSTPlace> places = new ArrayList<>();
        eSTProcessModel pM = new eSTProcessModel(places, transitions);

        //-------------Initialize "working classes"-------------------------------------------------------------------------------------------------------
        //TODO ensure all removers are usable (currently only replay based works)
        ImplicitPlaceRemover IPRemover = parameters.getImplicitPlaceRemovalVariant();
        IPRemover.init(transitions, traceVariants);


        //select traverser based on chosen traversal strategy
        CandidateTraverser candidates = parameters.getTraversalStrategy();
        candidates.init(transitions, outTransitionsMapping);

        if (candidates.hasLambda()) {
            InterestScorer.getInstance().setNumbOfTransitions(transitions.length);
            InterestScorer.getInstance().computeValues(traceVariants, traceVariantCounts, parameters.getLambdaThreshold());
        }

        int[] traceCountsArray = new int[traceVariantCounts.size()];
        //TODO make sure that order of traces remains the same
        for (int i = 0; i < traceVariants.size(); i++) {
            LinkedList<Integer> currentVariant = traceVariants.get(i);
            traceCountsArray[i] = traceVariantCounts.get(currentVariant);
        }
        ClassicDiscovery pDiscovery = new ClassicDiscovery(pM, candidates, IPRemover, parameters, log);

        //---------------------Evaluating Places (the important part)---------------------------------------------------------------------

        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Start adding places...");
        //Evaluating and possibly adding the candidate places
        final long timeEvalStart = System.currentTimeMillis();
        pDiscovery.start();
        pDiscovery.join(parameters.getTimeLimit());
        if (pDiscovery.isAlive()) {
            pDiscovery.interrupt();
        }
        PlugInStatistics.instance().setTimeEval(System.currentTimeMillis() - timeEvalStart);

        //results aftr discovery
        eSTProcessModel discoveredPM = pDiscovery.getProcessModel().copy();
        System.out.println("Final (concurrent) IP removal currently skipped");
        //---------------------------Preliminary Results----------------------------------------------------

        //compute results
        eSTProcessModel finalPM = discoveredPM;
        int numCurrentPlaces = finalPM.getPlaces().size();
        final int P_replayed = PlugInStatistics.instance().getNumFitting()
                + PlugInStatistics.instance().getNumUnfitting() + PlugInStatistics.instance().getNumFittingLoop()
                + PlugInStatistics.instance().getNumUnfittingLoop();
        final int cutoffP = (P_all - P_replayed);
        final double cutoffPPercentageP_all = ((cutoffP * 100.0) / P_all);

        //print results for debugging
        System.out.println(
                "_____________________________________________________________________________________________");
        System.out.println("P-all: " + P_all);
        System.out.println("Number of checked unfitting Places (Tree Traversal): "
                + PlugInStatistics.instance().getNumUnfitting());
        System.out.println("P-fit (Tree Traversal): " + PlugInStatistics.instance().getNumFitting());
        System.out.println("Cutoff Places: " + cutoffP + " | " + cutoffPPercentageP_all + " % of P-all");
        System.out.println(
                "------------------------------------------------------------------------------------------------------");
        System.out.println("Adding Places needed: " + (PlugInStatistics.instance().getTimeEval()) + " ms");
        System.out.println(
                "of this for finding tree candidates: " + (PlugInStatistics.instance().getTimeCandFind()) + " ms");
        System.out
                .println("Of this for Replay (Treetraversal): " + PlugInStatistics.instance().getTimeReplay() + " ms");
        System.out.println(
                "-----------------------------------------------------------------------------------------------------");

        if (parameters.shouldRemoveImplicits() && !parameters.shouldRemoveImplicitsConcurrently()) {
            //--------------------------Post-Processing---------------------------------------------------------------------------------
            System.out.println("Start post-processing...");
            final long postprocstart = System.currentTimeMillis();

            //remove implicit places
            numCurrentPlaces = finalPM.getPlaces().size();
            System.out.println("Number of places before removing implicit places: " + numCurrentPlaces);
            finalPM = IPRemover.removeAllIPs(finalPM, log);

            if (parameters.shouldCombineSimilarPlaces()) {
                finalPM = IPRemover.combineSimilarPlaces(finalPM);
            }

            PlugInStatistics.instance().setNumImpPlace(numCurrentPlaces - finalPM.getPlaces().size());

            long postproctime = System.currentTimeMillis() - postprocstart;
            System.out.println("P-final: " + finalPM.getPlaces().size());

            //print for debugging
            System.out.println("Post-processing needed: " + postproctime + " ms");
            System.out.println("-------------------------------------------------------------------------------------");
        } else {
            System.out.println(
                    "Concurrent IP-Removal Time needed: " + PlugInStatistics.instance().getTimeImpTest() + " ms");
        }

        if (parameters.getImplicitPlaceRemovalVariant().equals("Replay")) {
            System.out.println("IP-Removal - Number of comparisons: " + PlugInStatistics.instance().getComparisons());
        }

        for (eSTPlace p : finalPM.getPlaces()) {
            System.out.println(p.toBinaryString());
        }

        //-----------------create Petrinet from Process Model-------------------------------------------------------------------------


        Petrinet net = PetrinetFactory.newPetrinet("Process Model");
        Marking initial_marking = new Marking();
        Marking final_marking = new Marking();

        //add start place and add it to to initial markings
        Place startP = net.addPlace("Start");
        initial_marking.add(startP);
        //add end place and add it to final markings
        Place endP = net.addPlace("End");
        final_marking.add(endP);
        //add transitions and connecting arcs for start and end place
        int startIndex = log.findStartIndex(parameters, transitions);
        int endIndex = log.getInEndIndex();
        for (int i = 0; i < finalPM.getTransitions().length; i++) {//add the transitions to the petri net
            Transition t = net.addTransition(finalPM.getTransitions()[i]);
            if (i == startIndex) {
                net.addArc(startP, t);
                t.setInvisible(true);
            } else if (i == endIndex) {
                net.addArc(t, endP);
                t.setInvisible(true);
            }
        }

        //add intermediate places from process model
        final Collection<Transition> petriTransitions = net.getTransitions();
        for (eSTPlace myP : finalPM.getPlaces()) {
            Place newP = net.addPlace("");
            for (Transition t : petriTransitions) {
                if (getTransitionNames(myP.getInputTransitionKey(), transitions).contains(t.getLabel())) {
                    net.addArc(t, newP);
                }
                if (getTransitionNames(myP.getOutputTransitionKey(), transitions).contains(t.getLabel())) {
                    net.addArc(newP, t);
                }
            }
        }

        if (parameter != null) {
            ArrayList<PetrinetEdge> edgesToBeRemoved = new ArrayList<>();
            ArrayList<Transition> transitionsToBeRemoved = new ArrayList<>();
            ArrayList<Place> placesToBeRemoved = new ArrayList<>();

            for (Transition t : net.getTransitions()) {
                if (t.getLabel().equals("Start") || t.getLabel().equals("ArtificialStart")) {
                    Marking new_initial_marking = new Marking();
                    for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> outEdge : net.getOutEdges(t)) {
                        new_initial_marking.add((Place) outEdge.getTarget());
                        edgesToBeRemoved.add(outEdge);
                    }
                    initial_marking = new_initial_marking;

                    for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> inEdge : net.getInEdges(t)) {
                        edgesToBeRemoved.add(inEdge);
                        placesToBeRemoved.add((Place) inEdge.getSource());
                    }
                    transitionsToBeRemoved.add(t);
                }

//                if (t.getLabel().equals("End") || t.getLabel().equals("ArtificialEnd")) {
//                    Marking new_final_marking = new Marking();
//                    for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> inEdge : net.getInEdges(t)) {
//                        new_final_marking.add((Place) inEdge.getSource());
//                        edgesToBeRemoved.add(inEdge);
//                    }
//                    final_marking = new_final_marking;
//
//                    for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> outEdge : net.getOutEdges(t)) {
//                        edgesToBeRemoved.add(outEdge);
//                        placesToBeRemoved.add((Place) outEdge.getTarget());
//                    }
//                    transitionsToBeRemoved.add(t);
//                }
            }

            for (PetrinetEdge edge : edgesToBeRemoved) {
                net.removeEdge(edge);
            }

            for (Place place : placesToBeRemoved) {
                net.removePlace(place);
            }
            for (Transition transition : transitionsToBeRemoved) {
                net.removeTransition(transition);
            }

        }


        context.getConnectionManager().addConnection(new InitialMarkingConnection(net, initial_marking));
        context.getConnectionManager().addConnection(new FinalMarkingConnection(net, final_marking));

        System.out.println("Number of Arcs: " + net.getEdges().size());
        System.out.println("Number of Places: " + net.getPlaces().size());
        System.out.println("____________________________________________________________________________________");

        PlugInStatistics.instance().resetStatistics();
//    return log.getxLog();
//        return new Object[]{net, initial_marking, final_marking, log.getxLog()};
        return new AcceptingPetriNetWrapper(net, initial_marking, final_marking, parameters);
    }

    @Override
    public Object startGUI(UIPluginContext context, XLog log) {
        return getGeneralParameters(log, context);
    }

    @Override
    public String toString() {
        return "eST-Miner (older version, with lambda variant)";
    }

    //_____________________________END OF MAIN - Methods and Helper Functions__________________________________________________________

    //------------------------------------ User Input Handling -----------------------------------------------------

    //get input independent general parameters from user
    private Parameters getGeneralParameters(XLog log, UIPluginContext context) {
        eSTMinerDialog dialog = new eSTMinerDialog(log);
        InteractionResult result = context.showWizard("Configure eST-Miner parameters:", true, true, dialog);

        if (result != InteractionResult.FINISHED) {
            context.getFutureResult(0).cancel(false);
            return null;
        }

        Parameters parameters = dialog.apply();

        return parameters;
    }

    //scores each activity with 0
    private HashMap<String, Float> defaultActivityScores(XLog log, XEventClassifier classifier) {
        HashMap<String, Float> defaultScores = new HashMap<String, Float>();
        for (XTrace trace : log) {
            for (XEvent event : trace) {
                String key = classifier.getClassIdentity(event);
                defaultScores.put(key, (float) 0);
            }
        }
        return defaultScores;
    }

    //create transition array ordered according to sorting of scores hash map
    private String[] createOrderedTransitions(HashMap<String, Float> activityScores) {
        Collection<String> scoredActivities = activityScores.keySet();
        ArrayList<String> transitionList = new ArrayList<String>();
        ArrayList<Float> tempScores = new ArrayList<Float>(activityScores.values());
        tempScores.sort(null);
        Iterator<Float> iterator = tempScores.iterator();
        while (iterator.hasNext()) {
            Float value = iterator.next();
            for (String activityName : scoredActivities) {
                if (activityScores.get(activityName) == value) { //the key "activity" is mapped to the value
                    transitionList.add(activityName);
                }
            }
            while (!tempScores.isEmpty() && (tempScores.get(0) == value)) {//ensure the transitions mapped to from the current score are added only once
                tempScores.remove(0);
            }
            iterator = tempScores.iterator();
        }
        String[] transitions = new String[transitionList.size()];
        for (int i = 0; i < transitions.length; i++) {
            transitions[i] = transitionList.get(i);
        }
        return transitions;
    }

    //--------------------- General Helper Functions ------------------------------------------------------------

    //returns a collection containing all transitions names from the given transitions array
    private Collection<String> getTransitionNames(final int key, final String[] transitions) {
        Collection<String> result = new ArrayList<>();
        if (key > (Math.pow(2, transitions.length))) {
            return null;
        }
        for (int i = 0; i < transitions.length; i++) {
            if ((key & getMask(i, transitions)) > 0) { //test key for ones
                result.add(transitions[i]);
            }
        }
        return result;
    }

    //for a given position in the transition array return the corresponding bitmask
    private int getMask(final int pos, final String[] transitions) {
        return 1 << (transitions.length - 1 - pos);
    }

}