package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.BFSCandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.CandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.DFSCandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.LambdaDFSCandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ILPBasedImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.OptimizationBasedImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ReplayBasedImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.ActivityFrequencyOrderingStrategy;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.AverageFirstOccurrenceStrategy;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.AverageTraceOccurenceStrategy;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.LexicographicalOrderingStrategy;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.TraceFrequencyOrderingStrategy;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.TransitionOrderingStrategy;

import com.fluxicon.slickerbox.factory.SlickerFactory;

/**
 * Provides the user interface of the ProM Plugin. All parameters set in ProM were stored in a Parameters class.
 */
public class eSTMinerDialog extends JPanel {
    // ProM concerning ID
    private static final long serialVersionUID = 4105600805030210971L;
    // All items in the user interface, that can be handled or read.
    private static final String TITLE = "Configure eST-Miner parameters:";
    // The XES event log.
    private final XLog log;
    private final JComboBox<CandidateTraverser> traversalStrategyComboBox;
    private final JComboBox<String> discoveryVariantComboBox;
    private final JSlider tauThresholdSlider;
    private final JSlider lambdaThresholdSlider;
    private final JComboBox<XEventClassifier> eventClassifierComboBox;
    private final JComboBox<ImplicitPlaceRemover> implicitPlaceRemovalVariantComboBox;
    private final JCheckBox removeImplicitPlacesCheckBox;
    private final JCheckBox removeConcurrentlyCheckBox;
    private final JCheckBox combineSimilarPlacesCheckBox;
    private final JCheckBox useUniqueNamesCheckBox;
    private final JComboBox<TransitionOrderingStrategy> transitionOrderingStrategyComboBox;


    /**
     * Constructs the user interface for the eST-Miner in ProM.
     *
     * @param log The log containing the traces.
     */
    @SuppressWarnings("unchecked")
    public eSTMinerDialog(XLog log) {
        // Constructs an empty UI with the given title.
//    super(TITLE);
        SlickerFactory factory = SlickerFactory.instance();

        int leftColumnWidth = 200;
        int columnMargin = 20;
        int rowHeight = 40;

        SpringLayout layout = new SpringLayout();
        setLayout(layout);


        // Sets the log item.
        this.log = log;

        //variant
        String[] discoveryVariants = new String[]{"Classic", "Uniwired - not yet implemented"};
        final JLabel variantLabel;
        {
            variantLabel = factory.createLabel("Variant");
            add(variantLabel);
            layout.putConstraint(SpringLayout.NORTH, variantLabel, 5, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, variantLabel, leftColumnWidth, SpringLayout.WEST, this);

            discoveryVariantComboBox = factory.createComboBox(discoveryVariants);
            discoveryVariantComboBox.setPreferredSize(discoveryVariantComboBox.getMaximumSize());
            add(discoveryVariantComboBox);
            layout.putConstraint(SpringLayout.WEST, discoveryVariantComboBox, columnMargin, SpringLayout.EAST, variantLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, discoveryVariantComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    variantLabel);
        }


        // Tau threshold
        final JLabel tauLabel;
        final JLabel tauValue;
        final JLabel tauExplanation;
        {
            tauLabel = factory.createLabel("Tau threshold");
            add(tauLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, tauLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    variantLabel);
            layout.putConstraint(SpringLayout.EAST, tauLabel, leftColumnWidth, SpringLayout.WEST, this);

            tauThresholdSlider = factory.createSlider(SwingConstants.HORIZONTAL);
            tauThresholdSlider.setMinimum(0);
            tauThresholdSlider.setMaximum(1000);
            tauThresholdSlider.setValue(1000);
            add(tauThresholdSlider);
            layout.putConstraint(SpringLayout.WEST, tauThresholdSlider, columnMargin, SpringLayout.EAST, tauLabel);
            layout.putConstraint(SpringLayout.EAST, tauThresholdSlider, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, tauThresholdSlider, 0, SpringLayout.VERTICAL_CENTER,
                    tauLabel);

            tauValue = factory.createLabel(String.format("%.2f", 1.0));
            add(tauValue);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, tauValue, 0, SpringLayout.VERTICAL_CENTER, tauLabel);
            layout.putConstraint(SpringLayout.WEST, tauValue, columnMargin, SpringLayout.EAST, tauThresholdSlider);

            tauExplanation = factory.createLabel("  If set to 1.00, perfect log fitness is guaranteed.");
            add(tauExplanation);
            layout.putConstraint(SpringLayout.WEST, tauExplanation, columnMargin, SpringLayout.EAST, tauLabel);
            layout.putConstraint(SpringLayout.NORTH, tauExplanation, rowHeight / 6, SpringLayout.VERTICAL_CENTER,
                    tauLabel);
        }

        tauThresholdSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                tauValue.setText(String.format("%.2f", tauThresholdSlider.getValue() / 1000.0));
            }
        });

        final JLabel lambdaLabel;
        final JLabel lambdaValue;
        {
            lambdaLabel = factory.createLabel("Lambda threshold");
            add(lambdaLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, lambdaLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    tauLabel);
            layout.putConstraint(SpringLayout.EAST, lambdaLabel, leftColumnWidth, SpringLayout.WEST, this);

            lambdaThresholdSlider = factory.createSlider(SwingConstants.HORIZONTAL);
            lambdaThresholdSlider.setMinimum(0);
            lambdaThresholdSlider.setMaximum(1000);
            lambdaThresholdSlider.setValue(0);
            add(lambdaThresholdSlider);
            layout.putConstraint(SpringLayout.WEST, lambdaThresholdSlider, columnMargin, SpringLayout.EAST, lambdaLabel);
            layout.putConstraint(SpringLayout.EAST, lambdaThresholdSlider, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, lambdaThresholdSlider, 0, SpringLayout.VERTICAL_CENTER,
                    lambdaLabel);

            lambdaValue = factory.createLabel(String.format("%.2f", 0.0));
            add(lambdaValue);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, lambdaValue, 0, SpringLayout.VERTICAL_CENTER, lambdaLabel);
            layout.putConstraint(SpringLayout.WEST, lambdaValue, columnMargin, SpringLayout.EAST, lambdaThresholdSlider);
        }


        lambdaThresholdSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent arg0) {
                lambdaValue.setText(String.format("%.2f", lambdaThresholdSlider.getValue() / 1000.0));
            }
        });

        // Constructs a combo box to choose the correct classifier from.
        // Adds all existing classifiers to the list.
        List<String> logClassifiersLabels = new ArrayList<>();
        List<XEventClassifier> logClassifiers = log.getClassifiers();

        for (XEventClassifier classifier : logClassifiers) {
            logClassifiersLabels.add(classifier.toString());
        }

        if (logClassifiersLabels.size() == 0) {
            logClassifiersLabels.add(XLogInfoImpl.NAME_CLASSIFIER.toString());
        }

        final JLabel eventClassifierLabel;
        {
            eventClassifierLabel = factory.createLabel("Classifier:");
            add(eventClassifierLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, eventClassifierLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    lambdaLabel);
            layout.putConstraint(SpringLayout.EAST, eventClassifierLabel, leftColumnWidth, SpringLayout.WEST, this);

            eventClassifierComboBox = factory.createComboBox(logClassifiersLabels.toArray());
            eventClassifierComboBox.setPreferredSize(eventClassifierComboBox.getMaximumSize());
            add(eventClassifierComboBox);
            layout.putConstraint(SpringLayout.WEST, eventClassifierComboBox, columnMargin, SpringLayout.EAST, eventClassifierLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, eventClassifierComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    eventClassifierLabel);
        }

        CandidateTraverser[] candidateTraverserVariants = new CandidateTraverser[]{
                new DFSCandidateTraverser(),
                new LambdaDFSCandidateTraverser(),
                new BFSCandidateTraverser()
        };

        // Constructs a combo box to choose depth-first-search (DFS) of breadth-first-search (BFS).
        final JLabel traversalStrategyLabel;
        {
            traversalStrategyLabel = factory.createLabel("Tree Traversal Strategy:");
            add(traversalStrategyLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, traversalStrategyLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    eventClassifierLabel);
            layout.putConstraint(SpringLayout.EAST, traversalStrategyLabel, leftColumnWidth, SpringLayout.WEST, this);

            traversalStrategyComboBox = factory.createComboBox(candidateTraverserVariants);
            traversalStrategyComboBox.setPreferredSize(traversalStrategyComboBox.getMaximumSize());
            add(traversalStrategyComboBox);
            traversalStrategyComboBox.setSelectedIndex(0);
            layout.putConstraint(SpringLayout.WEST, traversalStrategyComboBox, columnMargin, SpringLayout.EAST, traversalStrategyLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, traversalStrategyComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    traversalStrategyLabel);
        }

        traversalStrategyComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                CandidateTraverser traverser = (CandidateTraverser) traversalStrategyComboBox.getSelectedItem();

                lambdaLabel.setVisible(traverser.hasLambda());
                lambdaThresholdSlider.setVisible(traverser.hasLambda());
                lambdaValue.setVisible(traverser.hasLambda());
            }
        });

        // Configure of implicit place removal.
        final JLabel implicitPlacesLabel;
        {
            implicitPlacesLabel = factory.createLabel("Remove Implicit Places?");
            add(implicitPlacesLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, implicitPlacesLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    traversalStrategyLabel);
            layout.putConstraint(SpringLayout.EAST, implicitPlacesLabel, leftColumnWidth, SpringLayout.WEST, this);

            removeImplicitPlacesCheckBox = factory.createCheckBox("", true);
            add(removeImplicitPlacesCheckBox);
            layout.putConstraint(SpringLayout.WEST, removeImplicitPlacesCheckBox, columnMargin, SpringLayout.EAST, implicitPlacesLabel);
            layout.putConstraint(SpringLayout.EAST, removeImplicitPlacesCheckBox, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, removeImplicitPlacesCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    implicitPlacesLabel);
        }

        final JLabel removeConcurrentlyLabel;
        {
            removeConcurrentlyLabel = factory.createLabel("Remove IPs concurrently?");
            add(removeConcurrentlyLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, removeConcurrentlyLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    implicitPlacesLabel);
            layout.putConstraint(SpringLayout.EAST, removeConcurrentlyLabel, leftColumnWidth, SpringLayout.WEST, this);

            removeConcurrentlyCheckBox = factory.createCheckBox("", false);
            add(removeConcurrentlyCheckBox);
            layout.putConstraint(SpringLayout.WEST, removeConcurrentlyCheckBox, columnMargin, SpringLayout.EAST, removeConcurrentlyLabel);
            layout.putConstraint(SpringLayout.EAST, removeConcurrentlyCheckBox, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, removeConcurrentlyCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    removeConcurrentlyLabel);
        }

        final JLabel combineSimilarPlacesLabel;
        {
            combineSimilarPlacesLabel = factory.createLabel("Combine similar places?");
            add(combineSimilarPlacesLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, combineSimilarPlacesLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    removeConcurrentlyLabel);
            layout.putConstraint(SpringLayout.EAST, combineSimilarPlacesLabel, leftColumnWidth, SpringLayout.WEST, this);

            combineSimilarPlacesCheckBox = factory.createCheckBox("", true);
            add(combineSimilarPlacesCheckBox);
            layout.putConstraint(SpringLayout.WEST, combineSimilarPlacesCheckBox, columnMargin, SpringLayout.EAST, combineSimilarPlacesLabel);
            layout.putConstraint(SpringLayout.EAST, combineSimilarPlacesCheckBox, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, combineSimilarPlacesCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    combineSimilarPlacesLabel);
        }

        final JLabel uniqueNamesLabel;
        {
            uniqueNamesLabel = factory.createLabel("Unique start & end labels?");
            add(uniqueNamesLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, uniqueNamesLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    combineSimilarPlacesLabel);
            layout.putConstraint(SpringLayout.EAST, uniqueNamesLabel, leftColumnWidth, SpringLayout.WEST, this);

            useUniqueNamesCheckBox = factory.createCheckBox("", false);
            add(useUniqueNamesCheckBox);
            layout.putConstraint(SpringLayout.WEST, useUniqueNamesCheckBox, columnMargin, SpringLayout.EAST, uniqueNamesLabel);
            layout.putConstraint(SpringLayout.EAST, useUniqueNamesCheckBox, -50, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, useUniqueNamesCheckBox, 0, SpringLayout.VERTICAL_CENTER,
                    uniqueNamesLabel);
        }

        // Configuring place removal strategy.
        ImplicitPlaceRemover[] postprocVariants = new ImplicitPlaceRemover[]{
                new ILPBasedImplicitPlaceRemover(),
                new ReplayBasedImplicitPlaceRemover(),
                new OptimizationBasedImplicitPlaceRemover()
        };

        // Constructs a combo box to choose the implicit place removal variant.
        final JLabel implicitPlaceRemovalLabel;
        {
            implicitPlaceRemovalLabel = factory.createLabel("Implicit Place Removal Method:");
            add(implicitPlaceRemovalLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, implicitPlaceRemovalLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    uniqueNamesLabel);
            layout.putConstraint(SpringLayout.EAST, implicitPlaceRemovalLabel, leftColumnWidth, SpringLayout.WEST, this);

            implicitPlaceRemovalVariantComboBox = factory.createComboBox(postprocVariants);
            implicitPlaceRemovalVariantComboBox.setPreferredSize(implicitPlaceRemovalVariantComboBox.getMaximumSize());
            add(implicitPlaceRemovalVariantComboBox);
            implicitPlaceRemovalVariantComboBox.setSelectedIndex(1);
            layout.putConstraint(SpringLayout.WEST, implicitPlaceRemovalVariantComboBox, columnMargin, SpringLayout.EAST, implicitPlaceRemovalLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, implicitPlaceRemovalVariantComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    implicitPlaceRemovalLabel);
        }

        removeImplicitPlacesCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeConcurrentlyLabel.setVisible(removeImplicitPlacesCheckBox.isSelected());
                removeConcurrentlyCheckBox.setVisible(removeImplicitPlacesCheckBox.isSelected());
                implicitPlaceRemovalLabel.setVisible(removeImplicitPlacesCheckBox.isSelected());
                implicitPlaceRemovalVariantComboBox.setVisible(removeImplicitPlacesCheckBox.isSelected());
            }
        });


        TransitionOrderingStrategy[] transitionOrderingStrategies = new TransitionOrderingStrategy[]{
                new ActivityFrequencyOrderingStrategy(),
                new AverageFirstOccurrenceStrategy(),
                new AverageTraceOccurenceStrategy(),
                new LexicographicalOrderingStrategy(),
                new TraceFrequencyOrderingStrategy()
        };

        // Constructs a combo box to choose the transition ordering strategy.
        final JLabel transitionOrderingStrategyLabel;
        {
            transitionOrderingStrategyLabel = factory.createLabel("Implicit Place Removal Method:");
            add(transitionOrderingStrategyLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, transitionOrderingStrategyLabel, rowHeight, SpringLayout.VERTICAL_CENTER,
                    implicitPlaceRemovalLabel);
            layout.putConstraint(SpringLayout.EAST, transitionOrderingStrategyLabel, leftColumnWidth, SpringLayout.WEST, this);

            transitionOrderingStrategyComboBox = factory.createComboBox(transitionOrderingStrategies);
            transitionOrderingStrategyComboBox.setPreferredSize(transitionOrderingStrategyComboBox.getMaximumSize());
            add(transitionOrderingStrategyComboBox);
            transitionOrderingStrategyComboBox.setSelectedIndex(1);
            layout.putConstraint(SpringLayout.WEST, transitionOrderingStrategyComboBox, columnMargin, SpringLayout.EAST, transitionOrderingStrategyLabel);
            layout.putConstraint(SpringLayout.VERTICAL_CENTER, transitionOrderingStrategyComboBox, 0, SpringLayout.VERTICAL_CENTER,
                    transitionOrderingStrategyLabel);
        }
    }

    /**
     * Takes the current configuration of the user interface and stores it in an Parameters object.
     *
     * @return The Parameters containing the current setting of the user interface.
     */
    public Parameters apply() {
        // Extracts the correct event classifier.
        List<XEventClassifier> logClassifiers = log.getClassifiers();
        //XEventClassifier classifierToUse = XLogInfoImpl.STANDARD_CLASSIFIER;
        XEventClassifier eventClassifier = XLogInfoImpl.NAME_CLASSIFIER;
        for (XEventClassifier classifier : logClassifiers) {
            if (classifier.toString().equals(eventClassifierComboBox.getSelectedItem().toString())) {
                eventClassifier = classifier;
                break;
            }
        }

        // Extracts all other information of the user interface.
        String discoveryVariant = discoveryVariantComboBox.getSelectedItem().toString();
        double tauThreshold = tauThresholdSlider.getValue() / 1000.0;
        double lambdaThreshold = lambdaThresholdSlider.getValue() / 1000.0;
        CandidateTraverser traversalStrategy = (CandidateTraverser) traversalStrategyComboBox.getSelectedItem();
        ImplicitPlaceRemover implicitPlaceRemovalVariant = (ImplicitPlaceRemover) implicitPlaceRemovalVariantComboBox.getSelectedItem();
        boolean removeImplicitPlaces = removeImplicitPlacesCheckBox.isSelected();
        boolean removeConcurrently = removeConcurrentlyCheckBox.isSelected();
        boolean combineSimilarPlaces = combineSimilarPlacesCheckBox.isSelected();
        boolean useUniqueNames = useUniqueNamesCheckBox.isSelected();
        TransitionOrderingStrategy transitionOrderingStrategy = (TransitionOrderingStrategy) transitionOrderingStrategyComboBox.getSelectedItem();

        // Storing and printing the info.
        Parameters parameters = new Parameters(discoveryVariant, tauThreshold, lambdaThreshold, traversalStrategy,
                eventClassifier, implicitPlaceRemovalVariant, removeImplicitPlaces, removeConcurrently,
                combineSimilarPlaces, useUniqueNames, transitionOrderingStrategy);
//        System.out.println(parameters.toString());

        return parameters;
    }

    public boolean canApply(Parameters parameters, JComponent jComponent) {
        return true;
    }

    public JComponent getComponent(Parameters parameters) {
        return this;
    }

    public String getTitle() {
        return TITLE;
    }
}