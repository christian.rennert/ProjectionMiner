package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel;

import java.util.HashMap;

/**
 * Efficient place implementation for the representation of Petri Net places in process of eST-Miner.
 */
public class eSTPlace {
    // The ingoing and outgoing transitions of a place are represented by using a bytewise representation as it can be
    // accessed faster than a boolean array.
    private int inputTransitionKey;
    private int outputTransitionKey;

    // Number of current tokens and missing tokens of a place
    private int currentTokens;
    private int underfed;

    // TODO
    private boolean activated;

    private boolean startPlace;
    private boolean endPlace;

    // An array containing boolean entries of each variant, that can be replayed
    private boolean[] variantVector; //used to save which trace variants are fitting this place

    /**
     * Constructor for the eSTPlace Class that is accessed by the factory.
     *
     * @param inputTransitionKey  The bytewise coded input transitions.
     * @param outputTransitionKey The bytewise coded output transitions.
     * @param numVariants         Number of variants in the event log.
     */
    eSTPlace(final int inputTransitionKey, final int outputTransitionKey, final int numVariants) {
        this.inputTransitionKey = inputTransitionKey;
        this.outputTransitionKey = outputTransitionKey;
        currentTokens = 0;
        underfed = 0;
        activated = false;
        startPlace = false;
        endPlace = false;
        variantVector = new boolean[numVariants];
    }

    /**
     * Constructor for the eSTPlace Class that is accessed by the copy function.
     *
     * @param inputTransitionKey  The bytewise coded input transitions.
     * @param outputTransitionKey The bytewise coded output transitions.
     * @param variantVector       The array containing the variants that can be replayed.
     * @param currentTokens       The number of tokens of a place.
     * @param underfed            The number of missing tokens of a place.
     * @param activated           // TODO
     */
    private eSTPlace(final int inputTransitionKey, final int outputTransitionKey, boolean[] variantVector, int currentTokens, int underfed, boolean activated) {
        this.inputTransitionKey = inputTransitionKey;
        this.outputTransitionKey = outputTransitionKey;
        this.currentTokens = currentTokens;
        this.underfed = underfed;
        this.activated = activated;
        startPlace = false;
        endPlace = false;
        this.variantVector = variantVector;
    }

    public boolean isStartPlace() {
        return startPlace;
    }

    public void setStartPlace(boolean startPlace) {
        this.startPlace = startPlace;
    }

    public boolean isEndPlace() {
        return endPlace;
    }

    public void setEndPlace(boolean endPlace) {
        this.endPlace = endPlace;
    }

    public void addToInputTransitionKey(int transitionKeyToBeAdded) {
        inputTransitionKey |= transitionKeyToBeAdded;
    }

    public void addToOutputTransitionKey(int transitionKeyToBeAdded) {
        outputTransitionKey |= transitionKeyToBeAdded;
    }

    /**
     * Aligns the ingoing and outgoing Transition Key according to two different transition strings.
     *
     * @param mappingBetweenTransitions The relational positions between the current keys and the target key system.
     * @param origTransitionsLength     The number of transitions in the old system.
     * @param targetTransitionsLength   The number of transitions in the new system.
     */
    public void alignTransitionKeys(HashMap<Integer, Integer> mappingBetweenTransitions, int origTransitionsLength, int targetTransitionsLength) {
        int newInputTransitionKey = 0;
        int newOutputTransitionKey = 0;

        // Iterating over the positions in the old system
        for (int origPos = 0; origPos < origTransitionsLength; origPos++) {
            // The current position in the target transition system
            int newPos = mappingBetweenTransitions.get(origPos);

            // Checking whether at a certain position a value exists and set the corresponding value in the new system.
            // The assumption is here, that we start numerating the transitions from left to right.
            // i.e. t=["a","b","c"] is "001" for "c"
            if ((inputTransitionKey & createMask(origPos, origTransitionsLength)) > 0) {
                newInputTransitionKey = newInputTransitionKey | createMask(newPos, targetTransitionsLength);
            }
            if ((outputTransitionKey & createMask(origPos, origTransitionsLength)) > 0) {
                newOutputTransitionKey = newOutputTransitionKey | createMask(newPos, targetTransitionsLength);
            }
        }

        // Updates the transition keys
        inputTransitionKey = newInputTransitionKey;
        outputTransitionKey = newOutputTransitionKey;
    }

    /**
     * @return A copy of the given place.
     */
    public eSTPlace copy() {
        return new eSTPlace(inputTransitionKey, outputTransitionKey, variantVector.clone(), currentTokens, underfed, activated);
    }

    /**
     * Changes the fitness status of a variant.
     *
     * @param pos           The index of the variant, whose entry in the fitness array needs to be modified.
     * @param fitnessStatus The new fitness status of the variant.
     */
    public void editVariantVector(int pos, boolean fitnessStatus) {
        variantVector[pos] = fitnessStatus;
    }

    /**
     * @return The place representation used for the eST-Miner as a number.
     */
    public String toNonBinaryString() {
        return "(" + inputTransitionKey + "|" + outputTransitionKey + ")";
    }

    public String toString() {
        return toBinaryString();
    }

    /**
     * @return The place representation used for the eST-Miner as byte code.
     */
    public String toBinaryString() {
        return "(" + Long.toBinaryString(inputTransitionKey) + "|" + Long.toBinaryString(outputTransitionKey) + ")";
    }

    /**
     * Incrementing the number of tokens that a place holds.
     */
    public void addToken() {
        currentTokens++;
    }

    /**
     * Implements the consumption behavior of a place.
     * Decrements the number of tokens a place holds, if the place holds at least one token.
     * Otherwise the number of missing tokens is incremented.
     */
    public void consumeToken() {
        if (currentTokens == 0) {
            underfed++;
        } else {
            currentTokens--;
        }
    }

    /**
     * Removes all transitions that are not contained in the mask for the keys.
     *
     * @param startMask A mask to filter out the start transition.
     * @param endMask   A mask to filter out the end transition.
     */
    public void cutStartAndEndInPlace(int startMask, int endMask) {
        if (inputTransitionKey != (inputTransitionKey & startMask)) {
            startPlace = true;
        }
        if (outputTransitionKey != (outputTransitionKey & endMask)) {
            endPlace = true;
        }

        inputTransitionKey = inputTransitionKey & startMask;
        outputTransitionKey = outputTransitionKey & endMask;
    }

    public void replaceMaskIngoing(int ingoingMaskToKeepTransitions, int ingoingMaskToAddTransitions) {
        if (inputTransitionKey != (inputTransitionKey & ingoingMaskToKeepTransitions)) {
            inputTransitionKey &= ingoingMaskToKeepTransitions;
            inputTransitionKey |= ingoingMaskToAddTransitions;
        }
    }

    public void replaceMaskOutgoing(int outgoingMaskToKeepTransitions, int outgoingMaskToAddTransitions) {
        if (outputTransitionKey != (outputTransitionKey & outgoingMaskToKeepTransitions)) {
            outputTransitionKey &= outgoingMaskToKeepTransitions;
            outputTransitionKey |= outgoingMaskToAddTransitions;
        }
    }

    public void identifyStartAndEndPlaces() {
        if (inputTransitionKey == 0) {
            startPlace = true;
        }
        if (outputTransitionKey == 0) {
            endPlace = true;
        }
    }

    /**
     * Set the activated property to true.
     */
    public void activate() {
        activated = true;
    }

    /**
     * Creates a mask for a single transition and forwards it to the fire method.
     *
     * @param position    The position of the transition to fire.
     * @param transitions The array of transitions.
     */
    public void fire(final int position, final String[] transitions) {
        int mask = createMask(position, transitions);
        fire(mask);
    }

    /**
     * Checks whether the mask matches existing input or output transitions to add or consume places.
     *
     * @param mask Used to select individual positions in the byte wise representation of the input and output transitions
     *             by byte wise comparison of mask and representation.
     */
    public void fire(final int mask) {
        if ((outputTransitionKey & mask) > 0) {//if place contains transition indicated by mask as output
            consumeToken();
            activate();
        }
        if ((inputTransitionKey & mask) > 0) {//if place contains transition indicated by mask as input
            addToken();
            activate();
        }
    }

    /**
     * TODO
     * only adds tokens, no activation
     *
     * @param mask Used to select individual positions in the byte wise representation of the input and output transitions
     *             by byte wise comparison of mask and representation.
     */
    public void producefire(int mask) {
        if ((inputTransitionKey & mask) > 0) {//if place contains transition indicated by mask as input
            currentTokens++;
        }
    }

    /**
     * TODO
     * only decreases tokens, no check of possible, no activation
     *
     * @param mask Used to select individual positions in the byte wise representation of the input and output transitions
     *             by byte wise comparison of mask and representation.
     */
    public void consumefire(int mask) {
        if ((outputTransitionKey & mask) > 0) {//if place contains transition indicated by mask as output
            currentTokens--;
        }
    }

    /**
     * @param place Another place, that needs to be compared.
     * @return True, if input and output transitions are equal.
     */
    public boolean isEqual(eSTPlace place) {
        return ((inputTransitionKey == place.inputTransitionKey) && (outputTransitionKey == place.outputTransitionKey));
    }

    //returns a string version of the place with named transitions

    /**
     * Constructs and returns a string describing the place according to the eST-Miner place description.
     *
     * @param transitions The real names of the transitions.
     * @return Appended transition names in the
     */
    public String toTransitionsString(final String[] transitions) {
        String result = "(" + getKeysTransitionNames(inputTransitionKey, transitions) + "|"
                + getKeysTransitionNames(outputTransitionKey, transitions) + ")";
        return result;
    }

    /**
     * @param key
     * @param transitions
     * @return
     */
    private String getKeysTransitionNames(int key, String[] transitions) {
        String result = "";
        for (int i = 0; i < transitions.length; i++) {
            if ((key & createMask(i, transitions)) > 0) {// test whether this transition is contained in given key
                result = result + transitions[i] + ", ";
            }
        }
        return result.substring(0, result.length() - 2);
    }

    /**
     * Tests, whether a given place is a subplace of the current place.
     *
     * @param subPlace The candidate to be checked.
     * @return True, if the given place is a subplace of the current place.
     */
    public boolean isSubPlace(eSTPlace subPlace) {
        return (((subPlace.inputTransitionKey & inputTransitionKey) == subPlace.inputTransitionKey) &&
                ((subPlace.outputTransitionKey & outputTransitionKey) == subPlace.outputTransitionKey));
    }

    // Getter & Setter

    /**
     * @return The array containing the booleans of the variants, that can be replayed by the place.
     */
    public boolean[] getVariantVector() {
        return variantVector;
    }

    /**
     * @param variantVector An array containing the booleans of the variants, that can be replayed by the place.
     */
    public void setVariantVector(boolean[] variantVector) {
        this.variantVector = variantVector;
    }


    public int getInputTransitionKey() {
        return inputTransitionKey;
    }

    public int getOutputTransitionKey() {
        return outputTransitionKey;
    }

    /**
     * @return The number of tokens the place holds.
     */
    public int getCurrentTokens() {
        return currentTokens;
    }

    public void setCurrentTokens(final int currentTokens) {
        this.currentTokens = currentTokens;
    }

    /**
     * @return The number of missing tokens for this place.
     */
    public int getUnderfed() {
        return underfed;
    }

    /**
     * Sets the number of tokens, that are missing.
     *
     * @param underfed Number of tokens missing.
     */
    public void setUnderfed(final int underfed) {
        this.underfed = underfed;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(final boolean activated) {
        this.activated = activated;
    }

    //return bitmask corresponding to position in the transition array

    /**
     * Computes the mask for a transition at a certain position.
     *
     * @param position    The position of the transition to be returned.
     * @param transitions The array containing the real names of the transitions.
     * @return The mask to get a certain transition.
     */
    public int createMask(final int position, final String[] transitions) {
        return (1 << (transitions.length - (position + 1)));
    }

    /**
     * Computes the mask for a transition at a certain position.
     *
     * @param position          The position of the transition to be returned.
     * @param numbOfTransitions The number of transitions contained in the log.
     * @return The mask to get a certain transition.
     */
    public int createMask(final int position, final int numbOfTransitions) {
        return (1 << (numbOfTransitions - (position + 1)));
    }

    /**
     * Counts the number of arcs ingoing and outgoing.
     *
     * @return The total count of arcs ingoing and outgoing of the place.
     */
    public int getDepth() {
        return Integer.bitCount(inputTransitionKey) + Integer.bitCount(outputTransitionKey);
    }


    /**
     * Counts the number of variants, that can be replayed.
     *
     * @return Number of variants, that can be replayed.
     */
    public int getFitness() {
        int result = 0;
        for (int i = 0; i < variantVector.length; i++) {
            if (variantVector[i]) {
                result++;
            }
        }
        return result;
    }

}
