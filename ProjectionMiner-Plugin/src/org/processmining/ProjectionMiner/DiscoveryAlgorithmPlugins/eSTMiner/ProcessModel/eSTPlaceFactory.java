package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel;

/**
 * A factory to create eSTPlaces.
 */
public class eSTPlaceFactory {
    // Singleton pattern
    private static eSTPlaceFactory instance;

    // The number of variants existing in the log
    private int numVariants;

    /**
     * According to the Singleton pattern the instantiated class can be accessed.
     *
     * @return An instance of the eSTPlaceFactory class.
     */
    public static eSTPlaceFactory getInstance() {
        if (eSTPlaceFactory.instance == null)
            eSTPlaceFactory.instance = new eSTPlaceFactory();
        return instance;
    }

    /**
     * Returns the number of variants.
     *
     * @return Number of variants in the log.
     */
    public int getNumVariants() {
        return numVariants;
    }

    /**
     * Sets the number of variants in the log.
     *
     * @param numVariants The number of variants in the log.
     */
    public void setNumVariants(int numVariants) {
        this.numVariants = numVariants;
    }

    /**
     * Creates a new place for given input and output transitions.
     *
     * @param inputTransitionKey  The binary coded input transitions.
     * @param outputTransitionKey The binary coded output transitions.
     * @return A new place.
     */
    public eSTPlace createPlace(int inputTransitionKey, int outputTransitionKey) {
        if (numVariants == 0) {
            System.out.println("When creating a place you should also set the number of Variants");
        }

        return new eSTPlace(inputTransitionKey, outputTransitionKey, numVariants);
    }
}
