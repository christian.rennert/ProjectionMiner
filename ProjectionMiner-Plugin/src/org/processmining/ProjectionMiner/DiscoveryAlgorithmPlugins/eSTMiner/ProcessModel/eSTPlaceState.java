package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel;

/**
 * There are six states, that can be used to describe how places fit for a fraction τ of traces in the event log.
 * OVERFED - If there tokens remaining for the replay of a fraction of at least (1-τ) of the event log.
 * UNDERFED - If there a tokens missing for the replay of a fraction of at least (1-τ) of the event log.
 * MALFED - Both overfed and underfed.
 * FIT - If there are no tokens missing or remaining for the replay of a fraction of at least τ of the event log.
 * UNFIT - If the overfed or underfed condition doesn't hold but the requirement for a fitting place still doesn't holds.
 * UNKNOWN - The state of the place is unknown.
 */
public enum eSTPlaceState {
    OVERFED, UNDERFED, MALFED,
    FIT, UNFIT, UNKNOWN
}
