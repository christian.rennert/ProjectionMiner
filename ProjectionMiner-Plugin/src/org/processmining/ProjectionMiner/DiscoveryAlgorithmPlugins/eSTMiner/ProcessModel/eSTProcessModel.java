package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.MaskHolder;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;


/**
 * A class containing all places and transitions of the process model.
 */
public class eSTProcessModel {
    eSTPlace startPlace = null;
    eSTPlace endPlace = null;

    // Overview of variants that are fitting for the model
//  private boolean[] variantVector;
    eSTPlace subModelStartPlace = null;
    eSTPlace subModelEndPlace = null;
    // The transitions of the process model
    private String[] transitions;
    private String[] transitionsWithSilent;
    // Place management of the process model
    private ArrayList<eSTPlace> places;


    /**
     * Constructor to set a new process model.
     * Initially all variants are expected to be replayable.
     *
     * @param places      The places of the model.
     * @param transitions The transitions between the places.
     */
    public eSTProcessModel(final ArrayList<eSTPlace> places, final String[] transitions) {
        // Places and transitions of the process model
        this.places = places;
        this.transitions = transitions;
        transitionsWithSilent = transitions;

        // Initializing variant vector with default all variants being replayable
//    variantVector = new boolean[numVariants];
//    Arrays.fill(variantVector, true);
    }

    public eSTProcessModel(Petrinet net, String[] transitions) {
        LinkedList<String> transitionList = new LinkedList<>(Arrays.asList(transitions));

        for (Transition transition : net.getTransitions()) {
            if (!transitionList.contains(transition.getLabel()) && !transition.getLabel().equals("ArtificialStart") && !transition.getLabel().equals("ArtificialEnd")) {
                transitionList.addFirst(transition.getLabel());
            }
        }

        System.out.println(transitionList);

        transitions = transitionList.toArray(new String[transitionList.size()]);
        this.transitions = transitions;
        transitionsWithSilent = transitions;

        HashMap<String, Integer> transitionToPosition = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            if (!transitions[i].equals("Start") && !transitions[i].equals("End")) {
                transitionToPosition.put(transitions[i], i);
            } else if (transitions[i].equals("Start")) {
                transitionToPosition.put("ArtificialStart", i);
                transitionToPosition.put("Start", i);
            } else if (transitions[i].equals("End")) {
                transitionToPosition.put("ArtificialEnd", i);
                transitionToPosition.put("End", i);
            }
        }

        ArrayList<eSTPlace> places = new ArrayList<>();

        for (Place place : net.getPlaces()) {
            int inputTransitionKey = 0;
            int outputTransitionKey = 0;
            for (Transition transition : net.getTransitions()) {
                if (!transitionToPosition.containsKey(transition.getLabel())) {
                    System.out.println(transition.getLabel());
                }

                if (net.getArc(transition, place) != null) {
                    inputTransitionKey |= createMask(transitionToPosition.get(transition.getLabel()), transitions.length);
                }
                if (net.getArc(place, transition) != null) {
                    outputTransitionKey |= createMask(transitionToPosition.get(transition.getLabel()), transitions.length);
                }
            }

            // Do not add the initial place or the final place
            if (!(((outputTransitionKey & createMask(transitionToPosition.get("ArtificialStart"), transitions.length)) > 0 && inputTransitionKey == 0) ||
                    ((inputTransitionKey & createMask(transitionToPosition.get("ArtificialEnd"), transitions.length)) > 0 && outputTransitionKey == 0))) {
                places.add(eSTPlaceFactory.getInstance().createPlace(inputTransitionKey, outputTransitionKey));
            }
        }

        this.places = places;
    }

//    public eSTProcessModel(Petrinet net, String[] transitions) {
//        this.transitions = transitions;
//        transitionsWithSilent = transitions;
//
//        HashMap<String, Integer> transitionToPosition = new HashMap<>();
//        for (int i = 0; i < transitions.length; i++) {
//            if (!transitions[i].equals("Start") && !transitions[i].equals("End")) {
//                transitionToPosition.put(transitions[i], i);
//            } else if (transitions[i].equals("Start")) {
//                transitionToPosition.put("ArtificialStart", i);
//            } else if (transitions[i].equals("End")) {
//                transitionToPosition.put("ArtificialEnd", i);
//            }
//        }
//
//        ArrayList<eSTPlace> places = new ArrayList<>();
//
//        for (Place place : net.getPlaces()) {
//            int inputTransitionKey = 0;
//            int outputTransitionKey = 0;
//            for (Transition transition : net.getTransitions()) {
//
//                if (net.getArc(transition, place) != null) {
//                    inputTransitionKey |= createMask(transitionToPosition.get(transition.getLabel()), transitions.length);
//                }
//                if (net.getArc(place, transition) != null) {
//                    outputTransitionKey |= createMask(transitionToPosition.get(transition.getLabel()), transitions.length);
//                }
//            }
//
//            // Do not add the initial place or the final place
//            if (!(((outputTransitionKey & createMask(transitionToPosition.get("ArtificialStart"), transitions.length)) > 0 && inputTransitionKey == 0) ||
//                    ((inputTransitionKey & createMask(transitionToPosition.get("ArtificialEnd"), transitions.length)) > 0 && outputTransitionKey == 0))) {
//                places.add(eSTPlaceFactory.getInstance().createPlace(inputTransitionKey, outputTransitionKey));
//            }
//        }
//
//        this.places = places;
//    }

    /**
     * Adds a place to the places given in the model.
     *
     * @param place The place to be added to the model.
     */
    public void addPlace(final eSTPlace place) {
        places.add(place);
    }

    /**
     * Is used to removed the start and end transition in the transitions array and corresponding in the places.
     */
    public void cutStartAndEnd() {
        int startPos = -1;
        int endPos = -1;

        // Computes the positions of the start and end transition
        for (int i = 0; i < transitions.length; i++) {
            if (transitions[i].contains("Start")) {
                startPos = i;
                continue;
            } else if (transitions[i].contains("End")) {
                endPos = i;
                continue;
            }
        }

        // Computes the masks to filter out the start and end transitions of the key
        int startMask = Integer.MAX_VALUE - createMask(startPos, transitions.length);
        int endMask = Integer.MAX_VALUE - createMask(endPos, transitions.length);

        // Removes the start and end transitions and marks the corresponding places.
        for (eSTPlace p : places) {
            p.cutStartAndEndInPlace(startMask, endMask);
        }
    }

    /**
     * Is used to recompute the transitionKeys of the places of one model if you want to insert the model into another
     * model which has an other ordering or number of transitions.
     *
     * @param targetTransitions
     */
    public void alignProcessModel(String[] targetTransitions) {
        int origPos = 0;
        for (int i = 0; i < transitions.length; i++) {
            if (transitions[i] == targetTransitions[0] || !transitions[i].contains("tau")) {
                origPos = i;
                break;
            }
        }
        ArrayList<String> newTargetTransitions = new ArrayList<>();
        newTargetTransitions.addAll(Arrays.asList(Arrays.copyOfRange(transitions, 0, origPos)));
        newTargetTransitions.addAll(Arrays.asList(Arrays.copyOf(targetTransitions, targetTransitions.length)));
        targetTransitions = newTargetTransitions.toArray(new String[newTargetTransitions.size()]);


        // Indexing all transitions of the targetTransitions to access them more easily
        HashMap<String, Integer> targetTransitionPosition = new HashMap<>();
        for (int i = 0; i < targetTransitions.length; i++) {
            targetTransitionPosition.put(targetTransitions[i], i);
        }


        // Computes the corresponding positions in the target system according to the positions of the transitions in
        // the current system
        HashMap<Integer, Integer> mappingBetweenTransitions = new HashMap<>();
        for (int i = 0; i < transitions.length; i++) {
            mappingBetweenTransitions.put(i, targetTransitionPosition.get(transitions[i]));
        }

        // Changes the keys for the ingoing and outgoing transitions for each place according to the precomputed values.
        for (eSTPlace p : places) {
            p.alignTransitionKeys(mappingBetweenTransitions, transitions.length, targetTransitions.length);
        }

        // Finally we also update the transitions
        transitions = targetTransitions;

//        ArrayList<String> newTransitionsWithSilent = new ArrayList<>();
//
//        int endPositionWithSilent = 0;
//        for (int i = 0; i < transitionsWithSilent.length; i++) {
//            if (!transitionsWithSilent[i].contains("tau")) {
//                endPositionWithSilent = i;
//                break;
//            }
//        }
//
//        int endPositionTarget = 0;
//        for (int i = 0; i < targetTransitions.length; i++) {
//            if (!targetTransitions[i].contains("tau")) {
//                endPositionTarget = i;
//                break;
//            }
//        }
//
//        newTransitionsWithSilent.addAll(Arrays.asList(Arrays.copyOfRange(transitionsWithSilent, 0, endPositionWithSilent)));
//        newTransitionsWithSilent.addAll(Arrays.asList(Arrays.copyOfRange(targetTransitions, 0, endPositionTarget)));
//        newTransitionsWithSilent.addAll(Arrays.asList(Arrays.copyOfRange(targetTransitions, endPositionTarget, targetTransitions.length)));
//
//        transitionsWithSilent = newTransitionsWithSilent.toArray(new String[newTransitionsWithSilent.size()]);
        transitionsWithSilent = targetTransitions;
    }

    public void identifyStartAndEndPlaces() {
        for (eSTPlace p : places) {
            if (p.getInputTransitionKey() == 0) {
                startPlace = p;
            }
            if (p.getOutputTransitionKey() == 0) {
                endPlace = p;
            }
        }
    }

//    public void identifyStartAndEndPlaces() {
//        for (eSTPlace p : places) {
//            p.identifyStartAndEndPlaces();
//        }
//    }

    public void alignProcessModelWithSilent(String[] targetTransitions) {
        // Indexing all transitions of the targetTransitions to access them more easily
        HashMap<String, Integer> targetTransitionPosition = new HashMap<>();
        for (int i = 0; i < targetTransitions.length; i++) {
            targetTransitionPosition.put(targetTransitions[i], i);
        }

        // Computes the corresponding positions in the target system according to the positions of the transitions in
        // the current system
        HashMap<Integer, Integer> mappingBetweenTransitions = new HashMap<>();
        for (int i = 0; i < transitionsWithSilent.length; i++) {
            mappingBetweenTransitions.put(i, targetTransitionPosition.get(transitionsWithSilent[i]));
        }

        // Changes the keys for the ingoing and outgoing transitions for each place according to the precomputed values.
        for (eSTPlace p : places) {
            p.alignTransitionKeys(mappingBetweenTransitions, transitionsWithSilent.length, targetTransitions.length);
        }

//        // Finally we also update the transitions
//        transitionsWithSilent = targetTransitions;
    }

    /**
     * Adds the places of a submodel to the current model and updates the transitions string array with the silent transitions.
     *
     * @param subModel The submodel to be integrated.
     */
    public void integrateSubModel(eSTProcessModel subModel) {
        subModelStartPlace = subModel.startPlace;
        subModelEndPlace = subModel.endPlace;

        ArrayList<String> newTransitionsWithSilent = new ArrayList<>();

        int endPositionWithSilent = 0;
        for (int i = 0; i < subModel.transitionsWithSilent.length; i++) {
            if (subModel.transitionsWithSilent[i] == this.transitionsWithSilent[0] || !subModel.transitionsWithSilent[i].contains("tau")) {
                endPositionWithSilent = i;
                break;
            }
        }

        newTransitionsWithSilent.addAll(Arrays.asList(Arrays.copyOfRange(subModel.transitionsWithSilent, 0, endPositionWithSilent)));
        newTransitionsWithSilent.addAll(Arrays.asList(this.transitionsWithSilent));

        String[] newTransitionWithSilentArray = newTransitionsWithSilent.toArray(new String[newTransitionsWithSilent.size()]);

        subModel.alignProcessModelWithSilent(newTransitionWithSilentArray);

        for (eSTPlace subModelPlace : subModel.places) {
            places.add(subModelPlace);
        }

        transitionsWithSilent = newTransitionWithSilentArray;
    }


    public void setSilentTransitionsToTransitions() {
        transitions = transitionsWithSilent;
    }

    public String[] getTransitionsWithSilent() {
        return transitionsWithSilent;
    }

    public void removePlace(eSTPlace place) {
        places.remove(place);
    }

    /**
     * Assumes that the current and the given process model have the same encoding of transitions.
     *
     * @param PMToCompare
     * @return
     */
    public boolean equals(eSTProcessModel PMToCompare) {
        if (PMToCompare == null || !Arrays.equals(transitions, PMToCompare.transitions)) {
            return false;
        }


        if (places.size() != PMToCompare.places.size()) {
            return false;
        }

        outerLoop:
        for (eSTPlace p : places) {
            for (eSTPlace p_comp : PMToCompare.places) {
                if (p.toBinaryString().equals(p_comp.toBinaryString())) {
                    continue outerLoop;
                }
            }
            return false;
        }

        return true;
    }

    /**
     * Adds several places to the places given in the model.
     *
     * @param placesToAdd List of the places to be added.
     */
    public void addPlace(final ArrayList<eSTPlace> placesToAdd) {
        places.addAll(placesToAdd);
    }

    /**
     * A method to generate a copy of the process model with identical values for the places, transitions and the variantvector.
     *
     * @return A copy of the actual model with a new object reference.
     */
    public eSTProcessModel copy() {
        return new eSTProcessModel(new ArrayList<eSTPlace>(places), transitions.clone());
    }

//  /**
//   * Sets the fitness value of a certain position in the variantvector to a given input.
//   *
//   * @param pos           A position in the variantvector.
//   * @param fitnessStatus The fitness status of the variant.
//   */
//  public void editVariantVector(int pos, boolean fitnessStatus) {
//    getVariantVector()[pos] = fitnessStatus;
//  }

    // Getter & Setter
    public ArrayList<eSTPlace> getPlaces() {
        return places;
    }

    public void setPlaces(final ArrayList<eSTPlace> places) {
        this.places = places;
    }

    public String[] getTransitions() {
        return transitions;
    }

//  public boolean[] getVariantVector() {
//    return variantVector;
//  }

//  public void setVariantVector(boolean[] variantVector) {
//    this.variantVector = variantVector;
//  }

    /**
     * Prints an overview of all current places in the model with respect to their level in the searchtree, their fitness
     * and their ingoing and outgoing transitions.
     */
    public void printPlaceSummary() {
        String result = "Current places in model: \n level \t fitness \t transitions: ";
        for (eSTPlace place : places) {
            result = result + "\n" + place.getDepth() + "\t" + place.getFitness() + "\t" + place.toTransitionsString(transitions);
        }

        System.out.println(result);
    }


    /**
     * Computes the mask for a transition at a certain position.
     *
     * @param position          The position of the transition to be returned.
     * @param numbOfTransitions The number of transitions contained in the log.
     * @return The mask to get a certain transition.
     */
    public int createMask(final int position, final int numbOfTransitions) {
        return (1 << (numbOfTransitions - (position + 1)));
    }

    public void createStartAndEndPlace(int startOutgoingTransitionMask, int endIngoingTransitionMask) {
        startPlace = eSTPlaceFactory.getInstance().createPlace(0, startOutgoingTransitionMask);
        places.add(startPlace);

        endPlace = eSTPlaceFactory.getInstance().createPlace(endIngoingTransitionMask, 0);
        places.add(endPlace);
    }

    public void splitIntoSimilarPlaces() {
        ArrayList<eSTPlace> placesToBeRemoved = new ArrayList<>();
        ArrayList<eSTPlace> newPlaces = new ArrayList<>();

        for (eSTPlace place : places){
            int loopingTransitionsKey = place.getInputTransitionKey() & place.getOutputTransitionKey();
            if(Integer.bitCount(loopingTransitionsKey) > 1){
                placesToBeRemoved.add(place);

                int partialInputKey = place.getInputTransitionKey() & (Integer.MAX_VALUE - loopingTransitionsKey);
                int partialOutputKey = place.getOutputTransitionKey() & (Integer.MAX_VALUE - loopingTransitionsKey);

                for (Integer mask : MaskHolder.getInstance().getMasks()) {
                    int currentLoopKey = mask & loopingTransitionsKey;

                    if(currentLoopKey > 0){
                        eSTPlace newPlace = eSTPlaceFactory.getInstance().createPlace(partialInputKey | currentLoopKey, partialOutputKey | currentLoopKey);
                        newPlaces.add(newPlace);
                    }
                }

            }
        }

        places.removeAll(placesToBeRemoved);
        places.addAll(newPlaces);
    }
}
