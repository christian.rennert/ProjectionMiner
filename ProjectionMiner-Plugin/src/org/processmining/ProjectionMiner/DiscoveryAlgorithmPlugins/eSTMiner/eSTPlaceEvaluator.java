package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlace;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTPlaceState;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.ProcessModel.eSTProcessModel;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Class to evaluate the fitness of a place contained in a corresponding process model for a given log and a
 * chosen threshold τ.
 */
public class eSTPlaceEvaluator {

    // TODO
    // private final double[][] scoreDF;

    /**
     * @param currentPlace The place to be evaluated.
     * @param processModel The process model, which contains the given place.
     * @param log          The event log to be replayed on the place.
     * @param threshold    The tau threshold used in the eST-algorithm.
     * @return The state of the place according to the eST-Miner.
     */
    public static eSTPlaceState testPlace(final eSTPlace currentPlace, final eSTProcessModel processModel, final eSTLog log, final double threshold) {
        return replayFull(currentPlace, processModel, log, threshold);
    }

    /**
     * @param place        The place to be evaluated.
     * @param processModel The process model, which contains the given place.
     * @param log          The event log to be replayed on the place.
     * @param threshold    The tau threshold used in the eST-algorithm.
     * @return The state of the place according to the eST-Miner.
     */
    private static eSTPlaceState replayFull(final eSTPlace place, final eSTProcessModel processModel, final eSTLog log, final double threshold) {
        // Variables to store intermediate results for the current place
        double numUf = 0;           // # of underfed traces
        double numOf = 0;           // # of overfed traces
        double numActTr = 0;        // # of activated traces
        double numFitAct = 0;       // # of fitting activated traces
        double numFit = 0;          // # of fitting traces
        int currentVariantID = 0;   // Index of the current variant

        // Replaying each trace individually on the place
        for (LinkedList<Integer> trace : log.getTraceVariants()) {
            eSTPlace placeCopy = place.copy();

            // Replaying all events of a trace on the place.
            for (int event : trace) {
                placeCopy.fire(event, processModel.getTransitions());
            }

            // Adding results for the replay of a trace
            HashMap<LinkedList<Integer>, Integer> traceVariantCounts = log.getTraceVariantCounts();

            // Check whether place received or emitted a token by checking for activation.
            if (placeCopy.isActivated()) {
                // Updating counts for the overfed / underfed / fitting traces of the place.
                numActTr = numActTr + traceVariantCounts.get(trace);

                if ((placeCopy.getUnderfed() == 0) && (placeCopy.getCurrentTokens() == 0)) {
                    // Adding to the number of fitting traces and the activated fitting traces.
                    numFitAct = numFitAct + traceVariantCounts.get(trace);
                    numFit = numFit + traceVariantCounts.get(trace);
                    place.editVariantVector(currentVariantID, true);
                } else {
                    if (placeCopy.getCurrentTokens() > 0) {
                        numOf = numOf + traceVariantCounts.get(trace);
                        place.editVariantVector(currentVariantID, false);
                    }
                    if (placeCopy.getUnderfed() > 0) {
                        numUf = numUf + traceVariantCounts.get(trace);
                        place.editVariantVector(currentVariantID, false);
                    }
                }
            } else {
                // Non-activated cannot be malfed. Therefore we update the counter of fitting traces.
                numFit = numFit + traceVariantCounts.get(trace);
                place.editVariantVector(currentVariantID, true);
            }

            currentVariantID++;
        }

        // Comparing the counts with the fraction threshold to find the state of the place.
        eSTPlaceState result = eSTPlaceState.UNFIT;

        // A place, which is never activated is useless and therefore considered unfitting
        if (numActTr > 0) {
            // Checking whether for a threshold τ, the fraction τ of traces in the log are fitting.
            boolean isFitting = (numFitAct / numActTr) >= threshold;

            if (isFitting) {
                result = eSTPlaceState.FIT;
            } else {

                // Checking whether for a threshold τ, the fraction (1-τ) of traces in the log are over- or underfitting.
                boolean isUnderfed = (numUf / numActTr) > (1.0 - threshold);
                boolean isOverfed = (numOf / numActTr) > (1.0 - threshold);

                if (isOverfed && isUnderfed) {
                    result = eSTPlaceState.MALFED;
                } else if (isUnderfed) {
                    result = eSTPlaceState.UNDERFED;
                } else if (isOverfed) {
                    result = eSTPlaceState.OVERFED;
                }
            }
        }

        return result;
    }
}
