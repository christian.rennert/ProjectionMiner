package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import java.util.ArrayList;

public class PlugInStatistics {

    private static PlugInStatistics instance = null;
    //replay based IP removal
    private final ArrayList<Integer> decreasingPlaces = new ArrayList<Integer>();
    private int numFitting = 0; //places  evaluated to be  fitting
    private int numUnfitting = 0; //places evaluated to be not fitting (not necessarily malfed)
    private int numRedundant = 0; //number places identified to be redundant (definition monotonicity-paper)
    private int numImpPlace = 0; //number of places identified to be implicit
    private int numCutPaths = 0; //number of cut paths due to malfedness
    private long timeCandFind = 0;
    private long timeEval = 0;
    private long timeReplay = 0;
    private long timeRedTest = 0;
    private long timeImpTest = 0;
    private long timeLoopCandFind = 0;
    private int numFittingLoop = 0;
    private int numUnfittingLoop = 0;
    private long timeReplayLoop = 0;
    private int numAddedLoop = 0;
    private int comparisons = 0;

    //singleton-pattern
    private PlugInStatistics() {
    }

    public static PlugInStatistics instance() {
        if (instance == null) {
            instance = new PlugInStatistics();
        }
        return instance;
    }

    public void resetStatistics() {
        numFitting = 0;
        numUnfitting = 0;
        numImpPlace = 0;
        numRedundant = 0;
        numCutPaths = 0;

        timeEval = 0;
        timeRedTest = 0;
        timeReplay = 0;
        timeCandFind = 0;
        timeImpTest = 0;

        timeLoopCandFind = 0;
        numFittingLoop = 0;
        numUnfittingLoop = 0;
        timeReplayLoop = 0;
        numAddedLoop = 0;

        comparisons = 0;
        decreasingPlaces.clear();

    }

    //Increment num methods
    public void incComparisons(int num) {
        comparisons = comparisons + num;
    }

    public void addCurrentRemainingPlaces(int num) {
        decreasingPlaces.add(Integer.valueOf(num));
    }

    public void incNumUnfitting() {
        numUnfitting++;
    }

    public void incNumFitting() {
        numFitting++;
    }

    public void incNumCutPaths() {
        numCutPaths++;
    }

    public void incNumFittingLoop() {
        numFittingLoop++;
    }

    public void incNumUnfittingLoop() {
        numUnfittingLoop++;

    }

    public void incNumAddedLoop() {
        numAddedLoop++;
    }

    //Increment time methods

    public void incTimeReplay(final long time) {
        timeReplay = timeReplay + time;
    }

    public void incTimeReplayLoop(long l) {
        timeReplayLoop = timeReplayLoop + l;
    }

    public void incTimeRedTest(final long time) {
        timeRedTest = timeRedTest + time;
    }

    public void incTimeCandFind(final long time) {
        timeCandFind = timeCandFind + time;
    }

    public void incTimeImpTest(final long time) {
        timeImpTest = timeImpTest + time;
    }

    public void incTimeLoopCandFind(long l) {
        timeLoopCandFind = timeLoopCandFind + l;

    }

    //G&S

    public int getNumRedundant() {
        return numRedundant;
    }

    public void setNumRedundant(int numRedundant) {
        this.numRedundant = numRedundant;
    }

    public int getNumFitting() {
        return numFitting;
    }

    public int getNumUnfitting() {
        return numUnfitting;
    }

    public long getTimeRedTest() {
        return timeRedTest;
    }

    public long getTimeReplay() {
        return timeReplay;
    }

    public long getTimeEval() {
        return timeEval;
    }

    public void setTimeEval(long timeEval) {
        this.timeEval = timeEval;
    }

    public long getTimeCandFind() {
        return timeCandFind;
    }

    public int getNumCutPaths() {
        return numCutPaths;
    }

    public long getTimeLoopCandFind() {
        return timeLoopCandFind;
    }

    public int getNumFittingLoop() {
        return numFittingLoop;
    }

    public int getNumAddedLoop() {
        return numAddedLoop;
    }

    public int getNumUnfittingLoop() {
        return numUnfittingLoop;
    }

    public int getNumImpPlace() {
        return numImpPlace;
    }

    public void setNumImpPlace(int numImpPlace) {
        this.numImpPlace = numImpPlace;
    }

    public long getTimeImpTest() {
        return timeImpTest;
    }

    public long getTimeReplayLoop() {
        return timeReplayLoop;
    }

    public ArrayList<Integer> getDecreasingPlaces() {
        return decreasingPlaces;
    }

    public int getComparisons() {
        return comparisons;
    }

}
