package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.CandidateTraversal.CandidateTraverser;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ILPBasedImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.PlaceRemoval.ImplicitPlaceRemover;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.eSTMiner.TransitionOrdering.TransitionOrderingStrategy;
import org.processmining.basicutils.parameters.impl.PluginParametersImpl;

/**
 * Class to store the eST-Miners parameter.
 */
public class Parameters extends PluginParametersImpl {
    // The strategy to traverse through the search tree.
    private final CandidateTraverser traversalStrategy;

    // The variant of the eST-Miner in use.
    private String discoveryVariant;

    // The τ threshold of the eST-Miner algorithm. Fraction of traces, that have to fit, for a place to be fitting.
    private double tauThreshold;

    private double lambdaThreshold;

    // Time limit to find places in milliseconds.
    private long timeLimit;

    // The event classifier in the XES event log.
    private XEventClassifier eventClassifier;

    // Settings for implicit place removal.
    private ImplicitPlaceRemover implicitPlaceRemovalVariant;
    private boolean removeImplicitPlaces;
    private boolean removeConcurrently;
    private boolean combineSimilarPlaces;

    // Flag to set unique names of start and end transition
    private final boolean useUniqueNames;

    // Transition ordering strategy
    private TransitionOrderingStrategy transitionOrderingStrategy;

    // TODO Make sure find usages

    /**
     * Constructor to store the parameters set in ProM.
     *
     * @param discoveryVariant            The chosen variant of the eST-Miner.
     * @param tauThreshold                The threshold of traces to be fitting for a place to be fitting.
     * @param lambdaThreshold             The threshold for eventually follows relations, if a place should be considered interesting.
     * @param traversalStrategy           The strategy for the state traversal in the search tree.
     * @param eventClassifier             The classifier in the log, which tags the events.
     * @param implicitPlaceRemovalVariant The variant of the postprocessing implicit place removal.
     * @param removeImplicitPlaces        Whether implicit places should be removed.
     * @param removeConcurrently          If implicit places should be removed, they should be removed concurrently.
     * @param combineSimilarPlaces
     * @param useUniqueNames
     */
    public Parameters(String discoveryVariant, double tauThreshold, double lambdaThreshold, CandidateTraverser traversalStrategy,
                      XEventClassifier eventClassifier, ImplicitPlaceRemover implicitPlaceRemovalVariant, boolean removeImplicitPlaces,
                      boolean removeConcurrently, boolean combineSimilarPlaces, boolean useUniqueNames, TransitionOrderingStrategy transitionOrderingStrategy) {
        this.discoveryVariant = discoveryVariant;
        this.tauThreshold = tauThreshold;
        this.lambdaThreshold = lambdaThreshold;
        this.traversalStrategy = traversalStrategy;

        // Set limit to a maximum.
        // TODO parameter stays fixed for now
        timeLimit = Long.MAX_VALUE;

        this.eventClassifier = eventClassifier;
        this.implicitPlaceRemovalVariant = implicitPlaceRemovalVariant;
        this.removeImplicitPlaces = removeImplicitPlaces;
        this.removeConcurrently = removeConcurrently;
        this.combineSimilarPlaces = combineSimilarPlaces;
        this.useUniqueNames = useUniqueNames;
        this.transitionOrderingStrategy = transitionOrderingStrategy;
    }

    /**
     * Default constructor, settings were set to something like none.
     */
    public Parameters() {
        discoveryVariant = "No discovery variant specified!";
        tauThreshold = 0.75;
        traversalStrategy = null;

        // Set limit to a maximum.
        // TODO parameter stays fixed for now
        timeLimit = Long.MAX_VALUE;

        eventClassifier = XLogInfoImpl.STANDARD_CLASSIFIER;
        implicitPlaceRemovalVariant = new ILPBasedImplicitPlaceRemover();
        removeImplicitPlaces = true;
        removeConcurrently = false;
        transitionOrderingStrategy = null;
        useUniqueNames = false;
    }

    public boolean shouldRemoveImplicitsConcurrently() {
        return removeConcurrently;
    }

    public void setRemoveConcurrently(boolean removeConcurrently) {
        this.removeConcurrently = removeConcurrently;
    }


    public double getThresholdTau() {
        return tauThreshold;
    }

    public void setThresholdTau(double threshold) {
        tauThreshold = threshold;
    }


    public long getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(long timeLimit) {
        this.timeLimit = timeLimit;
    }

    public XEventClassifier getEventClassifier() {
        return eventClassifier;
    }


    public void setEventClassifier(XEventClassifier eventClassifier) {
        this.eventClassifier = eventClassifier;
    }

    public boolean shouldRemoveImplicits() {
        return removeImplicitPlaces;
    }

    public boolean shouldUseUniqueNames() {
        return useUniqueNames;
    }

    public void setRemoveImplicitPlaces(boolean removeImplicitPlaces) {
        this.removeImplicitPlaces = removeImplicitPlaces;
    }

    public String getDiscoveryVariant() {
        return discoveryVariant;
    }

    public void setDiscoveryVariant(String discoveryVariant) {
        this.discoveryVariant = discoveryVariant;
    }

    public ImplicitPlaceRemover getImplicitPlaceRemovalVariant() {
        return implicitPlaceRemovalVariant;
    }

    public void setImplicitPlaceRemovalVariant(ImplicitPlaceRemover implicitPlaceRemovalVariant) {
        this.implicitPlaceRemovalVariant = implicitPlaceRemovalVariant;
    }

    /**
     * Constructs a string containing the stored settings made in the parameters.
     *
     * @return A string containing the parameters.
     */
    public String toString() {
        String result = "CHOSEN PARAMETERS: \n";
        result = result + "Selected Classifier: " + eventClassifier + "\n";
        result = result + "Discovery Variant: " + discoveryVariant + "\n";
        result = result + "Traversal Strategy: " + traversalStrategy.toString() + "\n";
        result = result + "Threshold Tau: " + tauThreshold + "\n";
        result = result + "Lambda threshold: " + lambdaThreshold + "\n";
        result = result + "Implicit places removed?: " + removeImplicitPlaces + "\n";
        result = result + "IPs removed concurrently?: " + removeConcurrently + "\n";
        result = result + "Combine similar places?: " + combineSimilarPlaces + "\n";
        result = result + "Removal Variant: " + implicitPlaceRemovalVariant.toString() + "\n";
        result = result + "Transition ordering Strategy:" + transitionOrderingStrategy + "\n";
        result = result + "Should use unique names?: " + useUniqueNames + "\n";
        return result;
    }

    public CandidateTraverser getTraversalStrategy() {
        return traversalStrategy;
    }

    public TransitionOrderingStrategy getTransitionOrderingStrategy() {
        return transitionOrderingStrategy;
    }

    public double getLambdaThreshold() {
        return lambdaThreshold;
    }

    public boolean shouldCombineSimilarPlaces() {
        return combineSimilarPlaces;
    }
}