package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins;

import com.raffaeleconforti.bpmnminer.converter.BPMNToPetriNetConverterPlugin;
import org.deckfour.xes.classification.XEventNameClassifier;
import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.SplitMiner.au.edu.qut.processmining.miners.splitminer.SplitMiner;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.SplitMiner.au.edu.qut.processmining.miners.splitminer.ui.miner.SplitMinerUI;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.SplitMiner.au.edu.qut.processmining.miners.splitminer.ui.miner.SplitMinerUIResult;
import org.processmining.acceptingpetrinet.models.impl.AcceptingPetriNetImpl;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.models.graphbased.directed.bpmn.BPMNDiagram;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;

public class LocalSplitMiner extends DiscoveryPlugin {
    @Override
    public AcceptingPetriNetWrapper discover(UIPluginContext context, XLog log, Object parameter) {
        SplitMinerUIResult result;
        if (parameter == null) {
            parameter = startGUI(context, log);
        }

        result = (SplitMinerUIResult) parameter;

        SplitMiner sm = new SplitMiner();
        BPMNDiagram output = sm.mineBPMNModel(log, new XEventNameClassifier(), result.getPercentileFrequencyThreshold(), result.getParallelismsThreshold(),
                result.getFilterType(), result.isParallelismsFirst(),
                result.isReplaceIORs(), result.isRemoveLoopActivities(), result.getStructuringTime());

        BPMNToPetriNetConverterPlugin converterPlugin = new BPMNToPetriNetConverterPlugin();
        Petrinet net = converterPlugin.convert(output);
        AcceptingPetriNetImpl acceptingPetriNet = new AcceptingPetriNetImpl(net);

        return new AcceptingPetriNetWrapper(acceptingPetriNet.getNet(), acceptingPetriNet.getInitialMarking(), acceptingPetriNet.getFinalMarkings().iterator().next(), parameter);
    }

    @Override
    public Object startGUI(UIPluginContext context, XLog log) {
        SplitMinerUI gui = new SplitMinerUI();
        SplitMinerUIResult result = gui.showGUI(context, "Setup HM+");
        return result;
    }

    @Override
    public String toString() {
        return "Split Miner";
    }
}
