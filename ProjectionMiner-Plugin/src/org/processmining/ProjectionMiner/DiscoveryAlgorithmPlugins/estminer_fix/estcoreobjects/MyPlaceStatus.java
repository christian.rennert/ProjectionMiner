package org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.estminer_fix.estcoreobjects;

public enum MyPlaceStatus {
		OVERFED,
		UNDERFED,
		MALFED, //(over and underfed)
		FIT, 
		UNFIT, 
		UNKNOWN
}
