package org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions;

import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.classification.XEventNameClassifier;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.AcceptingPetriNetWrapper;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.Plugin.ProjectionMinerParameters;
import org.processmining.ProjectionMiner.utils.Modifiers.LogModifier;
import org.processmining.ProjectionMiner.utils.Modifiers.PetriNetModifier;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.plugins.InductiveMiner.efficienttree.EfficientTree;
import org.processmining.plugins.InductiveMiner.mining.MiningParameters;
import org.processmining.plugins.InductiveMiner.mining.MiningParametersIM;
import org.processmining.plugins.InductiveMiner.plugins.IMProcessTree;
import org.processmining.processtree.Block;
import org.processmining.processtree.Node;
import org.processmining.processtree.ProcessTree;

import javax.swing.*;
import java.util.*;

public class InductivePTreeSampler extends LoopingTransitionStrategy {
    /**
     * @param context           The context of the ProM application.
     * @param inputLog          The projected log.
     * @param plugin            The discovery plugin to discover Petri nets on the given log after modifications.
     * @param parameter         The parameters of the plugin to be given.
     * @param generalParameters
     * @return A Petri net for
     * @throws InterruptedException An exception thrown when the discovery algorithm is interrupted.
     */
    @Override
    public AcceptingPetriNetWrapper apply(UIPluginContext context, XLog inputLog, DiscoveryPlugin plugin, Object parameter, ProjectionMinerParameters generalParameters) throws InterruptedException {
        // Setting up the inductive Miner.
//        org.processmining.plugins.inductiveminer2.variants.MiningParametersIM miningParametersIM = new org.processmining.plugins.inductiveminer2.variants.MiningParametersIM();
//        MiningParametersAbstract miningParameters = miningParametersIM.getMiningParameters();

//        // Creating the log implementation of the inductive miner.
//        IMLog log = miningParameters.getIMLog(inputLog);

        // Creating the process tree using the inductive miner.
        MiningParameters miningParametersNew = new MiningParametersIM();

        ProcessTree processTree = IMProcessTree.mineProcessTree(inputLog, miningParametersNew);
//        EfficientTree efficientTree = InductiveMiner.mineEfficientTree(log, miningParameters, () -> context.getProgress().isCancelled());

        // Checking the root, which is considered, for its type.
        // TODO: Implementing other constructs than sequences and concurrency as well.
        Node root = processTree.getRoot();
        List<ProcessTree.Type> possibleTypes = Arrays.asList(new ProcessTree.Type[]{ProcessTree.Type.XOR, ProcessTree.Type.SEQ,
                ProcessTree.Type.AND, ProcessTree.Type.LOOPXOR});

        if (!(possibleTypes.contains(processTree.getType(root)))) {
//            System.out.println("Der Sampler war nicht anwendbar.");
//            System.out.println("Der Typ ist:" + processTree.getType(root));
//            System.out.println("Die Kinder sind");
//            if (root.getParents().size() > 0) {
//                System.out.println("Es gibt Kinder");
//            }
//            for (root.getParents()) {
//                System.out.println(root.getName());
////                System.out.println(processTree.getNodeType(child));
//            }
            return null;
        }

        // Finding the set of activities contained in each subtree.
        LinkedList<HashSet<String>> subsetList = new LinkedList<>();
        Block rootBlock = (Block) root;

        Iterable<Node> rootChildren = rootBlock.getChildren();
        for (Node child : rootChildren) {
            subsetList.add(unpackChild(child, processTree));
        }

        LinkedList<HashSet<String>> newSubsetList = new LinkedList<>();
//        if (processTree.getType(root) != ProcessTree.Type.LOOPXOR) {
            Iterator<HashSet<String>> iterator = subsetList.iterator();
            if (iterator.hasNext()) {
                newSubsetList.add(iterator.next());
            }
            HashSet<String> secondItem = new HashSet<>();
            while (iterator.hasNext()) {
                secondItem.addAll(iterator.next());
            }

            newSubsetList.add(secondItem);
//        }else{
//            HashSet<String> secondItem = subsetList.removeLast();
//
//            Iterator<HashSet<String>> iterator = subsetList.iterator();
//            HashSet<String> firstItem = new HashSet<>();
//            while (iterator.hasNext()) {
//                firstItem.addAll(iterator.next());
//            }
//
//            newSubsetList.add(firstItem);
//            newSubsetList.add(secondItem);
//        }

        subsetList = newSubsetList;

//        Iterable<Integer> rootChildren = efficientTree.getChildren(root);
//        for (Integer child : rootChildren) {
//            subsetList.add(unpackChild(child, efficientTree));
//        }

//        // Checking the root, which is considered, for its type.
//        // TODO: Implementing other constructs than sequences and concurrency as well.
//        int root = efficientTree.getRoot();
//        if (!(efficientTree.isSequence(root) || efficientTree.isConcurrent(root))) {
//            System.out.println("Der Sampler war nicht anwendbar.");
//            System.out.println("Der Typ ist:" + efficientTree.getNodeType(root));
//            System.out.println("Die Kinder sind");
//            for (Integer child : efficientTree.getChildren(root)) {
//                System.out.println(efficientTree.getActivity(child));
//                System.out.println(efficientTree.getNodeType(child));
//            }
//            DebuggingClass.getInstance().setLastEfficientTree(efficientTree);
//
//            System.out.println("Es gibt Eltern: " + processTree.getRoot().getParents().size());
//            Block blub = (Block) processTree.getRoot();
//            System.out.println("Es gibt Kinder: " + blub.getChildren().size());
//
//            DebuggingClass.getInstance().setLastTree(processTree);
//            return null;
//        }

//        // Finding the set of activities contained in each subtree.
//        LinkedList<HashSet<String>> subsetList = new LinkedList<>();
//        Iterable<Integer> rootChildren = efficientTree.getChildren(root);
//        for (Integer child : rootChildren) {
//            subsetList.add(unpackChild(child, efficientTree));
//        }

        // Computing the subnets for each individual log.
        LinkedList<AcceptingPetriNetWrapper> subnets = new LinkedList<>();
        for (HashSet<String> activitySubset : subsetList) {
            XLog subLog = projectOnActivities(inputLog, activitySubset);

            boolean skippable = LogModifier.getInstance().removeEmptyTraces(subLog);
            if(subLog.isEmpty()){
                return null;
            }

            subnets.add(plugin.discoverWithMeasure(context, subLog, parameter));

            // Making subnet optional if needed
            if (skippable) {
                PetriNetModifier.getInstance().makePetriNetSkippable(
                        subnets.getLast().getNet(),
                        subnets.getLast().getInitialMarking(),
                        subnets.getLast().getFinalMarking());
            }
        }

        // The mined subnets are connected according to the root in process tree.
        AcceptingPetriNetWrapper result;
//        if (efficientTree.isSequence(root)) {
//            result = PetriNetModifier.getInstance().connectSequentially(subnets);
//        } else if (efficientTree.isConcurrent(root)) {
//            result = PetriNetModifier.getInstance().connectConcurrent(subnets);
//        } else {
//            throw new InterruptedException("Following case not handled yet: " + efficientTree.getNodeType(root));
//        }

        if (processTree.getType(root) == ProcessTree.Type.SEQ) {
            result = PetriNetModifier.getInstance().connectSequentially(subnets);
        } else if (processTree.getType(root) == ProcessTree.Type.XOR) {
            result = PetriNetModifier.getInstance().connectConcurrent(subnets);
        } else if (processTree.getType(root) == ProcessTree.Type.AND) {
            result = PetriNetModifier.getInstance().connectAnd(subnets);
        } else if (processTree.getType(root) == ProcessTree.Type.LOOPXOR) {
            result = PetriNetModifier.getInstance().connectLooping(subnets);
        } else {
            throw new InterruptedException("Following case not handled yet: " + processTree.getType(root));
        }

        return result;
    }

    private HashSet<String> unpackChild(Node child, ProcessTree processTree) {
        if (child.isLeaf()) {
            HashSet<String> result = new HashSet<>();
            result.add(child.getName());
            return result;
        }

        List<Node> currentChildren = ((Block) child).getChildren();
        List<Node> nextChildren = new ArrayList<>();

        boolean finished = false;
        HashSet<String> result = new HashSet<>();

        while (!finished) {
            for (Node currentChild : currentChildren) {
                if (currentChild.isLeaf()) {
                    if (!currentChild.getName().equals("tau")) {
                        result.add(currentChild.getName());
                    }
                } else {
                    nextChildren.addAll(((Block) currentChild).getChildren());
                }
            }

            if (nextChildren.size() > 0) {
                currentChildren = new ArrayList<>(nextChildren);
                nextChildren = new ArrayList<>();
            } else {
                finished = true;
            }
//            System.out.println("A");
        }

        return result;
    }

    @Override
    public int getPrefRecursionDepth() {
        return 5;
    }

    /**
     * Method to find the labels of the leafs of the ancestors of a node in a Process tree.
     *
     * @param nodeID The ID of a node in the process tree.
     * @param tree   The process tree to search in.
     * @return The labels of the activities in the ancestors.
     */
    public HashSet<String> unpackChild(int nodeID, EfficientTree tree) {
        HashSet<String> result = new HashSet<>();
        LinkedList<Integer> unvisitedNodes = new LinkedList<>();
        unvisitedNodes.add(nodeID);

        while (!unvisitedNodes.isEmpty()) {
            Integer currentNode = unvisitedNodes.removeFirst();

            if (tree.isActivity(currentNode)) {
                result.add(tree.getActivityName(currentNode));
            } else {
                tree.getChildren(currentNode).forEach(unvisitedNodes::add);
            }
        }

        return result;
    }

    /**
     * Projects a log on a set of activities by removing all other activities.
     *
     * @param inputLog   The input log to be projected.
     * @param activities The activities to project on.
     * @return The projected log.
     */
    public XLog projectOnActivities(XLog inputLog, HashSet<String> activities) {
        XLog logCopy = LogModifier.getInstance().copyLog(inputLog);

        DefaultListModel<XEventClass> eventClassList = new DefaultListModel<>();
        XEventClassifier classifier = new XEventNameClassifier();
        XLogInfo logInfo = XLogInfoFactory.createLogInfo(logCopy, classifier);
        eventClassList.clear();
        List<XEventClass> dumbJava = new LinkedList<>(logInfo.getEventClasses().getClasses());
        Collections.sort(dumbJava);
        for (XEventClass ec : dumbJava) {
            eventClassList.addElement(ec);
        }


        //make set of selected event classes
        Set<XEventClass> selectedEventClasses = new HashSet<>();
        for (int i = 0; i < eventClassList.size(); i++) {
            XEventClass e = eventClassList.get(i);
            if (activities.contains(e.getId())) {
                selectedEventClasses.add(e);
            }
        }

        //copy only the events with event class that was selected
        XLog result = new XLogImpl(logCopy.getAttributes());

        for (XTrace trace : logCopy) {
            XTrace copyTrace = new XTraceImpl(trace.getAttributes());
            for (XEvent event : trace) {
                if (selectedEventClasses.contains(logInfo.getEventClasses().getClassOf(event))) {
                    copyTrace.add(event);
                }
            }

            result.add(copyTrace);

        }
        return result;
    }

    @Override
    public String toString() {
        return "Subsampling activities using Process Trees";
    }
}

