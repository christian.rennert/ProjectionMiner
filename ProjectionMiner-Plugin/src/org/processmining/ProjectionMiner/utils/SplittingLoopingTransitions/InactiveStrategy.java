package org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions;

import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.AcceptingPetriNetWrapper;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.Plugin.ProjectionMinerParameters;
import org.processmining.contexts.uitopia.UIPluginContext;

/**
 * Dummy in case, we don't want to modify the Petri net.
 */
public class InactiveStrategy extends LoopingTransitionStrategy {
    @Override
    public AcceptingPetriNetWrapper apply(UIPluginContext context, XLog inputLog, DiscoveryPlugin plugin, Object parameter, ProjectionMinerParameters generalParameters)  {
        return null;
    }

    @Override
    public int getPrefRecursionDepth() {
        return 10;
    }

    @Override
    public String toString() {
        return "No strategy";
    }
}
