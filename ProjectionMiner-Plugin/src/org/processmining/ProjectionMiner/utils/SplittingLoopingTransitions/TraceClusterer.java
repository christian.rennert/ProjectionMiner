package org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions;

import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.impl.XLogImpl;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.AcceptingPetriNetWrapper;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.Plugin.ProjectionMinerParameters;
import org.processmining.ProjectionMiner.utils.Modifiers.LogModifier;
import org.processmining.ProjectionMiner.utils.Modifiers.PetriNetModifier;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.xlu.matching.models.RepEGraph;
import org.processmining.xlu.matching.models.Tree;
import org.processmining.xlu.matching.models.collection.EGraphsWithMappings;
import org.processmining.xlu.matching.models.collection.RepEGraphs;
import org.processmining.xlu.matching.plugins.EGFusionPlugin;
import org.processmining.xlu.matching.plugins.EGMatchPlugin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Strategy for clustering traces by similarity and discovering subnets for the similar traces.
 */
public class TraceClusterer extends LoopingTransitionStrategy {
    /**
     * @param context           The context of the ProM application.
     * @param inputLog          The projected log.
     * @param plugin            The discovery plugin to discover Petri nets on the given log after modifications.
     * @param parameter         The parameters of the plugin to be given.
     * @param generalParameters
     * @return A Petri net with concurrent subnets, that cover similar traces.
     * @throws InterruptedException
     */
    @Override
    public AcceptingPetriNetWrapper apply(UIPluginContext context, XLog inputLog, DiscoveryPlugin plugin, Object parameter, ProjectionMinerParameters generalParameters) throws InterruptedException {
        // Computing the clustered logs
        EGMatchPlugin pluginMatcher = new EGMatchPlugin();
        EGraphsWithMappings mappings = pluginMatcher.computeTraceMatchingGreedy(context, inputLog);

        EGFusionPlugin pluginFuser = new EGFusionPlugin();
        RepEGraphs fusedgraphs = pluginFuser.fuseIncGraphs(context, mappings);

        LinkedList<XLog> clusteredLogs = new LinkedList<>();

        List<RepEGraph> graphs = fusedgraphs.getGraphs();
        if (graphs.size() == 1) {
            List<RepEGraph> newGraphs = new ArrayList<>();

            int childCount = fusedgraphs.getHierarchy().getRoot().getChildAt(0).getChildCount();

            for (int i = 0; i < childCount; i++) {
                newGraphs.add((RepEGraph) ((Tree<?>) fusedgraphs.getHierarchy().getRoot().getChildAt(0).getChildAt(i)).getObject());
            }
            graphs = newGraphs;
        }

        for (RepEGraph graph : graphs) {
            XLog newLog = new XLogImpl(inputLog.getAttributes());

            for (Integer id : graph.getRepresentedGraphIds()) {
                newLog.add(fusedgraphs.getLog().get(id));
            }

            clusteredLogs.add(newLog);
        }

        LinkedList<XLog> newLogList = new LinkedList<>();
        Iterator<XLog> iterator = clusteredLogs.iterator();
        if (iterator.hasNext()) {
            newLogList.add(iterator.next());
        }
        XLog secondItem = new XLogImpl(inputLog.getAttributes());
        while (iterator.hasNext()) {
            secondItem.addAll(iterator.next());
        }
        newLogList.add(secondItem);

        clusteredLogs = newLogList;

        // Discovering the subnets and inserting them into an exclusive choice construct.
        LinkedList<AcceptingPetriNetWrapper> subnets = new LinkedList<>();

        for (XLog clusteredLog : clusteredLogs) {
            boolean skippable = LogModifier.getInstance().removeEmptyTraces(clusteredLog);
            subnets.add(plugin.discoverWithMeasure(context, clusteredLog, parameter));

            // Making subnet optional if needed
            if (skippable) {
                PetriNetModifier.getInstance().makePetriNetSkippable(
                        subnets.getLast().getNet(),
                        subnets.getLast().getInitialMarking(),
                        subnets.getLast().getFinalMarking());
            }
        }

        return PetriNetModifier.getInstance().connectExclusiveChoice(subnets);
    }

    @Override
    public int getPrefRecursionDepth() {
        return 1;
    }

    @Override
    public String toString() {
        return "Trace Clustering using L2ME";
    }
}
