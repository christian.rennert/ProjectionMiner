package org.processmining.ProjectionMiner.utils.SplittingLoopingTransitions;

import org.deckfour.xes.model.XLog;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.AcceptingPetriNetWrapper;
import org.processmining.ProjectionMiner.DiscoveryAlgorithmPlugins.DiscoveryPlugin;
import org.processmining.ProjectionMiner.Plugin.ProjectionMinerParameters;
import org.processmining.contexts.uitopia.UIPluginContext;

/**
 * Abstract class for collecting the options for splitting the looping transitions.
 */
public abstract class LoopingTransitionStrategy {
    /**
     * Method to apply the strategy to find a refined Petri net for the projected log.
     *
     * @param context   The context of the ProM application.
     * @param inputLog  The projected log.
     * @param plugin    The discovery plugin to discover Petri nets on the given log after modifications.
     * @param parameter The parameters of the plugin to be given.
     * @param generalParameters The Parameter of the RecursiveSubnetMining Plugin
     * @return An accepting Petri net that was found using the strategy on the
     * @throws InterruptedException An exception thrown when the discovery algorithm is interrupted.
     */
    public abstract AcceptingPetriNetWrapper apply(UIPluginContext context, XLog inputLog, DiscoveryPlugin plugin, Object parameter, ProjectionMinerParameters generalParameters) throws InterruptedException;

    public abstract int getPrefRecursionDepth();
}
