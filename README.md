# ProjectionMiner
A discovery algorithm plugin in the ProM framework for the "Improving the eST-Miner Models using Non-Unique Transition Labels" master thesis.

## Usage
Using the plugin requires Eclipse and Ivy for import of the project. 
By running the "ProM with UITopia (ProjectionMiner).launch" file using the Eclipse UI, an instance of ProM can be started.
Here, the user can import logs and Petri nets to apply the ProjectionMiner to.

There are three possible inputs for the ProjectionMiner:
 - an event log (such that an initial net is discovered first)
 - a Petri net (such that the marking of the Petri net is guessed)
 - a Petri net, its initial and final marking
 
The ProjectionMiner returns an accepting Petri net, its initial and final marking.

When using the ProjectionMiner, first the UI of the ProjectionMiner is shown where a discovery algorithm can be selected and further options can be taken.
For a more detailed description, we refer to the Chapter 6 in "Improving the eST-Miner Models using Non-Unique Transition Labels".
Next, the user is shown the UI of the discovery algorithm chosen.
It is important to choose a variant that guarantees perfect fitness for the ProjectionMiner to work properly.